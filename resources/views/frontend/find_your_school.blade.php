@extends('layouts.front_theme')
@section('content')

<!-- Start Subscribe Area -->
<section class="bcare__subscribe bg-image--7 subscrive--2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-lg-12">
                <div class="subscribe__inner">
                    <h2>Select Your School</h2>
                    <div class="newsletter__form">
                        <div class="input__box">
                            <div id="mc_embed_signup">
                                <form action="" onsubmit="return searchSchools()" 
                                      method="post" 
                                      >
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <div class="news__input">
                                                {{ Form::select('state_id',$states,[],['id'=>'state_id','onChange'=>'citiesDD()','class'=>'form-control','required']) }}
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="news__input">
                                                {{ Form::select('city_id',$cities,[],['id'=>'city_id','class'=>'form-control','required']) }}
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="clearfix subscribe__btn" style="text-align: left;margin: 0">
                                                <input style="height: 38px" type="submit" value="Search" name="Search" id="search_school" class="bst__btn btn--white__color">
                                            </div>          
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="schools-container" class="dcare__courses__area section-padding--lg bg--white">
</section>

<!-- End Subscribe Area -->
@endsection
@section('script')
<script>
    function citiesDD() {
//        alert('');
        var $url = "{{ url('cities-dd') }}";
        var sId = $('#state_id').val();

        $url = $url + "?state_id=" + sId + "&city_id=&ignore_without_schools=1";
        if(sId != "") {
            $('#city_id').html('<option value="">Select City</option>');  
            $("#search_school").attr('disabled',true);
            $("#search_school").attr('value',"Please wait....");
            $.get($url, function (data) {
                $('#city_id').html(data.html);
                $("#search_school").attr('disabled',false);
                $("#search_school").attr('value',"Search");
            });
        } else {
            $('#city_id').html('<option value="">Select City</option>');   
        }
    }
    function searchSchools() {
        var $url = "{{ url('find-your-school') }}";
        var sId = $('#state_id').val();
        var cId = $('#city_id').val();
        
        $url = $url + "?state_id=" + sId + "&city_id=" + cId;

        $.get($url, function (data) {
            $('#schools-container').html(data);
            $('html, body').animate({
                scrollTop: $(".subscribe__inner").offset().top
            }, 500);
        });
        return false;
    }
</script>
@endsection
