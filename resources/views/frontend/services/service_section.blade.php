
<section class="elementor-section elementor-top-section elementor-element elementor-element-5842250 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="5842250" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-46b059d" data-id="46b059d" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
                <div class="elementor-element elementor-element-6552dbc elementor-widget elementor-widget-image" data-id="6552dbc" data-element_type="widget" data-widget_type="image.default">
                    <div class="elementor-widget-container">
                        <!--/wp-content/uploads/2022/11/serious-firefighter-in-uniform-and-helmet-checking-fire-alarm-1024x684.jpg-->
                        <img width="800" height="534" src="<?= asset('public/images/thumbnails/' . $service->image) ?>" class="attachment-large size-large wp-image-1159" 
                             alt="<?= $service->title ?>" decoding="async" loading="lazy" 
                             /></div>
                </div>
                <?= $service->description ?>
            </div>
        </div>
        <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-ce6248d" data-id="ce6248d" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
                <section class="elementor-section elementor-inner-section elementor-element elementor-element-4d73243 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="4d73243" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-no">
                        <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-4821159" data-id="4821159" data-element_type="column">
                            <div class="elementor-widget-wrap elementor-element-populated">
                                <?php
                                $allServices = services();
                                ?>
                                @foreach($allServices as $sr)
                                <?php
                                $class = 'elementor-element-b00307c';
                                if ($sr->id == request()->id) {
                                    $class = 'elementor-element-1ec2383';
                                }
                                ?>
                                <div class="elementor-element <?= $class ?> elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="b00307c" data-element_type="widget" data-widget_type="icon-list.default">
                                    <div class="elementor-widget-container">
                                        <ul class="elementor-icon-list-items">
                                            <a href="<?= url('service-detail/' . $sr->id) ?>">
                                                <li class="elementor-icon-list-item">
                                                    <span class="elementor-icon-list-icon">
                                                        <i aria-hidden="true" class=" arrow_carrot-2right"></i>						</span>
                                                    <span class="elementor-icon-list-text"><?= $sr->title ?></span>
                                                </li>
                                            </a>
                                        </ul>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </section>

                <section class="elementor-section elementor-inner-section elementor-element elementor-element-9785aa1 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="9785aa1" data-element_type="section">
                    <div class="elementor-background-overlay"></div>
                    <div class="elementor-container elementor-column-gap-no">
                        <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-d0a87a9" data-id="d0a87a9" data-element_type="column">
                            <div class="elementor-widget-wrap elementor-element-populated">
                                <div class="elementor-element elementor-element-22e952c elementor-widget elementor-widget-heading" data-id="22e952c" data-element_type="widget" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                        <h2 class="elementor-heading-title elementor-size-default">Follow Us</h2>		</div>
                                </div>
                                <div class="elementor-element elementor-element-14440fc elementor-widget-divider--view-line elementor-widget elementor-widget-divider" data-id="14440fc" data-element_type="widget" data-widget_type="divider.default">
                                    <div class="elementor-widget-container">
                                        <div class="elementor-divider">
                                            <span class="elementor-divider-separator">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-element elementor-element-ccae6df elementor-shape-circle e-grid-align-left elementor-grid-0 elementor-widget elementor-widget-social-icons" data-id="ccae6df" data-element_type="widget" data-widget_type="social-icons.default">
                                    <div class="elementor-widget-container">
                                        <div class="elementor-social-icons-wrapper elementor-grid">

                                            @if($siteDetail->fb_link)
                                            <span class="elementor-grid-item">
                                                <a href="<?= $siteDetail->fb_link ?>" class="elementor-icon elementor-social-icon elementor-social-icon-facebook-f elementor-repeater-item-ab939df" target="_blank">
                                                    <span class="elementor-screen-only">Facebook-f</span>
                                                    <i class="fab fa-facebook-f"></i>					
                                                </a>
                                            </span>
                                            @endif
                                            @if($siteDetail->insta_link)
                                            <span class="elementor-grid-item">
                                                <a href="<?= $siteDetail->insta_link ?>" class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-repeater-item-b944676" target="_blank">
                                                    <span class="elementor-screen-only">Instagram</span>
                                                    <i class="fab fa-instagram"></i>					</a>
                                            </span>
                                            @endif
                                            @if($siteDetail->twitter_link)
                                            <span class="elementor-grid-item">
                                                <a href="<?= $siteDetail->twitter_link ?>" class="elementor-icon elementor-social-icon elementor-social-icon-twitter elementor-repeater-item-0b2499b" target="_blank">
                                                    <span class="elementor-screen-only">Twitter</span>
                                                    <i class="fab fa-twitter"></i>
                                                </a>
                                            </span>
                                            @endif
                                            @if($siteDetail->youtube_link)
                                            <span class="elementor-grid-item">
                                                <a href="<?= $siteDetail->youtube_link ?>" class="elementor-icon elementor-social-icon elementor-social-icon-youtube elementor-repeater-item-961e5e6" target="_blank">
                                                    <span class="elementor-screen-only">Youtube</span>
                                                    <i class="fab fa-youtube"></i>					
                                                </a>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>