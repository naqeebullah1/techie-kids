<div class="clearfix"></div>
<div class="row ">
    <div class="col-md-12">
        <div class="card">
            <h4 class="bold" style="padding:0px 10px;font-size:18px;"><a class="toggler" href="#" onClick="toggleSearch('searchfields')">{{ __('Fetch Stripe Payment') }}<span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
            <?php
            $display = '';
            $requestData = [
                "School" => null,
                "Class" => null,
                "Parent Email" => null,
                "Child Name" => null,
                "Subscription ID" => null,
            ];
            if (Request::all()) {
                $requestData = Request::all();
                $isData = 1;
            }
            if(isset($isData)) {
                $display = '';
            }
            
//            dump($requestData);exit;
            extract($requestData, EXTR_PREFIX_ALL, "ex");
            ?>
            <div class="card-body" style="{{ $display }}" id="searchfields">
                <form method="post" id="searchform" accept-charset="utf-8" action="{{route('admin.admission.search_stripe')}}">
                    @csrf
                    <div class="row">
                        <input type="hidden" name="sort_field" class="sort_field" value="">
                        <input type="hidden" name="sort_order" class="sort_order" value="">
                        <div class="col-lg-2">
                            <select name="school_id" id="schoolname" class="form-control" onChange='loadClasses()' required>
                                <option value="">Select School</option>
                                @foreach($schools as $school)
                                
                                <option <?= @$ex_school_id == $school->id ? 'selected' : '' ?> value="{{ @$school->id }}">{{ $school->name }}</option>
                                
                                @endforeach
                            </select>
                                 
                        </div>
                        <div class="col-lg-2">
                           <select name="class_id" id="class_id" class="form-control" onChange='loadParents()' required>
                                <option value="">Select Class</option>
                                
                            </select>   
                        </div>
                        <div class="col-lg-2">
                            <select name="parent_id" id="parent_id" class="form-control" onChange='loadChild()' required>
                                <option value="">Select Parent</option>
                                @foreach($parents as $parent)
                                
                                <option <?= @$ex_parent_id == $parent->id ? 'selected' : '' ?> value="{{ @$parent->id }}">{{ $parent->last_name }} - {{ $parent->email }}</option>
                                
                                @endforeach
                            </select>  
                                
                        </div>
                        <div class="col-lg-2">
                            <select name="child_id" id="child_id" class="form-control">
                                <option value="">Select Child</option>
                                
                            </select> 

                        </div>
                        <div class="col-lg-4">
                            <input type="text" name="subscription_id" class="form-control" placeholder="Stripe Subscription ID" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="reset" name="reset" class="btn btn-success" style="margin-top:25px;" value="Reset" onClick="resetForm('searchform')">
                            <input type="submit" name="search" class="btn btn-primary" style="margin-top:25px;" value="Fetch Payment Status" id="searchBnt">
                            <br>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
function loadClasses() {
       // alert('1');
       var $url = "{{ route('admin.class-dd') }}";
        var sId = $('#schoolname').val();
        $url = $url + "?school_id=" + sId + "&class_id=";
        if(sId != "") {
            $('#class_id').html('<option value="">Select Class</option>');  
            $.get($url, function (data) {
                console.log(data.html);
                $('#class_id').html(data.html);
            });
        } else {
            $('#class_id').html('<option value="">Select Class</option>');   
        }
    }  
    
    function loadChild() {
       // alert('1');
       var $url = "{{ route('admin.child-dd') }}";
        var sId = $('#parent_id').val();
        $url = $url + "?parent_id=" + sId;
        if(sId != "") {
            $('#child_id').html('<option value="">Select Child</option>');  
            $.get($url, function (data) {
                console.log(data.html);
                $('#child_id').html(data.html);
            });
        } else {
            $('#child_id').html('<option value="">Select Class</option>');   
        }
    }  

</script>
