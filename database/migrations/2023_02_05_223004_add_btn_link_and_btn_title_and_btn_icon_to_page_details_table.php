<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBtnLinkAndBtnTitleAndBtnIconToPageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('page_details', function (Blueprint $table) {
            //
            $table->string('btn_link')->nullable();
            $table->string('btn_title')->nullable();
            $table->string('btn_icon')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('page_details', function (Blueprint $table) {
            //
        });
    }
}
