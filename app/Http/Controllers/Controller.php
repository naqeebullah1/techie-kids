<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Page;
use Illuminate\Support\Facades\View;
use App\Models\Footer;
use App\Models\Team;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function __construct()
      {
        //its just a dummy data object.
        $page = Page::with('pageDetails')->where('is_contact','=','1')->first();
        $footer = Footer::take(1)->first();
        $teams = Team::where('is_active', '=', 1)->get();
        // Sharing is caring
        View::share('contact', $page);
        View::share('footer', $footer);
        View::share('teams', $teams);
      }
  
}
