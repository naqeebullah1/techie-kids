@extends('layouts.front_theme')
@section('content')

<section class="junior__service bg-white pb--50 mt--50">
    <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section__title text-center pb--70">
                        <h1 class="">Privacy Policy</h1>
                    </div>
                </div>
            </div>
        </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?php echo $content['privacy'] ?>
            </div>
        </div>
    </div>
</div>
@endsection

