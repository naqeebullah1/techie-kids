<div class="container">
    <div class="row class__grid__page">
        <!-- Start Single Courses -->
        @forelse($schools as $school)
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="courses">
                <div class="courses__thumb">
                    <a href="<?= url('frontend/school-details/' . $school->id) ?>">
                        <img src="<?= asset('images/uploads/' . $school->image) ?>" onerror="this.src='{{asset('frontend/images')}}/logo-noimage.png'" alt="courses images">
                    </a>
                </div>
                <div class="courses__inner">
                    <div class="courses__wrap">
                        <div class="courses__date"><i class="fa fa-map-marker"></i><?= $school->address ?></div>
                        <div class="courses__content">
                            <h4><a href="<?= url('frontend/school-details/' . $school->id) ?>"><?= $school->name ?></a></h4>
                            <!--<p>Lor error sit volupta aclaud antium, toe ape sriam, ab illnv ritatis et quasi architecto beatae viliq.</p>-->

                            <div class="wel__btn text-right">
                                <a class="dcare__btn" href="<?= url('frontend/school-details/' . $school->id) ?>">View Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @empty
        <h2 class="text-danger">No School Available</h2>
        @endforelse
        <!-- End Single Courses -->

    </div>
    <!--    <div class="row">
            <div class="col-lg-12">
                <div class="dcare__pagination mt--80">
                    <ul class="dcare__page__list d-flex justify-content-center">
                        <li><a href="#"><span class="ti-angle-double-left"></span></a></li>
                        <li><a class="page" href="#">Prev</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
                        <li><a href="#">28</a></li>
                        <li><a class="page" href="#">Next</a></li>
                        <li><a href="#"><span class="ti-angle-double-right"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>-->
</div>