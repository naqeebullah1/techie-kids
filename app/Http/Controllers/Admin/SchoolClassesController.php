<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\User;
use App\Models\SchoolClass;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Mail;

class SchoolClassesController extends Controller {

    public function index(Request $request, $schoolId) {
        $school = School::find($schoolId);

        $classes = SchoolClass::where('school_id', $schoolId)->paginate(15);
//        dd($classes[0]->teacher);
        if ($request->ajax()) {
            $view = view('admin.school_classes.table', compact('classes'))
                    ->render();
            return response()->json(['html' => $view]);
        }
        return view('admin.school_classes.index', compact('classes', 'school'));
    }

    public function create($schoolId, $id = null) {
//        dd($id);
        $school = School::find($schoolId);
//        dd($school);
        $class = new SchoolClass();
        if ($id) {
            $class = SchoolClass::find($id);
        }
        $teachers = User::getList(2)->get();
//        dd();
//        dd($teachers->toArray());
        $view = view('admin.school_classes.form', compact('class', 'teachers', 'school'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function store(Request $request) {
//        dd($request->all());
        try {
            DB::beginTransaction();

            $record = $request->record;
            $record['start_on'] = date('Y-m-d', strtotime($record['start_on']));
            $record['end_on'] = "2099-01-01";
            $start_at = $record['start_at'];
            $record['start_at'] = date('H:i:00', strtotime($record['start_at']));
            $duration = $record['duration'] * 60;
            $end_at = date('H:i:00', strtotime($start_at) + $duration);
            $record['end_at'] = $end_at;
            unset($record['duration']);
            $id = $request->id;
            if (isset($record['weekly_off_days'])) {
                $record['weekly_off_days'] = implode(',', $record['weekly_off_days']);
            } else {
                $record['weekly_off_days'] = "";
            }
            if ($id) {
                $record['updated_by'] = auth()->user()->id;

                $record = SchoolClass::find($id)->update($record);
                $outcome = "School Class Updated Successfully";
            } else {
                $record['status'] = 1;
                $record = SchoolClass::create($record);
                $outcome = "School Class Created Successfully";
                // Send Email to Teacher

                $getTeacherId = SchoolClass::where(['id' => $record->id])->get()->first()['user_id'];
                $teacherData = User::where(['id' => $getTeacherId])->where(['status' => 1])->get()->first();

                $school_name = School::where(['id' => $record->school_id])->get()->first()['name'];
                $class_name = $record->title;


                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . url('/') . '" title="logo" target="_blank">
                            <img width="220" src="https://techiekidsclub.com/frontend/images/logo/techie_kids_logo.png" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            Hi ' . $teacherData["first_name"] . ', here are the details for the new class you\'ll be teaching:<br />

<strong>' . $school_name . '</strong><br /> <strong>' . date('M-d-Y', strtotime($record->start_on)) . ' ' . date('h:m:i a', strtotime($record->start_at)) . '</strong><br />Thanks for being part of our Techie Kids Club team!
                                        </p>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.' . str_replace(" ", "", strtolower(config('app.name'))) . '.com</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                if (isset($teacherData) && !empty($teacherData)) {
                    Mail::send(array(), array(), function ($message) use ($html, $teacherData, $school_name) {
                        $message->to($teacherData['email'])
                                ->subject($teacherData['first_name'] . ", you have a new class at $school_name")
                                ->setBody($html, 'text/html');
                    });
                }
            }

            DB::commit();
            return redirect()->back()->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function destroy($id) {
        $outcome = '';
        try {
            DB::beginTransaction();
            $record = SchoolClass::find($id);

            //ANY CHECK IF NEEDED
            /* if ($record->school_classes->count()) {
              return redirect()->back()->with('error', 'School is associated to this classes. Please delete Classes first');
              }
             * 
             */

            $record->delete();
            $outcome = "School Class deleted successfully.";

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

    public function loadStd($id) {
        $students = \App\Models\AdmissionDetail::where('school_class_id', $id)->get();
        $view = view('admin.school_classes.cls_std', compact('students'))
                ->render();
        return response()->json(['html' => $view]);
    }

}
