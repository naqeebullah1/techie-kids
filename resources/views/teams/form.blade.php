@csrf
{{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif --}}

<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="name" class="col-md-12 col-form-label text-md-left @error('name')  @enderror">Name</label>
            <input type="text" class="form-control " name="name" value="{{ old('name') ?? $team->name }}" required=""
                autocomplete="off" autofocus="">
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="title" class="col-md-12 col-form-label text-md-left @error('title')  @enderror">Speciality</label>
            <input type="text" class="form-control " name="title" value="{{ old('title') ?? $team->title }}" required=""
                autocomplete="off" autofocus="">
            @error('title')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="col-md-12">
        <label for="image" class="col-md-12 col-form-label text-md-left">Image <br>
            <span class="text-danger">Preferred image dimensions to upload is (Rendered aspect ratio:370*377)</span>
        </label>
        <input type="file" accept="image/png, image/gif, image/jpeg" class="dropify @error('image')  @enderror" @if($team->image)
        data-default-file="{{ asset('images/teams/'.$team->image) }}"
        @endif
            name="image">
        @error('image')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="col-md-12">
        <label for="section" class="col-md-12 col-form-label text-md-left">Content<span
                class="required">*</span></label>
        <textarea class="section form-control @error('section')  @enderror" name="section" required="">{{ old('section') ?? $team->description }}</textarea>
        @error('section')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>
<div class="form-group row mt-2">
    <div class="col-md-12 offset-md-12 text-right">
        <button id="save-form" type="submit" class="btn btn-primary">
            @isset ($create)
                {{ __('Create') }}
            @else
                {{ __('Update') }}
            @endisset
        </button>
    </div>
</div>
