<style>
    .js .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }

    .inputfile + label {
        max-width: 80%;
        font-size: 1.25rem;
        /* 20px */
        font-weight: 700;
        text-overflow: ellipsis;
        white-space: nowrap;
        cursor: pointer;
        display: inline-block;
        overflow: hidden;
        padding: 0.625rem 1.25rem;
        /* 10px 20px */
    }
    .no-js .inputfile + label {
        display: none;
    }

    .inputfile:focus + label,
    .inputfile.has-focus + label {
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }

    .inputfile + label * {
        /* pointer-events: none; */
        /* in case of FastClick lib use */
    }

    .inputfile + label svg {
        width: 1em;
        height: 1em;
        vertical-align: middle;
        fill: currentColor;
        margin-top: -0.25em;
        /* 4px */
        margin-right: 0.25em;
        /* 4px */
    }


    /* style 1 */

    .inputfile-1 + label {
        color: #f1e5e6;
        background-color: #07c25f;
    }

    .inputfile-1:focus + label,
    .inputfile-1.has-focus + label,
    .inputfile-1 + label:hover {
        background-color: #722040;
    }

</style>
@csrf
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="title" class="col-md-12 col-form-label text-md-left">Title</label>
            <input type="text" class="form-control " onchange="makeSlug(this.value)" name="category[title]" value="{{ $category->title }}" required="" autocomplete="off" autofocus="">
        </div>
    </div>
    <div class="col-md-12 mb-2">
        <div class="form-group-default required" aria-required="true">
            <label for="tags" class="col-md-12 col-form-label text-md-left font-weight-normal" style="font-size: 13px;">Tags</label>
            <input type="text" id="tags" class="form-control " name="category[tags]" value="{{ $category->tags }}">
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="link" class="col-md-12 col-form-label text-md-left">Link</label>
            <input type="url" id="link" class="form-control " name="category[link]" value="{{ $category->link }}" autocomplete="off" autofocus="">
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="title" class="col-md-12 col-form-label text-md-left">Description</label>
            <textarea style="height:100px !important;" class="section form-control" name="category[description]" required="" rows="5">{{ $category->description }}</textarea>
        </div>
    </div>
    @if($category->file)
    <div class="col-12 mb-2">
        <table class="table table-bordered table-sm table-small">
            <thead>
                <tr>
                    <th>File</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $files = explode(',', $category->file);
                ?>
                @foreach($files as $file)
                <tr>
                    <td><?= $file ?></td>
                    <td>
                        <?php
                        if ($file && file_exists(public_path() . '/images/thumbnails/' . $file)) {
                            $img = 'images/thumbnails/' . $file;
                            ?>
                            <a href="<?= url($img); ?>" class="btn btn-success btn-sm btn-small" download="">
                                <i class="fa fa-download"></i>
                            </a>
                            <?php
                        }
                        ?>
                        <a class="btn btn-danger btn-sm btn-small" href="javascript:;" onclick="return deletefile(this,<?= request()->id ?>, '<?= $file ?>')"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>

    </div>
    @endif
    <div class="col-12">
        <div class="box">
            <input type="file" name="image[]" 
                   @isset($create)
                   required=""
                   @endisset
                   id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple />
                   <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
            <!--            <h5>OR</h5>
                        <label for="file-1" style="display:flex;justify-content: center;align-items: center;width:100%;height: 100px" class="alert alert-success">
                            Drag File Here
                        </label>-->
        </div>
    </div>

</div>
<div class="form-group row mt-2">
    <div class="col-md-12 offset-md-12 text-right">
        <button id="save-form" type="submit" class="btn btn-primary">
            @isset($create)
            {{ __('Create') }}
            @else
            {{ __('Update') }}
            @endisset
        </button>
    </div>
</div>
<script type="text/javascript" src="{{ asset('js/file-input.js') }}"></script>
<script type="text/javascript">
                            function makeSlug($value) {
                                var $slug = $value.toLowerCase().replace(' ', '-');
                                $('#slug').val($slug);
                            }
                            function deletefile($this, $id, $fileName) {
                                $.get("{{ url('backend/dl-file-destroy') }}/" + $id + "/" + $fileName, function (data, status) {
//                                    alert("Data: " + data + "\nStatus: " + status);
                                });
                                $($this).parents('tr').remove();
                            }
</script>