<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdmissionDetail extends Model {

    use HasFactory;

    protected $guarded = [];

    public function admission_class() {
        return $this->belongsTo(SchoolClass::class,'school_class_id');
    }
    public function school() {
        return $this->belongsTo(School::class);
    }
    public function student() {
        return $this->belongsTo(Student::class);
    }

}
