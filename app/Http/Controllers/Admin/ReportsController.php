<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\School;
use App\Models\SchoolClass;
use App\Models\Student;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PurchaseOrdersExport;

class ReportsController extends Controller {

    public function searchAll() {
        $rec = 10;
        $user = auth()->user();
        $userId = $user->id;
        $roles = $user->ModelHasRoles->pluck('role_id')->toArray();

        if (request()->print) {
            $data = User::selectRaw('concat(users.first_name," ",users.last_name) as rm_name,
                    schools.name as school_name,
                    school_classes.title as class_title,
                    concat(teachers.first_name," ",teachers.last_name) as teacher_name,
                    concat(students.first_name," ",students.last_name) as student_name,
                    concat(parents.first_name," ",parents.last_name) as parent_name                    
                    ')
                    ->leftJoin('schools', 'schools.user_id', 'users.id')
                    ->leftJoin('states as school_state', 'school_state.id', 'schools.state_id')
                    ->leftJoin('cities as school_city', 'school_city.id', 'schools.city_id')
                    ->leftJoin('school_classes', 'school_classes.school_id', 'schools.id')
                    ->leftJoin('users as teachers', 'teachers.id', 'school_classes.user_id')
                    ->leftJoin('admission_details', 'admission_details.school_class_id', 'school_classes.id')
                    ->leftJoin('students', 'admission_details.student_id', 'students.id')
                    ->leftJoin('users as parents', 'parents.id', 'students.user_id');

            if (in_array(4, $roles)) {
                $data = $data->where('users.id', $userId);
            }
            if (in_array(2, $roles)) {
                $data = $data->where('teachers.id', $userId);
            }

            /*
             * RM
             */
            $keyword = request()->keyword;
            if (request()->only_this == '4') {
                $data = $data->where(function($q) use($keyword) {
                    return $q->orWhere(DB::raw('concat(users.first_name," ",users.last_name)'), 'like', '%' . $keyword . '%')
                                    ->orWhere('users.email', 'like', '%' . $keyword . '%');
                });
            }
            /*
             * Teachers
             */
            if (request()->only_this == '2') {
                $data = $data->where(function($q) use($keyword) {
                    return $q->orWhere(DB::raw('concat(teachers.first_name," ",teachers.last_name)'), 'like', '%' . $keyword . '%')
                                    ->orWhere('teachers.email', 'like', '%' . $keyword . '%');
                });
            }
            /*
             * Parents
             */
            if (request()->only_this == '3') {
                $data = $data->where(function($q) use($keyword) {
                    return $q->orWhere(DB::raw('concat(parents.first_name," ",parents.last_name)'), 'like', '%' . $keyword . '%')
                                    ->orWhere('parents.email', 'like', '%' . $keyword . '%');
                });
            }




            if (request()->only_this == 'c') {
                $data = $data
                        ->where(function($q) use($keyword) {
                    return $q->where('school_classes.title', 'like', '%' . $keyword . '%')
                            ->orWhere(DB::raw('concat(teachers.first_name," ",teachers.last_name)'), 'like', '%' . $keyword . '%')
                            ->orWhere('schools.name', 'like', '%' . $keyword . '%');
                });
            }
            if (request()->only_this == 'sc') {
                $data = $data
                        ->where(function($q) use($keyword) {
                    return $q->where('schools.name', 'like', '%' . $keyword . '%')
                            ->orWhere('schools.address', 'like', '%' . $keyword . '%')
                            ->orWhere('schools.email', 'like', '%' . $keyword . '%')
                            ->orWhere('schools.contact', 'like', '%' . $keyword . '%')
                            ->orWhere('school_state.state', 'like', '%' . $keyword . '%')
                            ->orWhere('school_city.city', 'like', '%' . $keyword . '%');
                });
            }
            if (request()->only_this == 's') {
                $data = $data->where(function($q) use($keyword) {
                    return $q->where(DB::raw('concat(students.first_name," ",students.last_name)'), 'like', '%' . $keyword . '%')
                                    ->orWhere(DB::raw('concat(parents.first_name," ",parents.last_name)'), 'like', '%' . $keyword . '%')
                                    ->orWhere('parents.email', 'like', '%' . $keyword . '%');
                });
            }

            return Excel::download(new PurchaseOrdersExport($data), 'summary-report.xlsx');
//            return redirect()->back();
        }

        if (request()->keyword) {

            $keyword = request()->keyword;
            /*
             * Reginal Managers Starts
             */
            $managers = User::where(function($q) use($keyword) {
                        return $q->orWhere(DB::raw('concat(first_name," ",last_name)'), 'like', '%' . $keyword . '%')
                                        ->orWhere('users.email', 'like', '%' . $keyword . '%');
                    })->whereHas('ModelHasRoles', function($q) {
                $q->where('role_id', 4);
            });
//            dd($roles);
            if (in_array(4, $roles)) {
                $managers = $managers->where('users.id', $userId);
            }
            if (in_array(2, $roles)) {
                $managers = $managers
                        ->join('schools as s', 's.user_id', 'users.id')
                        ->join('school_classes as sc', 'sc.user_id', 's.id')
                        ->where('sc.user_id', $userId)
                        ->groupBy('users.id');
            }
            $managers = $managers->paginate($rec);

            if (request()->type == '4') {
                if (request()->ajax()) {
                    $view = view('admin.reports.includes.regional_director_table', compact('managers'))
                            ->render();
                    return response()->json(['html' => $view, 'type' => request()->type]);
                }
            }

            /*
             * Regional Manager Ends
             */

            /*
             * Teacher Starts
             */
            $teachers = User::where(function($q) use($keyword) {
                        return $q->orWhere(DB::raw('concat(first_name," ",last_name)'), 'like', '%' . $keyword . '%')
                                        ->orWhere('users.email', 'like', '%' . $keyword . '%');
                    })->whereHas('ModelHasRoles', function($q) {
                $q->where('role_id', 2);
            });
//            if (in_array(2, $roles)) {
//                $teachers = $teachers->where('users.id', $userId);
//            }

            if (in_array(4, $roles)) {
                $teachers = $teachers
                        ->join('school_classes as sc', 'sc.user_id', 'users.id')
                        ->join('schools as s', 's.id', 'sc.school_id')
                        ->where('s.user_id', $userId)
                        ->groupBy('users.id');
//                dd($userId);
            }
            if (in_array(2, $roles)) {
                $teachers = $teachers->where('users.id', $userId);
            }
            $teachers = $teachers->paginate($rec);

            /*
             * Teacher Ends
             */
            if (request()->type == '2') {
                if (request()->ajax()) {
                    $view = view('admin.reports.includes.teachers_table', compact('teachers'))
                            ->render();
                    return response()->json(['html' => $view, 'type' => request()->type]);
                }
            }

            /*
             * Parents STARTS
             */


            $parents = User::where(function($q) use($keyword) {
                        return $q->orWhere(DB::raw('concat(first_name," ",last_name)'), 'like', '%' . $keyword . '%')
                                        ->orWhere('users.email', 'like', '%' . $keyword . '%');
                    })->whereHas('ModelHasRoles', function($q) {
                $q->where('role_id', 3);
            });

            if (in_array(4, $roles)) {
                $parents = $parents
                        ->join('admissions as a', 'a.parent_id', 'users.id')
                        ->join('admission_details as ad', 'ad.admission_id', 'a.id')
//                        ->join('school_classes as sc', 'sc.user_id', 'users.id')
                        ->join('schools as s', 's.id', 'ad.school_id')
                        ->where('s.user_id', $userId)
                        ->groupBy('users.id');
            }
            if (in_array(2, $roles)) {
                $parents = $parents
                        ->join('admissions as a', 'a.parent_id', 'users.id')
                        ->join('admission_details as ad', 'ad.admission_id', 'a.id')
                        ->join('school_classes as sc', 'sc.user_id', 'users.id')
                        ->where('sc.user_id', $userId)
                        ->groupBy('users.id');
            }

            $parents = $parents->paginate($rec);



            if (request()->type == '3') {
                if (request()->ajax()) {
                    $view = view('admin.reports.includes.parents_table', compact('parents'))
                            ->render();
                    return response()->json(['html' => $view, 'type' => request()->type]);
                }
            }

            /*
             * PARENT ENDS
             */

            /*
             * Schools STARTS
             */
            $schools = School::select('schools.*', 'states.state', 'cities.city')
                    ->join('states', 'states.id', 'schools.state_id')
                    ->join('cities', 'cities.id', 'schools.city_id')
                    ->where(function($q) use($keyword) {
                return $q->where('name', 'like', '%' . $keyword . '%')
                        ->orWhere('address', 'like', '%' . $keyword . '%')
                        ->orWhere('schools.email', 'like', '%' . $keyword . '%')
                        ->orWhere('contact', 'like', '%' . $keyword . '%')
                        ->orWhere('states.state', 'like', '%' . $keyword . '%')
                        ->orWhere('cities.city', 'like', '%' . $keyword . '%');
            });
            if (in_array(4, $roles)) {
                $schools = $schools->where('schools.user_id', $userId);
            }
            if (in_array(2, $roles)) {
                $schools = $schools
                        ->join('school_classes as sc', 'sc.school_id', 'schools.id')
                        ->where('sc.user_id', $userId)
                        ->groupBy('schools.id');
            }

            $schools = $schools->paginate($rec);

            /*
             * SCHOOLS ENDS
             */
            if (request()->type == 'schools') {
                if (request()->ajax()) {
                    $view = view('admin.reports.includes.schools_table', compact('schools'))
                            ->render();
                    return response()->json(['html' => $view, 'type' => request()->type]);
                }
            }
            /*
             * Students STarts
             */
            $students = Student::select(
                            'students.*', 'parents.email','parents.phone1' , 'parents.id as parentId', DB::raw('concat(parents.first_name," ",parents.last_name) as parent'))
                    ->join('users as parents', 'parents.id', 'students.user_id')
                    ->where(function($q) use($keyword) {
                return $q->where(DB::raw('concat(students.first_name," ",students.last_name)'), 'like', '%' . $keyword . '%')
                        ->orWhere(DB::raw('concat(parents.first_name," ",parents.last_name)'), 'like', '%' . $keyword . '%')
                        ->orWhere('parents.email', 'like', '%' . $keyword . '%');
            });
            
            if (in_array(4, $roles)) {
                $students = $students
                        ->join('admission_details as ad', 'ad.student_id', 'students.id')
                        ->join('schools as schools', 'ad.school_id', 'schools.id')
                        ->where('schools.user_id', $userId);
            }
            if (in_array(2, $roles)) {
                $students = $students
                        ->join('admission_details as ad', 'ad.student_id', 'students.id')
                        ->join('schools as schools', 'ad.school_id', 'schools.id')
                        ->join('school_classes as sc', 'sc.school_id', 'schools.id')
                        ->where('sc.user_id', $userId);
            }

            $students = $students->paginate($rec);


            if (request()->type == 'students') {
                if (request()->ajax()) {
                    $view = view('admin.reports.includes.students_table', compact('students'))
                            ->render();
                    return response()->json(['html' => $view, 'type' => request()->type]);
                }
            }
            /*
             * STudent Ends
             */

            /*
             * Classes Starts 
             */
            $classes = SchoolClass::select('school_classes.*', 'schools.name as school', DB::raw('concat(teachers.first_name," ",teachers.last_name) as teacher'))
                    ->join('users as teachers', 'teachers.id', 'school_classes.user_id')
                    ->join('schools', 'schools.id', 'school_classes.school_id')
                    ->where(function($q) use($keyword) {
                return $q->where('title', 'like', '%' . $keyword . '%')
                        ->orWhere(DB::raw('concat(teachers.first_name," ",teachers.last_name)'), 'like', '%' . $keyword . '%')
                        ->orWhere('schools.name', 'like', '%' . $keyword . '%');
            });

            if (in_array(4, $roles)) {
                $classes = $classes->where('schools.user_id', $userId);
            }
            if (in_array(2, $roles)) {
                $classes = $classes->where('school_classes.user_id', $userId);
            }

            $classes = $classes->paginate($rec);

            if (request()->type == 'school-classes') {
                if (request()->ajax()) {
                    $view = view('admin.reports.includes.school_classes_table', compact('classes'))
                            ->render();
                    return response()->json(['html' => $view, 'type' => request()->type]);
                }
            }
            /*
             * Classes Ends
             */

            $view = view('admin.reports.includes.all_tables', compact('managers', 'teachers', 'parents', 'schools', 'classes', 'students'))
                    ->render();
            return response()->json(['html' => $view, 'type' => 'main']);

//            dd(request()->all());
        }
        return view('admin.reports.search_all');
//        return view('admin.reports.search_all', compact('users', 'schools', 'classes'));
    }

}
