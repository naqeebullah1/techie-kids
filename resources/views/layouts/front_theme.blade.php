<!doctype html>
<html class="no-js" lang="zxx">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Techie Kids Club &#8211; Coding and Robotics for Kids Preschool &#8211; Third Grade</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicons -->
        <link rel="shortcut icon" href="<?= asset('/frontend') ?>/images/favicon.ico">
        <link rel="apple-touch-icon" href="<?= asset('/frontend') ?>/images/icon.png">
        <!-- Google font (font-family: 'Dosis', Roboto;) -->
        <link href="https://fonts.googleapis.com/css?family=Dosis:400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">


        <!-- Stylesheets -->
        <link rel="stylesheet" href="<?= asset('/frontend') ?>/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <link rel="stylesheet" href="<?= asset('/frontend') ?>/css/plugins.css">
        <link rel="stylesheet" href="<?= asset('/frontend') ?>/style.css">

        <!-- Cusom css -->
        <link rel="stylesheet" href="<?= asset('/frontend') ?>/css/custom.css">
<link href="{{ asset('plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css" media="screen">
        <!-- Modernizer js -->
        <script src="<?= asset('/frontend') ?>/js/vendor/modernizr-3.5.0.min.js"></script>
         <link rel="stylesheet" href="{{asset('dropify/dist/css/dropify.min.css')}}">
         <link rel="stylesheet" href="{{asset('dropify/dist/css/dropify.min.css')}}">
         <link href="{{asset('frontend/js/toaster')}}/build/toastr.css" rel="stylesheet" type="text/css" />
        <!--<script src="http://localhost/techie_kids/js/jquery.js"></script>-->
        <style>
            .error {
                color:#dc3545!important;
            }
        </style>
        @yield('style')
    </head>
    <body>
        @include('cookieConsent::index')
        <!--[if lte IE 9]>
                <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <!-- <div class="fakeloader"></div> -->

        <!-- Main wrapper -->
        <div class="wrapper" id="wrapper">
            @include('layouts.partials.header')
            @yield('content')


            @include('layouts.partials.footer')
            <!-- Cartbox -->
            <div class="cartbox-wrap">
                <div class="cartbox text-right">
                    <button class="cartbox-close"><i class="zmdi zmdi-close"></i></button>
                    <div class="cart-container cartbox__inner text-left">
                        @include('frontend.cart',['cart'=>(session('cart_info'))?session('cart_info'):[]])
                    </div>
                </div>
            </div>
            <!-- //Cartbox -->

        </div><!-- //Main wrapper -->

        <!-- JS Files -->
        <script src="<?= asset('/frontend') ?>/js/vendor/jquery-3.2.1.min.js"></script>
        <script src="<?= asset('/frontend') ?>/js/popper.min.js"></script>
        <script src="<?= asset('/frontend') ?>/js/bootstrap.min.js"></script>
        <script src="<?= asset('/frontend') ?>/js/plugins.js"></script>
        <script src="<?= asset('/frontend') ?>/js/active.js"></script>
                <script src="{{asset('dropify/dist/js/dropify.min.js')}}"></script> 
                 <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
                 <script src="{{asset('frontend/js/toaster/toastr.js')}}" type="text/javascript"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

        @yield('script')
        <script>
$(document).ready(function () {
    $('.customer-logos').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: true,
        responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 3
                }
            }]
    });
    $('.kids-code-slider').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: true,
        dots: true,
        pauseOnHover: true,
        responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 3
                }
            }]
    });
    $('.testimonial-carousel').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5500,
        arrows: false,
        dots: false,
        pauseOnHover: true,
        responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 1
                }
            }]
    });
});

const btns = document.querySelectorAll(".acc-btn");

// fn
function accordion() {
    // this = the btn | icon & bg changed
    this.classList.toggle("is-open");

    // the acc-content
    const content = this.nextElementSibling;

    // IF open, close | else open
    if (content.style.maxHeight)
        content.style.maxHeight = null;
    else
        content.style.maxHeight = content.scrollHeight + "px";
}

// event
btns.forEach((el) => el.addEventListener("click", accordion));
        </script>
    <script>
    $(".readmore").on("click",function(){
        //alert($(this).parent().parent().find('.content_hidden').text());
       $(this).parent().parent().find('.content_hidden').removeClass("d-none"); 
       $(this).parent().addClass("d-none");
    });
    $(".readless").on("click",function(){
        //alert($(this).parent().parent().find('.content_show').text());
       $(this).parent().parent().find('.content_show').removeClass("d-none"); 
       $(this).parent().addClass("d-none");
    });
</script>
<script>
    $(".phone").mask("(999) 999-9999", { autoclear: false });
    $(function(){
  var hash = window.location.hash;
  hash && $('ul.nav a[href="' + hash + '"]').tab('show');

  $('.nav-tabs a').click(function (e) {
    $(this).tab('show');
    /*var scrollmem = $('body').scrollTop();
    window.location.hash = this.hash;
    $('html,body').scrollTop(scrollmem);*/
  });
});

            $(".toggle-password").click(function() {
              $(this).toggleClass("fa-eye fa-eye-slash");
              var input = $($(this).attr("toggle"));
              if (input.attr("type") == "password") {
                input.attr("type", "text");
              } else {
                input.attr("type", "password");
              }
            });
          </script> 
    </body>

</html>
