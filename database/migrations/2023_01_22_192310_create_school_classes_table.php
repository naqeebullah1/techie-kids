<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolClassesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('school_classes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('school_id')->unsigned();
            $table->foreignId('user_id')->unsigned();//teacher Id
            $table->string('title');
            $table->date('start_on');
            $table->date('end_on');
            
            $table->time('start_at');
            $table->time('end_at');
            $table->string('weekly_off_days');
            
            $table->float('class_charges',6,2);
            $table->tinyInteger('status');
//            $Names = array( 0=>"Sun", 1=>"Mon", 2=>"Tue", 3=>"Wed", 4=>"Thu", 5=>"Fri", 6=>"Sat" );

            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('school_classes');
    }

}
