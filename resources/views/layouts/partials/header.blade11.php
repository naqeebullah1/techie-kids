<header class="top_panel_wrap top_panel_style_3 scheme_original">

    <div class="top_panel_middle">
        <div class="content_wrap">
            <div class="contact_logo">
                <div class="logo">
                    <address href="index.html">
                        <img src="<?= asset('images/footer-logo.png') ?>" class="logo_main" alt=""
                                width="202">
                    </address>
                </div>
            </div>
            <div class="menu_main_wrap">
                <nav class="menu_main_nav_area">
                    <ul id="menu_main" class="menu_main_nav">
                         <?php
                        $menus = menus();
                        ?>
                        @foreach ($menus as $menu)
                            <li class="menu-item"><a
                                    href="<?= url('page/' . $menu->slug) ?>"><span class="{{ $menu->slug }}">{{ $menu->title }}</span></a>
                                    @if($menu->slug == 'about')
                                      <ul class="sub-menu">
                                          <li class="menu-item"><a href=""><span class="{{ $menu->slug }}">Our Team</span></a>
                                          <ul class="sub-menu">
                                        @foreach ($teams as $team)
                                            <li class="menu-item {{ $menu->slug }}"><a
                                                    href="{{ route('view-team-member', $team->id) }}"><span class="{{ $menu->slug }}">{{ $team->name }}</span></a>
                                            </li>
                                        @endforeach
                                        </ul>
                                        </li>
                                    </ul>
                                    @endif
                                    </li>
                        @endforeach
                        <!--<li class="menu-item"><a href="<?= url('page/home') ?>"><span>Home</span></a>-->
                        <!--</li>-->
                        <!--<li class="menu-item">-->
                        <!--    <a href="<?= url('page/services') ?>"><span>Services</span></a>-->
                        <!--</li>-->
                        <!--<li class="menu-item menu-item-has-children"><a-->
                        <!--        href="<?= url('page/about') ?>"><span>About</span></a>-->
                        <!--    <ul class="sub-menu">-->
                        <!--        <li class="menu-item"><a href=""><span>Our Team</span></a>-->
                        <!--            <ul class="sub-menu">-->
                        <!--                @foreach ($teams as $team)-->
                        <!--                    <li class="menu-item"><a-->
                        <!--                            href="{{ route('view-team-member', $team->id) }}"><span>{{ $team->name }}</span></a>-->
                        <!--                    </li>-->
                        <!--                @endforeach-->
                        <!--            </ul>-->
                        <!--        </li>-->
                        <!--    </ul>-->
                        <!--</li>-->
                        <!--<li class="menu-item"><a href="<?= url('page/contacts') ?>"><span>Contacts</span></a>-->
                        <!--</li>-->
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    </div>
</header>
<div class="header_mobile header-transparent">
    <div class="content_wrap">
        <div class="menu_button icon-menu"></div>
        <div class="logo">
            <a href="<?php echo url('/'); ?>">
                <img src="<?= asset('images/footer-logo.png') ?>" class="logo_main" alt=""
                    width="202" height="49">
            </a>
        </div>
    </div>
    <div class="side_wrap">
        <div class="close">Close</div>
        <div class="panel_top">
            <nav class="menu_main_nav_area">
                <nav class="menu_main_nav_area">
                    <ul id="menu_main" class="menu_main_nav">
                        @foreach ($menus as $menu)
                            <li class="menu-item"><a
                                    href="<?= url('page/' . $menu->slug) ?>"><span class="{{ $menu->slug }}">{{ $menu->title }}</span></a>
                                    @if($menu->slug == 'about')
                                      <ul class="sub-menu">
                                          <li class="menu-item"><a href=""><span class="{{ $menu->slug }}">Our Team</span></a>
                                          <ul class="sub-menu">
                                        @foreach ($teams as $team)
                                            <li class="menu-item {{ $menu->slug }}"><a
                                                    href="{{ route('view-team-member', $team->id) }}"><span class="{{ $menu->slug }}">{{ $team->name }}</span></a>
                                            </li>
                                        @endforeach
                                        </ul>
                                        </li>
                                    </ul>
                                    @endif
                                    </li>
                        @endforeach
                        <!--<li class="menu-item"><a href="<?= url('page/home') ?>"><span>Home</span></a>-->
                        <!--</li>-->
                        <!--<li class="menu-item">-->
                        <!--    <a href="<?= url('page/services') ?>"><span>Services</span></a>-->
                        <!--</li>-->
                        <!--<li class="menu-item menu-item-has-children"><a-->
                        <!--        href="<?= url('page/about') ?>"><span>About</span></a>-->
                        <!--    <ul class="sub-menu">-->
                        <!--        <li class="menu-item"><a href=""><span>Our Team</span></a>-->
                        <!--            <ul class="sub-menu">-->
                        <!--                @foreach ($teams as $team)-->
                        <!--                    <li class="menu-item"><a-->
                        <!--                            href="{{ route('view-team-member', $team->id) }}"><span>{{ $team->name }}</span></a>-->
                        <!--                    </li>-->
                        <!--                @endforeach-->
                        <!--            </ul>-->
                        <!--        </li>-->
                        <!--    </ul>-->
                        <!--</li>-->
                        <!--<li class="menu-item"><a href="<?= url('page/contacts') ?>"><span>Contacts</span></a>-->
                        <!--</li>-->
                    </ul>
                    {{-- <ul id="menu_main_mobile" class="menu_main_nav">
                        <?php
                        $menus = menus();
                        ?>
                        @foreach ($menus as $menu)
                            <li class="menu-item"><a
                                    href="<?= url('page/' . $menu->slug) ?>"><span>{{ $menu->title }}</span></a></li>
                        @endforeach
                    </ul>
                </nav> --}}
                </nav>
        </div>
    </div>
    <div class="mask"></div>
</div>
