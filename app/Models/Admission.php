<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admission extends Model {

    use HasFactory;

    protected $guarded = [];

    public function details() {
        return $this->hasMany(AdmissionDetail::class);
    }

    public function admission_parent() {
        return $this->belongsTo(User::class, 'parent_id');
    }

}
