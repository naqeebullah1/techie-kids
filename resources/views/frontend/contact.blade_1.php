@extends('layouts.front_theme')
@section('content')
    <!--  Page Content, class footer-fixed if footer is fixed  -->
    <div id="page-content" class="header-static footer-fixed">
        <!--  Slider  -->
        <div id="flexslider" class="fullpage-wrap small">
            <ul class="slides">
                <?php
                $image = 'img' . rand(1, 5) . '.jpeg';
                ?>
                <li class="flex-active-slide"
                    style="background-image: url('{{ asset('login/images/' . $image) }}'); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
                    <div class="container text text-center">
                        <h1 class="white margin-bottom-small">Contact Us</h1>
                    </div>
                    <div class="gradient dark"></div>
                </li>
                <ol class="breadcrumb">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li class="active">Contact</li>
                </ol>
            </ul>
        </div>
        <!--  END Slider  -->
        <div class="page_content_wrap page_paddings_no">
            <div class="content_wrap">
                <div class="content">
                    <article class="post_item post_item_single page hentry">
                        <section class="post_content">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <h2
                                                class="sc_title sc_title_regular sc_align_center margin_top_huge margin_bottom_tiny centext">
                                                How To Find Us</h2>
                                            <h3 class="vc_custom_heading">Address and Direction</h3>
                                            <div
                                                class="columns_wrap sc_columns columns_nofluid autoheight sc_columns_count_2">
                                                <div class="column-1_2 sc_column_item centext">
                                                    <div class="sc_column_item_inner bgimage_column"></div>
                                                </div>
                                                <div class="column-1_2 sc_column_item">
                                                    <div id="sc_services_604_wrap" class="sc_services_wrap">
                                                        <div id="sc_services_604"
                                                            class="sc_services sc_services_style_services-2 sc_services_type_icons margin_top_medium alignleft">
                                                            <div id="sc_services_604_1" class="sc_services_item">
                                                                <span class="sc_icon icon-location-light"></span>
                                                                <div class="sc_services_item_content">
                                                                    <h4 class="sc_services_item_title">Our Adress</h4>
                                                                    <div class="sc_services_item_description">
                                                                        <p>528 tenth Avenue, Boston, BT 58965</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="sc_services_604_2" class="sc_services_item">
                                                                <span class="sc_icon icon-mobile-light"></span>
                                                                <div class="sc_services_item_content">
                                                                    <h4 class="sc_services_item_title">Phone</h4>
                                                                    <div class="sc_services_item_description">
                                                                        <p>Manager +1 800 125 65 24</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="sc_services_604_3" class="sc_services_item">
                                                                <span class="sc_icon icon-calendar-light"></span>
                                                                <div class="sc_services_item_content">
                                                                    <h4 class="sc_services_item_title">Open Hours</h4>
                                                                    <div class="sc_services_item_description">
                                                                        <p>Mn - St: 8:00am - 9:00pm Sn: Closed</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="contact_form_popup" class="sc_popup mfp-with-anim mfp-hide">
                                                        <div id="sc_form_020_wrap" class="sc_form_wrap">
                                                            <div id="sc_form_020" class="sc_form sc_form_style_form_1">
                                                                <form id="sc_form_020_form" data-formtype="form_1"
                                                                    method="post" action="includes/sendmail.php">
                                                                    <div class="sc_form_info">
                                                                        <div class="sc_form_item sc_form_field label_over">
                                                                            <label class="required"
                                                                                for="sc_form_username">Name</label>
                                                                            <input id="sc_form_username" type="text"
                                                                                name="username" placeholder="Name *">
                                                                        </div>
                                                                        <div class="sc_form_item sc_form_field label_over">
                                                                            <label class="required"
                                                                                for="sc_form_email">E-mail</label>
                                                                            <input id="sc_form_email" type="text"
                                                                                name="email" placeholder="E-mail *">
                                                                        </div>
                                                                    </div>
                                                                    <div class="sc_form_item sc_form_message label_over">
                                                                        <label class="required"
                                                                            for="sc_form_message">Message</label>
                                                                        <textarea data-autoresize rows="1" id="sc_form_message" name="message" placeholder="Message"></textarea>
                                                                    </div>
                                                                    <div class="sc_form_item sc_form_button">
                                                                        <button>Send Message</button>
                                                                    </div>
                                                                    <div class="result sc_infobox"></div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="#contact_form_popup"
                                                        class="sc_button sc_button_square sc_button_style_filled sc_button_size_medium aligncenter margin_bottom_medium sc_popup_link">Use
                                                        contact form</a>
                                                    <div class="vc_empty_space space_20p">
                                                        <span class="vc_empty_space_inner"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true"
                                class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div id="sc_googlemap_961" class="sc_googlemap" data-zoom="16"
                                                data-style="ultra_light">
                                                <div id="sc_googlemap_961_1" class="sc_googlemap_marker"
                                                    data-title="New York"
                                                    data-description="&lt;div class=&quot;wpb_text_column wpb_content_element &quot; &gt; &lt;div class=&quot;wpb_wrapper&quot;&gt; &lt;p&gt;New York&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;"
                                                    data-address="New York" data-latlng=""
                                                    data-point="images/map_marker.png"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row-full-width"></div>
                        </section>
                    </article>
                    <section class="related_wrap related_wrap_empty"></section>
                </div>
            </div>
        </div>
        {{-- <div id="page-wrap" class="content-section fullpage-wrap">
            <div class="row margin-leftright-null">
                <div class="container">
                    <!--  Contact Info  -->
                    <div class="col-md-6 padding-leftright-null">
                        <div class="text">
                            <h2 class="margin-bottom-null title line left">Get in touch Person</h2>
                            <p class="heading center grey margin-bottom-null">Joseph Kelly</p>
                            <div class="padding-onlytop-md">
                                <!--<p class="margin-bottom">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem harum aspernatur sapiente error, voluptas fuga, laudantium ullam magni fugit. Qui!</p>-->
                                <p>
                                    <!--<span class="contact-info">Address <em>322 Moon St, Venice
                                                Italy, 1231</em></span><br>--><span class="contact-info">Phone <em>(813)
                                            405-9333</em></span><br><span class="contact-info">Email <a
                                            href="mailto:jkelly@harlaxtonhomes.com"><em>jkelly@harlaxtonhomes.com</em></a></span>
                                </p>
                                <!--<p class="margin-md-bottom-null"><span class="contact-info">Monday to Friday <em>9.00 am to 12.00 pm</em></span><br><span class="contact-info">Saturday from <em>9.00 am to 12.00 pm</em></span></p>-->
                            </div>
                        </div>
                    </div>
                    <!--  END Contact Info -->
                    <!--  Input Form  -->
                    <div class="col-md-6 padding-leftright-null">
                        <div class="text padding-onlybottom-sm padding-md-top-null">
                            <form id="contact-form" action="<?= url('send-message') ?>" method="post"
                                class="padding-onlytop-md padding-md-topbottom-null">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <input class="form-field" name="name" id="name" type="text"
                                            placeholder="Name">
                                    </div>
                                    <div class="col-md-12">
                                        <input class="form-field" name="mail" id="mail" type="text"
                                            placeholder="Email">
                                    </div>
                                    <div class="col-md-12">
                                        <input class="form-field" name="subjectForm" id="subjectForm" type="text"
                                            placeholder="Subject">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea class="form-field" name="messageForm" id="messageForm" rows="6" placeholder="Your Message"></textarea>
                                        <div class="submit-area padding-onlytop-sm">
                                            <input type="submit" id="submit-contact" class="btn-alt" value="Send Message">
                                            <div id="msg" class="message"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--  END Input Form  -->
                </div>
            </div>
            <div class="row margin-leftright-null">
                <!-- Map. Settings in assets/js/maps.js -->
                <div class="col-md-12 padding-leftright-null map">
                    <div id="map"></div>
                </div>
            </div>
        </div> --}}
    </div>
    <!--  END Page Content, class footer-fixed if footer is fixed  -->
@endsection
