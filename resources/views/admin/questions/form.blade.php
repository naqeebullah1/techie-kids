<div class="row">
    <div class="col-md-12">
        <a href="<?= route('admin.questions.index') ?>" class="pull-right mb-2 btn btn-success">Go Back</a>

        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">
                    @if($type=='create')
                    Create FAQ
                    @else
                    Update FAQ
                    @endif
                </div>
            </div>
            <div class="card-body">
                <form method="post" enctype="multipart/form-data" action="{{ route('admin.questions.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label class="col-12">FAQ For</label>
                            <label>
                                
                                <input @if($service->faq_for=='parents') checked @endif type="radio" value="parents" required="" name="faq_for"/> Parents
                            </label>
                            <label>
                                <input @if($service->faq_for=='schools') checked @endif type="radio" value="schools" name="faq_for"/> Schools

                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label>Question</label>
                            <input type="hidden" class="form-control" value="{{ $type }}" name="id"/>
                            <input type="text" onchange="" class="form-control" value="{{ $service->question }}" name="question"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label>Answer</label>
                            <textarea class="form-control ckeditor" name="answer"><?= $service->answer ?></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12 text-right">
                            <button type="submit" class="mt-2 btn btn-success">
                                @if($type=='create')
                                Add
                                @else
                                Update
                                @endif
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>