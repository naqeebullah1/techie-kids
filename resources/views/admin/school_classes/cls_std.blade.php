<div class="d-flex">
    <h4 style="flex-grow: 1" class="mb-2">Class Students</h4>
    <a style="align-self: center" class="btn btn-danger" href="javascript:" onclick="removeDetail()">Close</a>
</div>
<table class="sortable table table-bordered table-small">
    <thead>
        <tr>
            <th>Image</th>
            <th>First Name</th>
            <th>Last Name</th>
            <?php
            $roles = auth_user_roles();
            ?>
            @if(!in_array(2,$roles))
            <th>Parent</th>
            <th>Email</th>
            <th>Phone</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach($students as $city)
        <tr>
            <td>
                @if($city->student->image)
                <img src="<?= asset('images/uploads/' . $city->student->image) ?>" width="100px">
                @else
                <img src="<?= asset('images/uploads/no-image.jpg') ?>" width="100px">
                @endif
            </td>
            <td>
                <?= $city->student->first_name ?>
            </td>
            <td>
                <?= $city->student->last_name ?>
            </td>
            
            @if(!in_array(2,$roles))
            <td>
                <?= $city->student->user->first_name . ' ' . $city->student->user->last_name ?>
            </td>
            <td>
                <?= $city->student->user->email ?>
            </td>
            <td>
                <?= $city->student->user->phone1 ?>
            </td>
            @endif

        </tr>
        @endforeach
        @can('page.edit')
        <tr>

            <td colspan="6">
                <input type="submit" value="Update" class="btn btn-success m-auto d-block"/>
            </td>

        </tr>    
        @endcan
    </tbody>
</table>
