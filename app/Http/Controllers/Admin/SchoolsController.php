<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\State;
use App\Models\City;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class SchoolsController extends Controller {

    public function index(Request $request) {
        $schools = School::getList()->paginate(15);
        if ($request->ajax()) {
            $view = view('admin.schools.table', compact('schools'))
                    ->render();
            return response()->json(['html' => $view]);
        }
        $roles = auth()->user()->ModelHasRoles->pluck('role_id')->toArray();

        return view('admin.schools.index', compact('schools','roles'));
    }

    public function create($id = null) {
        $school = new School();
        if ($id) {
            $school = School::find($id);
        }
        $states = State::dropdown()->getAll();

        $cities = [];
        if ($school->state_id) {
            $sId = $school->state_id;
            $cities = $this->citiesList($sId);
        }
        $managers = User::getList(4)->get();

        $view = view('admin.schools.form', compact('school', 'states', 'cities', 'managers'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function citiesDD() {
        $sId = request()->state_id;
        $value = request()->city_id;

        $options = $this->citiesList($sId);
        $view = view('partials.dropdowns', compact('options', 'value'))
                ->render();
//        $empty="<option>Select</option>"
        return response()->json(['html' => $view]);
    }

    public function citiesList($sId) {
        $sId = State::find($sId)->state_code;
        $cities = City::dropdown()
                ->whereState($sId)->orderBy('city','ASC');
        if (request()->ignore_without_schools) {
            $getSchools = School::select('city_id')->whereHas('school_classes', function($q) {
                //return $q->whereDate('school_classes.start_on', '>', date('Y-m-d'));
                return $q->whereNotNull('school_classes.start_on');
            })->groupBy('city_id')->get();
            $cityWithData = $getSchools->pluck('city_id')->toArray();
            $cities = $cities->whereIn('id', $cityWithData);
        }
        return $cities->getAll();
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $record = $request->record;
//            dd($record);
            $id = $request->id;

            if ($request->hasFile('image')) {

                $originName = $request->file('image')->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file('image')->getClientOriginalExtension();
                $fileName = date('ymdhis') . '.' . $extension;

                $request->file('image')->move(public_path('images/uploads'), $fileName);
                $record['image'] = $fileName;
            }
//            dd($record);
            if ($id) {
                $record['updated_by'] = auth()->user()->id;
                School::find($id)->update($record);
                $outcome = "School Updated Successfully";
            } else {
                School::create($record);
                $outcome = "School Created Successfully";
            }

            DB::commit();
            return redirect()->back()->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function destroy($id) {
        $outcome = '';
        try {
            DB::beginTransaction();
            $record = School::find($id);
            if ($record->school_classes->count()) {
                return redirect()->back()->with('error', 'School is associated to this classes. Please delete Classes first');
            }

            $record->delete();
            $outcome = "School deleted successfully.";

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

}
