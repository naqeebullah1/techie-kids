<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Page;
use App\Models\Category;
use Illuminate\Support\Facades\Mail;
use App\Mail\contactMail;
use App\Models\Contact;
use App\Models\Testimonial;
use App\Models\Team;
use App\Models\Service;
use App\Models\School;
use App\Models\SchoolClass;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\Admission;
use App\Models\AdmissionDetail;

class FrontendController extends Controller {

    public function index($type = 'home') {
//        dd($type);
        $page = Page::with('pageDetails')->whereSlug($type)->first();
        $slider = new Banner;
        if ($type == "home") {
            $slider = Banner::where('active', 1)->first();
        }
        $schools = School::where('status',1)->get();
//dd($schools);
        return view('frontend.index', compact('slider', 'page', 'schools'));

//        if ($page->is_contact == 1):
//            return view('frontend/contact');
//        elseif ($page->is_front == 1):
//        else:
//            return view('frontend.other_pages', compact('page'));
//
//        endif;
    }

    public function gallery($slug) {
        $gallery = Category::with('images')->whereSlug($slug)->first();
        return view('frontend.gallery', compact('gallery'));
    }

    public function service($id) {
        $service = \App\Models\Service::find($id);
        return view('frontend.service', compact('service'));
    }

    public function teamMember($id) {
        $teamMember = \App\Models\Team::find($id);
        return view('frontend.team_member', compact('teamMember'));
    }

    public function serviceDetails($id) {
        $service = Service::find($id);
        return view('frontend.services.service_details', compact('service'));
    }

    public function serviceDetail($id) {
        $serviceDetail = \App\Models\Service::find($id);
        return view('frontend.service_detail', compact('serviceDetail'));
    }

    public function contact() {
        return view('frontend.contact');
    }

    public function sendMessage(Request $request) {
        
        if(isset($request->for_demo)) {  
           request()->validate([
            'contact.first_name' => ['required'],
            'contact.last_name' => ['required'],
            'contact.email' => ['required'],
            'contact.phone' => ['required'],
            'contact.school_name' => ['required'],
            'contact.title' => ['required'],
            'contact.select_one' => ['required'],
                ], [
            'contact.first_name.required' => 'First Name is required.',
            'contact.last_name.required' => 'Last Name is required.',
            'contact.email.required' => 'Email is required.',
            'contact.phone.required' => 'School Phone is required.',
            'contact.school_name.required' => 'School Name is required.',
            'contact.title.required' => 'Contact Title is required.',
            'contact.select_one.required' => 'Please Select One is required.',
        ]); 
        } else {
            request()->validate([
            'contact.first_name' => ['required'],
            'contact.last_name' => ['required'],
            'contact.email' => ['required'],
            'contact.phone' => ['required'],
            'contact.message' => ['required'],
                ], [
            'contact.first_name.required' => 'First Name is required.',
            'contact.last_name.required' => 'Last Name is required.',
            'contact.email.required' => 'Email is required.',
            'contact.phone.required' => 'Phone is required.',
            'contact.message.required' => 'Message is required.',
        ]);    
        }
        
    
        try {
            DB::beginTransaction();
            
            $data = $request->contact;
            if ($request->service) {
                $services = json_encode($request->service);
                $data['services'] = $services;
            }
            Contact::create($data);


            $to = env('MAIL_FROM_ADDRESS');
                // $to="naqeb7700@gmail.com";


            $mail = Mail::to($to)
                    ->send(new contactMail($request->contact));
   
//   dd($to);
            $outcomeStatus = "";
            $outcomeMessage = "";
            if (Mail::failures()) {
                $outcomeStatus = "error";
                $outcomeMessage = "Some error occured, please try again.";
            } else {
                $outcomeStatus = "success";
                $outcomeMessage = "Thank you for contacting us. We will reach out to you soon.";
            }
            DB::commit();
            return redirect()->back()->with($outcomeStatus, $outcomeMessage)->withFragment('#contactsection');
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }
    public function check_email($email) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        } else {
            return true;
        }
    }

    public function findYourSchool() {
        $getSchools = School::select('state_id')->whereHas('school_classes', function($q) {
                //return $q->whereDate('school_classes.start_on', '>', date('Y-m-d'));
                return $q->whereNotNull('school_classes.start_on');
            })->groupBy('state_id')->get();
        $statesWithData = $getSchools->pluck('state_id')->toArray();


//        dd();
        $states = \App\Models\State::whereIn('id', $statesWithData)->orderBy('state','ASC')->Dropdown()->getAll()->pluck('value', 'id')->toArray();
        $states = ['' => 'Select State'] + $states;
        $cities = ['' => 'Select City'];

        if (request()->state_id || request()->city_id) {
            $schools = School::query();
            if (request()->state_id) {
                $schools = $schools->where('state_id', request()->state_id);
            }
            if (request()->city_id) {
                $schools = $schools->where('city_id', request()->city_id);
            }
            $schools = $schools->whereHas('school_classes', function($q) {
                //return $q->whereDate('school_classes.start_on', '>', date('Y-m-d'));
                return $q->whereNotNull('school_classes.start_on');
            });
            $schools = $schools->get();
           
            return view('frontend.partials.card', compact('schools'));
        }



        return view('frontend.find_your_school', compact("states", 'cities'));
    }

    public function schoolDetails($id) {
        $school = School::find($id);
        return view('frontend.school_details', compact("school"));
    }

    public function classDetails($id) {
        $school = SchoolClass::find($id);
        return view('frontend.class_details', compact("school"));
    }

    public function login() {
    if(strstr(url()->previous(),"/class-details/")) {
        session(['classlink' => url()->previous()]);
    }
        return view('frontend.login');
    }

     public function updateCart($id = null) {
        if ($id) {
            $schoolClass = SchoolClass::find($id);
            $student = \App\Models\Student::find(request()->student_id);
            $new = [
                'school_logo' => asset('images/uploads/' . $schoolClass->school->image),
                'school_name' => $schoolClass->school->name,
                'title' => $schoolClass->title,
                'class_charges' => $schoolClass->class_charges,
                'end_date' => $schoolClass->end_on,
                'student_id' => $student->id,
                'student_name' => $student->first_name . ' ' . $student->last_name,
                'school_class_id' => $id,
                'school_id' => $schoolClass->school_id,
                'classroom_name' => request()->classroom_name
            ];

            $old = Session::get('cart_info');

            $cart = [];
            if ($old) {
                foreach ($old as $o):
                    $cart[] = $o;
                endforeach;
                $cart[] = $new;
            }
            if (empty($cart)) {
                $cart = [$new];
            }

            Session::put('cart_info', $cart);
        } else {
            $cart = Session::get('cart_info');
        }
        $view = view('frontend.cart', compact('cart'))
                ->render();
        return response()->json(['html' => $view, 'count' => count($cart)]);
    }

    public function checkout() {
//                Session::forget('cart_info');

        return view('frontend.checkout');
    }

    public function storeOrder(Request $request) {
//        dd($request->all());
        try {
            DB::beginTransaction();
            $data = Session::get('cart_info');
            $addmission = Admission::create(['parent_id' => auth()->user()->id]);

            foreach ($data as $d):
                $s = [
                    "class_charges" => $d['class_charges'],
                    "student_id" => $d['student_id'],
                    "school_class_id" => $d['school_class_id'],
                    "school_id" => $d['school_id'],
                    "admission_id" => $addmission->id,
                ];
                AdmissionDetail::create($s);
//                dump($s);
            endforeach;
//            exit;

            if (Mail::failures()) {
                $outcomeStatus = "error";
                $outcomeMessage = "Some Error Occured. Please try again.";
            } else {
                $outcomeStatus = "success";
//                $outcomeMessage = "Some Error Occured. Please try again.";
                $outcomeMessage = "Your admission process is started. We will reply soon. Thank you!";
            }
            Session::forget('cart_info');
            DB::commit();
            return redirect()->back()->with($outcomeStatus, $outcomeMessage);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function viewCart() {
//        Session::forget('cart_info');
        if (request()->isMethod('post')) {
//            dd(request()->all());
            Session::put('cart_info', request()->cart);
            return redirect()->back()->with('success', 'Cart updated successfully');
        }
        return view('frontend.view_cart');
    }

    public function thanks() {
        Session::forget('cart_info');
        return view('frontend.thanks');
    }

}
