<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\AdmissionDetail;
use App\Models\Admission;
use App\Models\School;
use App\Models\SchoolClass;
use Stripe;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Mail;

class ParentsController extends Controller {

    public function children(Request $request, $parentId = null) {
//        dd('');
        $students = Student::where('user_id', $parentId)->paginate(15);
        if ($request->ajax()) {
            $view = view('parents.students.table', compact('students'))
                    ->render();
            return response()->json(['html' => $view]);
        }
        $classes = AdmissionDetail::select('school_classes.*','school_class_id','students.first_name as s_first_name','students.last_name as s_last_name')
                ->join('school_classes', 'school_classes.id', 'admission_details.school_class_id')
                ->join('students', 'students.id', 'admission_details.student_id')
                ->join('admissions', 'admissions.id', 'admission_details.admission_id')
                ->where('admissions.parent_id', $parentId)
                ->get();
//                ->toArray();
        $parent = User::find($parentId);
        return view('parents.students.index', compact('students', 'parent','classes'));
    }

//    childCreate
    public function childCreate($id = null) {
        $userId = auth()->user()->id;
        $student = new Student();

        if ($id) {
            $student = Student::find($id);
        }

        $view = view('parents.students.form', compact('student'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function admissionDetails($id) {
        $details = AdmissionDetail::where('admission_id', $id)->get();
        $admission = Admission::where('id', $id)->first();

        $view = view('parents.partials.add_details', compact('details', 'admission'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function childStore(Request $request) {

        $request->merge(['first_name' => $request['record']['first_name']]);
        $request->merge(['last_name' => $request['record']['last_name']]);
        $dob = date('Y-m-d', time());
        $request->merge(['dob' => $dob]);
        if ($request->id) {
            $valdidateData = Validator::make($request->all(), [
                        'first_name' => 'required | min:3',
                        'last_name' => 'required | min:2',
                        //'dob' => 'required',
                        'image' => 'nullable',
            ]);
        } else {
            $valdidateData = Validator::make($request->all(), [
                        'first_name' => 'required | min:3',
                        'last_name' => 'required | min:2',
                        'dob' => 'required',
                        'image' => 'nullable',
            ]);
        }
        if ($valdidateData->fails()) {
            return redirect()->back()->withErrors($valdidateData)->withInput()->withFragment('#children');
        }

        try {
            DB::beginTransaction();
            $city = $request->record;
            $id = $request->id;
            if ($request->hasFile('image')) {

                $originName = $request->file('image')->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file('image')->getClientOriginalExtension();
                $fileName = date('ymdhis') . '.' . $extension;

                $request->file('image')->move(public_path('images/uploads'), $fileName);
                $city['image'] = $fileName;
            }
            $city['dob'] = date('Y-m-d', time());
            $city['user_id'] = auth()->user()->id;
            $city['allow_photo_release'] = $request->allow_photo_release;
            if ($id) {
                $city['updated_by'] = auth()->user()->id;
                $city = Student::find($id)->update($city);
                $outcome = "Student Updated Successfully";
            } else {
//                dd($city);
                $city = Student::create($city);
                $outcome = "Student Created Successfully";
            }

            DB::commit();
            return redirect()->back()->with('success', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function dashboard() {
        $user = auth()->user();
        return view('parents.dashboard', compact('user'));
    }

    public function updateProfile(Request $request) {
//        dd($request->all());

        $valdidateData = Validator::make($request->all(), [
                    'first_name' => 'required | min:3',
                    'last_name' => 'required | min:2',
                    'email' => 'required | email | unique:users,email,' . Auth::user()->id,
                    'password' => 'nullable|min:8|confirmed',
                    'password_confirmation' => 'nullable',
                    'roles' => 'required',
                    'status' => '',
                    'phone1' => 'required',
                    'phone2' => '',
                    'address1' => '',
                    'address2' => '',
                    'city' => '',
                    'state' => '',
                    'zip' => '',
                    'check_privacy' => '',
                    'check_terms' => '',
                    'check_privacy_date' => '',
                    'check_terms_date' => '',
        ]);
        if ($valdidateData->fails()) {
            return redirect()->back()->withErrors($valdidateData)->withInput()->withFragment('#profile');
        }

        $user = auth()->user();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone1 = $request->phone1;
        $user->phone2 = $request->phone2;

        if ($request->password) {
            $user->password = $request->password;
        }
        $user->save();
        return redirect()->back()->with('success', 'Profile updated successfully.');
    }

    public function deleteChild($id) {
        $student = Student::find($id);
//dd($student->admission_details);
        if ($student->admission_details->count()) {
            return redirect()->back()->with('error', 'This student is applied for admissions. So it can not be deleted.');
        }
        $student->delete();
        return redirect()->back()->with('success', 'Student deleted successfully');
    }
    
    public function cancelstripesubscription($id) {
        $stripeAPiKey = "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";
        if (env('STRIPE_API_KEY')) {
            $stripeAPiKey = env('STRIPE_API_KEY');
        }

        Stripe\Stripe::setApiKey($stripeAPiKey);
        
        try {
                    $subscriptionData = \Stripe\Subscription::retrieve($id);
                    $subscriptionData->cancel();
                    $subs = AdmissionDetail::where(['stripe_subscription_id' => $id])->get()->first();
                        
                        // Make sure you've got the Page model
                        if($subs) {
                            $subs->status = 0;
                            $subs->save();
                        } 
                        
                    return redirect()->back()->with('success', "Subscription cancelled sucessfully");
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput()->withFragment('#purchases');
                }
    }
    
    public function stripepay(Request $request) {

        if (isset($request->cart_index)) {
            $cartIndex = $request->cart_index;
            Session::put('cart_index', $request->cart_index);
        }
        // else{
        //     $cartIndex = Session::get('cart_index');
        // }
        $cartIndex = Session::get('cart_index');

        //header('Content-Type: application/json');
        $jsonStr = file_get_contents('php://input');
        $jsonObj = json_decode($jsonStr);

        $stripeAPiKey = "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";
        if (env('STRIPE_API_KEY')) {
            $stripeAPiKey = env('STRIPE_API_KEY');
        }

        Stripe\Stripe::setApiKey($stripeAPiKey);
        $carts = Session::get('cart_info');
        $cval = $carts[$cartIndex];


        $school_name = School::where(['id' =>$cval['school_id']])->get()->first()['name'];
        $class_name = SchoolClass::where(['id' =>$cval['school_class_id']])->get()->first()['title'];
        
        //foreach($carts as $key => $cval) {
        if ($jsonObj->request_type == 'create_customer_subscription') {
            $name = !empty($jsonObj->name) ? $jsonObj->name : '';

            $email = !empty($jsonObj->email) ? $jsonObj->email : '';
            $amount = !empty($jsonObj->amount) ? $jsonObj->amount : '';

            $planInterval = "month";
            // Convert price to cents 

            $planName = $class_name." - ".$school_name;



            //$cart = $carts[0];
            // Add customer to stripe 
            try {
                // Check if customer id exist
                $customer_id = User::where(['id'=>auth()->user()->id])->get()->first()['stripe_customer_id'];
                if($customer_id) {
                    // retrieve customer
                    $customer = \Stripe\Customer::retrieve($customer_id);   
                }     
                
                // else
                else {
                    $customer = \Stripe\Customer::create([
                            'name' => $name,
                            'email' => $email
                    ]);
                    if($customer){
                        // Update user with customer id    
                        $user = User::find(auth()->user()->id);
                        
                        // Make sure you've got the Page model
                        if($user) {
                            $user->stripe_customer_id = $customer->id;
                            $user->save();
                        } 
                    }
                }
            } catch (Exception $e) {
                $api_error = $e->getMessage();
                echo json_encode(['error' => $api_error]);
            }


            $class = \App\Models\SchoolClass::find($cval['school_class_id']);
            $endDate = date('Y-m-d H:i:s', strtotime("2099-12-30 00:00:00"));
            $planPriceCents = round($cval['class_charges'] * 100);
            $cancel_at = strtotime($endDate);
            if (empty($api_error) && $customer) {
                try {
                    // Check if product id exist
                    
                    $price_id = SchoolClass::where(['id'=>$cval['school_class_id']])->get()->first()['stripe_product_id'];
                    if($price_id) {
                        // retrieve Price
                        $price = \Stripe\Price::retrieve($price_id);   
                    }     
                    else {
                     // Create price with subscription info and interval 
                    $price = \Stripe\Price::create([
                                'unit_amount' => $planPriceCents,
                                'currency' => 'USD',
                                'recurring' => ['interval' => $planInterval],
                                'product_data' => ['name' => $planName],
                    ]);
                    if($price){
                        // Update user with customer id    
                        $schoolclassprice = SchoolClass::find($cval['school_class_id']);
                        
                        // Make sure you've got the Page model
                        if($schoolclassprice) {
                            $schoolclassprice->stripe_product_id = $price->id;
                            $schoolclassprice->save();
                        } 
                    }
                }
                     
                
                   
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                }

                if (empty($api_error) && $price) {
                    // Create a new subscription 
                    try {
                        $subscription = \Stripe\Subscription::create([
                                    'customer' => $customer->id,
                                    'items' => [[
                                    'price' => $price->id,
                                        ]],
                                    'proration_behavior' => 'none',
                                    'payment_behavior' => 'default_incomplete',
                                    'cancel_at' => $cancel_at,
                                    'expand' => ['latest_invoice.payment_intent'],
                        ]);
                    } catch (Exception $e) {
                        $api_error = $e->getMessage();
                        echo json_encode(['error' => $api_error]);
                    }

                    if (empty($api_error) && $subscription) {
                        $output = [
                            'subscriptionId' => $subscription->id,
                            'clientSecret' => $subscription->latest_invoice->payment_intent->client_secret,
                            'customerId' => $customer->id
                        ];
                            
                        echo json_encode($output);
                    } else {
                        
                        echo json_encode(['error' => $api_error]);
                    }
                } else {
                    
                    echo json_encode(['error' => $api_error]);
                }
            } else {
                
                echo json_encode(['error' => $api_error]);
            }
        } elseif ($jsonObj->request_type == 'payment_insert') {


            $payment_intent = !empty($jsonObj->payment_intent) ? $jsonObj->payment_intent : '';
            $subscription_id = !empty($jsonObj->subscription_id) ? $jsonObj->subscription_id : '';
            $customer_id = !empty($jsonObj->customer_id) ? $jsonObj->customer_id : '';
            $planInterval = "month";
            // Retrieve customer info 
            try {
                $customer = \Stripe\Customer::retrieve($customer_id);
            } catch (Exception $e) {
                $api_error = $e->getMessage();
                echo json_encode(['error' => $api_error]);
            }

            // Check whether the charge was successful 
            if (!empty($payment_intent) && $payment_intent->status == 'succeeded') {
                
                
                // Retrieve subscription info 
                try {
                    $subscriptionData = \Stripe\Subscription::retrieve($subscription_id);
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                }

                $payment_intent_id = $payment_intent->id;
                $paidAmount = $payment_intent->amount;
                $paidAmount = ($paidAmount / 100);
                $paidCurrency = $payment_intent->currency;
                $payment_status = $payment_intent->status;


                $created = date("Y-m-d H:i:s", $payment_intent->created);
                $current_period_start = $cancel_at = $current_period_end = '';
                if (!empty($subscriptionData)) {
                    $created = date("Y-m-d H:i:s", $subscriptionData->created);
                    $current_period_start = date("Y-m-d H:i:s", $subscriptionData->current_period_start);
                    $current_period_end = date("Y-m-d H:i:s", $subscriptionData->current_period_end);
                    $cancel_at = date("Y-m-d H:i:s", $subscriptionData->cancel_at);
                }

                $customer_name = $customer_email = '';
                if (!empty($customer)) {
                    $customer_name = !empty($customer->name) ? $customer->name : '';
                    $customer_email = !empty($customer->email) ? $customer->email : '';

                    if (!empty($customer_name)) {
                        $name_arr = explode(' ', $customer_name);
                        $first_name = !empty($name_arr[0]) ? $name_arr[0] : '';
                        $last_name = !empty($name_arr[1]) ? $name_arr[1] : '';
                    }
                }

                // Check if any transaction data exists already with the same TXN ID 


                $payment_id = 0;
                if (!empty($prevPaymentID)) {
                    $payment_id = $prevPaymentID;
                } else {
                    
                }


//                $data = Session::get('cart_info');
                if (Session::get('admission_id')) {
                    $addId = Session::get('admission_id');
                } else {
                    $addmission = \App\Models\Admission::create([
                                'parent_id' => auth()->user()->id,
                    ]);
                    $addId = $addmission->id;
                }


                $addDetails = [
                    "class_charges" => $cval['class_charges'],
                    "student_id" => $cval['student_id'],
                    "school_class_id" => $cval['school_class_id'],
                    "school_id" => $cval['school_id'],
                    "admission_id" => $addId,
                    "classroom_name" => $cval['classroom_name'],
                ];
    
        

                $addDetails["stripe_subscription_id"] = $subscription_id;
                $addDetails["stripe_customer_id"] = $customer_id;
                $addDetails["stripe_payment_intent_id"] = $payment_intent_id;
                $addDetails["paid_amount"] = $cval['class_charges'];
                $addDetails['paid_amount_currency'] = $paidCurrency;
                $addDetails['plan_interval'] = $planInterval;
                $addDetails['customer_name'] = $customer_name;
                $addDetails['customer_email'] = $customer_email;
                $addDetails['plan_period_start'] = $current_period_start;
                $addDetails['plan_period_end'] = $current_period_end;
                $addDetails['plan_cancel_at'] = $cancel_at;
                $addDetails['admission_id'] = $addId;
                $addDetails['status'] = 1;


                try {
                    $add = AdmissionDetail::create($addDetails);
                } catch (\Exception $e) {
                    // do task when error
                    echo $e->getMessage();   // insert query
                }

            
                $output = [
                    'payment_id' => base64_encode($addId),
                    'cart_index' => $cartIndex
                ];

                if (count(Session::get('cart_info'))) {
                    Session::put('admission_id', $addId);
                } else {
                    Session::put('admission_id', null);
                }

                /*
                 * UNSET INDEX FROM ARRAY
                 */

                Session::put('cart_index', null);
                unset($carts[$cartIndex]);
                Session::put('cart_info', $carts);
                
                // Send Email to teacher
                $getTeacherId = SchoolClass::where(['id'=>$cval['school_class_id']])->get()->first()['user_id'];    
                $teacherData = User::where(['id' => $getTeacherId])->where(['status'=>1])->get()->first();
                $student_name = $cval['student_name'];
                
                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . url('/') . '" title="logo" target="_blank">
                            <img width="220" src="https://techiekidsclub.com/frontend/images/logo/techie_kids_logo.png" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            <strong>'.$student_name.'</strong> has been added to <strong>'. $class_name.'</strong> at <strong>'. $school_name .'</strong>
                                        </p>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.' . str_replace(" ", "", strtolower(config('app.name'))) . '.com</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                if(isset($teacherData) && !empty($teacherData)){  
                    Mail::send(array(), array(), function ($message) use ($html,$teacherData,$school_name,$student_name) {
                        $message->to($teacherData['email'])
                                ->subject("A new student " . $student_name . " has registered at ".$school_name)
                                ->setBody($html, 'text/html');
                    });
                }
                // Send Email to RD
                
                $getRDId = School::where(['id'=>$cval['school_id']])->get()->first()['user_id'];    
                $RDData = User::where(['id' => $getRDId])->where(['status'=>1])->get()->first();
                if(isset($RDData) && !empty($RDData)){
                    Mail::send(array(), array(), function ($message) use ($html,$RDData,$school_name, $student_name) {
                        $message->to($RDData['email'])
                                ->subject("A new student " . $student_name . " has registered at ".$school_name)
                                ->setBody($html, 'text/html');
                    });
                }
                // Send Email to Parent with order details
                $account_url = url('/') . "/login";
                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . url('/') . '" title="logo" target="_blank">
                            <img width="220" src="https://techiekidsclub.com/frontend/images/logo/techie_kids_logo.png" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">Welcome!</h1>
                                        <span
                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            Hi ' . auth()->user()->first_name . ', <br /><br /><span style="display:block;text-align:left;">
' . $cval["student_name"] . ' is registered to join Techie Kids Club classes at ' . $cval["school_name"] . '. <br />
Get ready to hear all about coding + robotics over dinner!<br />
Here\'s what you can expect:<br />
<ul style="list-style:square;text-align:left;">
 <li> Your child will join the class on the next regularly scheduled class day.</li>
<li> The enrichment coach and your child\'s classroom teacher will remind ' . $cval["student_name"] . ' that it\'s class day and get them to the right classroom.</li>
<li> Each week, you\'ll get an update on what\'s happening in class. Log into your <a href="' . $account_url . '" target="_blank">account</a> to see it. </li>
</ul></span>
                                        </p>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.' . str_replace(" ", "", strtolower(config('app.name'))) . '.com</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                $student_name = $cval['student_name'];
                $user = User::where(['id' => Auth::user()->id])->first();
                Mail::send(array(), array(), function ($message) use ($html, $user, $student_name) {
                    $message->to(Auth::user()->email)
                            ->subject("$student_name is now enrolled in " . config('app.name') . " Team")
                            ->setBody($html, 'text/html');
                });

                echo json_encode($output);
            } else {
                echo json_encode(['error' => 'Transaction has been failed!']);
            }
        }
        // }// end of loop cart
    }

    public function demandLibrary() {
        $banners = \App\Models\DemandLiberary::query();
        if (request()->ajax()) {
//            dd('');
            if (request()->type) {
                $search = request()->type;

                $banners = $banners->where(function($q) use($search) {
                    return $q->where('title', 'like', '%' . $search . '%')
                                    ->orWhere('description', 'like', '%' . $search . '%')
                                    ->orWhere('tags', 'like', '%' . $search . '%');
                });
            }
            $banners = $banners->paginate(1000000);
            $view = view('parents.partials.dl_card', compact('banners'))
                    ->render();
            return $view;
        }
        $banners = $banners->paginate(1000000);
        return view('parents.demand_liberary', compact('banners'));
    }

}
