<?php
$team = team();
?>
<!-- Start Team Area -->
<section class="dcare__team__area pb--50 pt-5 bg--white">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section__title text-center">
                    <h2 class="title__line">Regional Directors</h2>
                    <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunte magna aliquaet, consectetempora incidunt</p>-->
                </div>
            </div>
        </div>
        <div class="row mt--0">
            <!-- Start Single Team -->
            @foreach($team['director'] as $tm)
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="team">
                    <div class="team__thumb">
                        <a href="#">
                            <img src="<?= asset('/images/teams/' . $tm->image) ?>" alt="team images">
                        </a>
                        <div class="team__hover__action">
                            <div class="team__hover__inner">
                                <?= $tm->description ?>
                                <!--<p>Lorem ipsum dolor sit amet, consecteturadmodi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>-->
                                <!--                                <ul class="dacre__social__link--2 d-flex justify-content-center">
                                                                    <li class="facebook"><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                                                    <li class="twitter"><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                                                                    <li class="vimeo"><a href="https://vimeo.com/"><i class="fa fa-vimeo"></i></a></li>
                                                                    <li class="pinterest"><a href="https://www.pinterest.com/"><i class="fa fa-pinterest-p"></i></a></li>
                                                                </ul>-->
                            </div>
                        </div>
                    </div>
                    <div class="team__details">
                        <div class="team__info">
                            <h6><a href="#"><?= $tm->name ?></a></h6>
                            <span><?= $tm->title ?></span>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
<section class="dcare__team__area pb--50 pt-5 bg--white">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section__title text-center">
                    <h2 class="title__line">Enrichment Coaches</h2>
                    {!! $section->section !!}
                </div>
            </div>
        </div>
        <div class="row mt--0">
            <!-- Start Single Team -->
            @foreach($team['coach'] as $tm)
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="team">
                    <div class="team__thumb">
                        <a href="#">
                            <img src="<?= asset('/images/teams/' . $tm->image) ?>" alt="team images">
                        </a>
                        <div class="team__hover__action">
                            <div class="team__hover__inner">
                                <?= $tm->description ?>
                                <!--<p>Lorem ipsum dolor sit amet, consecteturadmodi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>-->
                                <!--                                <ul class="dacre__social__link--2 d-flex justify-content-center">
                                                                    <li class="facebook"><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                                                    <li class="twitter"><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                                                                    <li class="vimeo"><a href="https://vimeo.com/"><i class="fa fa-vimeo"></i></a></li>
                                                                    <li class="pinterest"><a href="https://www.pinterest.com/"><i class="fa fa-pinterest-p"></i></a></li>
                                                                </ul>-->
                            </div>
                        </div>
                    </div>
                    <div class="team__details">
                        <div class="team__info">
                            <h6><a href="#"><?= $tm->name ?></a></h6>
                            <span><?= $tm->title ?></span>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
<!-- End Team Area -->