<table class="sortable table table-bordered draggable">
    <thead>
        <tr>
            <th>Title</th>
            <?php
            $roles = auth()->user()->ModelHasRoles->pluck('role_id')->toArray();
            ?>
            @if(in_array(1,$roles))
            <th>Tags</th>
            <th>Status</th>
            @endif
            <th>Documents</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($banners as $k=>$b)
        <tr>

            <td>
                <?= $b->title ?>
            </td>
            @if(in_array(1,$roles))
            <td>
                <?php
                $tags = json_decode($b->tags);
                ?>
                @foreach ($tags as $tag)
                <span class="badge badge-success"><?= $tag->value; ?></span>
                @endforeach
            </td>
            <td>
                <a href="{{ url('backend/change-status/demand_liberaries/'.$b->id,$b->active) }}">

                    <span class="span-status {{ $b->active == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                        {{ $b->active == 1 ? __('Active') : __('Blocked') }}
                    </span>
                </a>
            </td>
            @endif
            <td>
                <?php
                $files = explode(',', $b->file);
                foreach ($files as $file):

                    if ($file && file_exists(public_path() . '/images/thumbnails/' . $file)) {
                        $img = 'images/thumbnails/' . $file;
                        ?>
                        <a href="<?= url($img); ?>" class="btn btn-success btn-sm btn-small" download="">
                            <i class="fa fa-download"></i>
                        </a>
                        <?php
                    }
                endforeach;
                ?>
            </td>
            <td class="sortable-handle">
                <a data-toggle="modal" onclick="setDetails('<?= $b->id ?>')" 
                   data-target="#myModal" href="javascript:;" class="btn btn-warning btn-sm btn-small">
                    Details
                </a>


                @can('demand-liberaries-edit')
                <a href="{{ route('admin.demand_liberaries.edit',$b->id) }}" class="btn btn-primary btn-sm btn-small">
                    <i class="fa fa-edit"></i>
                </a>
                @endcan
                @can('demand-liberaries-delete')
                <button type="submit"
                        onclick="event.preventDefault(); setUrl('{{ route('admin.demand.destroy', $b->id) }}'); deleteConfirm('destroy', 'You are about to delete a demand-library , Are you sure?')"

                        class="btn btn-icon btn-round btn-danger btn-sm btn-small">
                    <i class="fa fa-trash"></i>
                </button>
                @endcan
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<form 
    id="destroy"
    method="post" id="destroy" 
    action="">
    @csrf
    @method('DELETE')
</form>
{{ $banners->links()->with('onclick','getMore(this)')->with('type',request()->type) }}