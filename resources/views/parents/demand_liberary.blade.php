@extends('layouts.front_theme')
@section('content')

<!-- Start Subscribe Area -->
<section class="bcare__subscribe bg-image--7 subscrive--2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-lg-12">
                <div class="subscribe__inner">
                    <h2>Search  For Demand Library</h2>
                    <div class="newsletter__form">
                        <div class="input__box">
                            <div id="mc_embed_signup">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="news__input">
                                                <input type="text" class="form-control mb-2" placeholder="Type any keywords..." id="search-dl" name="search">
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="schools-container" class="dl-container dcare__courses__area section-padding--lg bg--white">
    @include("parents.partials.dl_card")
</section>

<!-- End Subscribe Area -->
@endsection
@section('style')
<style>
    .p-center .pagination{
        justify-content: center !important;
    }
    .d-content{
        cursor: pointer;
    }
</style>
@endsection
@section('script')
<script>
    function toggleMore($this, $cls) {
        $($this).parent('div').find('.d-content').addClass('d-none');
        $($this).parent('div').find($cls).removeClass('d-none');
    }
    function getMore($this) {
        var url = $($this).attr('href');
        $('.dl-container').load(url);
        return false;
    }
     $(function () {
        //setup before functions
        var typingTimer;     //timer identifier
        var doneTypingInterval = 500;  //time in ms, 5 seconds for example
        var $input = $('#search-dl');

//on keyup, start the countdown
        $input.on('keyup', function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
        });

//on keydown, clear the countdown 
        $input.on('keydown', function () {
            clearTimeout(typingTimer);
        });

//user is "finished typing," do something
        function doneTyping() {
            var $val = $('#search-dl').val();
            $val = $val.split(' ').join('%20');
            $('.dl-container').load("<?= url()->full() ?>?type=" + $val);
        }
        
    })
</script>
@endsection
