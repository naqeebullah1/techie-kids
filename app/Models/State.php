<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;


class State extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable,
        SoftDeletes,
        HasFactory;

    protected $guarded = [];

    public function cities() {
        return $this->hasMany(City::class, 'state_code', 'state_code');
    }

    public function scopeGetAll($query) {
        return $query->get();
    }

    public function scopeDropdown($query) {
        return $query->select('id', 'state as value');
    }

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $user = auth()->user();
            $model->created_by = $user->id;
            $model->updated_by = $user->id;
        });

        static::updating(function($model) {
            $user = auth()->user();
            $model->updated_by = $user->id;
        });
    }

}
