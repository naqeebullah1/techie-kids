
<table class="sortable table table-bordered table-small">
    <thead>
        <tr>
            <th>Title</th>
            <th>Teacher</th>
            <th>Charges</th>
            <th>Duration Of Class</th>
            <th>Week Days</th>
            <th>Timimg Of Class</th>
            <th>Status</th>
            <th>Students</th>
            @can('school-classes-edit')

            <th>Actions</th>
            @endcan
        </tr>
    </thead>
    <tbody>
        @foreach($classes as $city)
        <tr>
            <td>
                <?= $city->title ?>
            </td>
            <td>
                <?= $city->teacher->first_name . ' ' . $city->teacher->last_name ?>
            </td>
            <td>
                $<?= $city->class_charges ?>
            </td>
            <td>
                <b>From: </b><?= formatDate($city->start_on) ?><br>
                <!--<b>To: </b> <?= formatDate($city->end_on) ?>-->
            </td>
            <td>
              <?php  $weekly_off_days = $city->weekly_off_days;
                                if ($weekly_off_days) {
                                    $weekly_off_days = explode(',', $weekly_off_days);
                                    if ($weekly_off_days) {
                                        echo "";
                                        foreach ($weekly_off_days as $day):
                                            echo '<span>' . weekDays()[$day] . '</span>&nbsp;&nbsp;';
                                        endforeach;
                                        echo "";
                                    }else {
                                        echo "<p>No Days Found</p>";
                                    }
                                }
                    ?>
            </td>
            <td>
                <b>From: </b><?= formatTime($city->start_at) ?><br>
                <b>To: </b> <?= formatTime($city->end_at) ?>
            </td>
            <td>
                @can('school-classes-edit')
                <a href="{{ url('backend/change-status/school_classes/'.$city->id,$city->status) }}">

                    <span class="span-status {{ $city->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                        {{ $city->status == 1 ? __('Active') : __('Blocked') }}
                    </span>
                </a>
                @else
                <span class="span-status {{ $city->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                    {{ $city->status == 1 ? __('Active') : __('Blocked') }}
                </span>

                @endcan
            </td>            
            <td>
                <a class="btn btn-success" onclick="return loatstd('{{ url('load-students/'.$city->id) }}')" href="javascript:">
                    Students
                </a>
            </td>            
            @can('school-classes-edit')
            <td>
                <a onclick="event.preventDefault(); loadForm(this)" href="<?= url('backend/school-classes-create/' . $city->school_id . '/' . $city->id) ?>"
                   class="btn btn-small btn-sm btn-primary">
                    <i class="fa fa-edit"></i>
                </a>
                <button type="submit"
                        onclick="event.preventDefault(); setUrl('{{ route('admin.schools.classes.destroy', $city->id) }}'); deleteConfirm('destroy', 'Associated data with this page will also be deleted, Are you sure?')"
                        class="btn btn-icon btn-round btn-danger btn-small btn-sm">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
            @endcan
        </tr>
        @endforeach
        @can('page.edit')
        <tr>

            <td colspan="6">
                <input type="submit" value="Update" class="btn btn-success m-auto d-block"/>
            </td>

        </tr>    
        @endcan
    </tbody>
</table>

<form 
    id="destroy"
    method="post" id="destroy" 
    action="">
    @csrf
    @method('DELETE')
</form>
{{ $classes->links() }}

