<h3 style="flex-grow: 1;">
@if(request()->id)
{{ __('Edit Child') }}
@else
{{ __('Create Child') }}
@endif
</h3>
<div class="card-body">
    <form enctype="multipart/form-data" method="post" action="{{ route('parent.children.store') }}">
        @csrf
        <div class="row">
            <div class="col-lg-6">
                <input value="{{ $student->id }}" type="hidden" name="id"/>
                <input value="{{ request()->user_id }}"  type="hidden" name="record[user_id]"/>
                <input value="{{ $student->first_name }}" placeholder="First Name" type="text" class="form-control" name="record[first_name]"/>
            </div>
            <div class="col-lg-6">
                <input value="{{ $student->last_name }}" placeholder="Last Name" type="text" class="form-control" name="record[last_name]"/>
            </div>
            
            <div class="col-lg-12 mt-2">
                <label><strong>Upload Your Child Photo (Optional)</strong></label>
                <input type="file" data-default-file="{{asset('images/uploads/'.$student->image)}}" class="dropify" name="image"/>
            </div>
            <div class="col-lg-12 mt-2">
                                                    
                                                    <label class="" for="allow_photo_release">
                                                    <input type="checkbox" id="allow_photo_release" class="" name="allow_photo_release" value="1" <?php if($student->allow_photo_release == 1) { echo "checked='checked'"; } ?> />
                                                     <strong>I agree to allow photos of my child(ren) to be used in promotional materials for Techie Kids Club (<a target="_blank" href="{{url('/')}}/privacy-policy">Privacy Policy</a>)</strong></label> <br /><span class="text-info">* Techie Kids Club will never share or sell your data or images</span>
                                                </div>
            <div class="col-12 mt-2 text-right">
                <a href="javascript:" onclick="removeForm()" class="mt-2 btn btn-success">Cancel</a>
                @if(request()->id)
                <button type="submit" class="mt-2 btn btn-success">Update</button>
                @else
                <button type="submit" class="mt-2 btn btn-success">Add</button>
                @endif
            </div>
        </div>
    </form>
</div>
