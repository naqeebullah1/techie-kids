<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">
                    @if(request()->id)
                    {{ __('Edit City') }}
                    @else
                    {{ __('Create City') }}
                    @endif
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('admin.cities.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <label>City</label>
                            <input value="{{ $city->id }}" type="hidden" name="id"/>
                            <input value="{{ request()->state_code }}" type="hidden" name="record[state_code]"/>
                            <input value="{{ $city->city }}" type="text" class="form-control" name="record[city]"/>
                        </div>
                    <!--</div>-->
                    <div class="col-lg-6">
                        <br/>
                        <a href="javascript:" onclick="removeForm()" class="mt-2 btn btn-success">Cancel</a>
                        @if(request()->id)
                        <button type="submit" class="mt-2 btn btn-success">Update</button>
                        @else
                        <button type="submit" class="mt-2 btn btn-success">Add</button>
                        @endif
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>