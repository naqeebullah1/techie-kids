<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('route:clear');
    dd(Artisan::output());
});

//Before authentication
Route::group(['middleware' => 'beforeAuth'], function() {
    Route::get('/', 'FrontendController@index');
//    Route::get('/', 'Admin\UsersController@login');
    Route::get('page/{type?}', 'FrontendController@index')->name('front');
    Route::get('/backend', 'Admin\UsersController@login');
    Route::get('backend/login', 'Admin\UsersController@login')->name('backend.login');
    Route::get('backend/forgotPassword', 'Admin\UsersController@recoverPassword');
    Route::get('backend/resetPassword/{token}', 'Admin\UsersController@resetPassword');
    Route::get('view-service/{id}', 'FrontendController@service')->name('view-service');
    Route::get('service-detail/{id}', 'FrontendController@serviceDetails')->name('service-details');
    Route::get('privacy-policy', 'FooterController@privacy');
    Route::get('terms-conditions', 'FooterController@terms');

    Route::get('view-service-detail/{id}', 'FrontendController@serviceDetail')->name('view-service-detail');

    Route::post('dologin', 'Admin\UsersController@login');
    Route::post('send-message', 'FrontendController@sendMessage')->name('sendMessage');
    Route::post('recoverPassword', 'Admin\UsersController@recoverPassword');


//    Route::post('changePassword', 'Admin\UsersController@changePassword');
    Route::get('gallery/{slug}', 'FrontendController@gallery');
    Route::get('contact', 'FrontendController@contact');
    Route::get('find-your-school', 'FrontendController@findYourSchool');
    Route::get('cities-dd', 'Admin\SchoolsController@citiesDD');
    
    Route::get('frontend/school-details/{id}', 'FrontendController@schoolDetails');
    Route::get('frontend/class-details/{id}', 'FrontendController@classDetails');

    Route::get('parentlogin', 'FrontendController@login');
    Route::get('update-cart/{id?}', 'FrontendController@updateCart');
    Route::get('checkout', 'FrontendController@checkout');
    Route::post('store-order', 'FrontendController@storeOrder');
    Route::match(['get', 'post'], 'view-cart', 'FrontendController@viewCart');
    Route::get('forgotPassword', 'Admin\UsersController@recoverParentPassword');
    Route::get('resetPassword/{token}', 'Admin\UsersController@resetParentPassword');
});
Route::post('changePassword', 'Admin\UsersController@changePassword');


//After authentication with prefix:backend
Route::prefix('backend')->name('admin.')->middleware('checkAuth')->group(function() {
    Route::resource('users', 'Admin\UsersController');
    Route::match(['get', 'post'], '/users-list/{role_id?}', 'Admin\UsersController@index')->name('users.index');
    Route::match(['get', 'post'], '/parent-list/{role_id?}', 'Admin\UsersController@parentlist')->name('users.parentlist');

    Route::resource('roles', 'Admin\RolesController');
    Route::get('user-profile', 'Admin\UsersController@profileEdit');
    Route::get('reset-password-manually/{email}', 'Admin\UsersController@resetpasswordmanually');
    Route::get('change-password', 'Admin\UsersController@changePassword');
    Route::get('dashboard', 'PagesController@dashboard');
    Route::post('logout', 'Admin\UsersController@logout')->name('logout');
    Route::patch('user-profile', 'Admin\UsersController@updateProfile')->name('user.profile');


    //for home page banner
    // Route::resource('banners', 'BannerController')->except('show');

    Route::get('banners/{type?}', 'BannerController@index')->name('banners.index');
    Route::post('banners/store', 'BannerController@store')->name('banners.store');
    Route::delete('banners/destroy/{id}', 'BannerController@destroy')->name('banners.destroy');

    Route::post('bannar_ajax_upload', 'BannerController@bannar_ajax_upload')->name('bannar_ajax_upload');
    Route::post('banners/sort-order', 'BannerController@sortData')->name('banner.sort.order');
    Route::post('banners-upload', 'TrendingProductController@upload')->name('upload.widget.image');
    Route::get('change-status/{table}/{id}/{status}', 'BannerController@changeStatus')->name('admin.banner.change.status');

    Route::resource('pages', 'PagesController')->except('show');
    Route::get('page-details/{pageId}', 'PagesController@pageDetails')->name('page.details');
    Route::get('contact-details/{pageId}', 'PagesController@contactDetails')->name('contact.details');
    Route::get('pages/edit/{id?}', 'PagesController@index');
    Route::get('page-details/create/{pageId}', 'PagesController@pageDetailCreate')->name('page.details.create');
    Route::post('page-details/store/{pageId}', 'PagesController@pageDetailStore')->name('page.details.store');
    Route::get('page-details/edit/{id}', 'PagesController@pageDetailEdit')->name('page.details.edit');
    Route::put('page-details/update/{id}', 'PagesController@pageDetailUpdate')->name('page.details.update');

    Route::post('categories/store', 'GalleriesController@categoriesStore')->name('categories.store');
    Route::get('categories', 'GalleriesController@categories')->name('categories.index');
    Route::get('categories/edit/{id}', 'GalleriesController@categoriesEdit')->name('categories.edit');
    Route::get('categories/create', 'GalleriesController@categoriesCreate')->name('categories.create');
    Route::put('categories/update/{id}', 'GalleriesController@categoriesUpdate')->name('categories.update');

    Route::get('demand-liberaries', 'DemandLiberariesController@index')->name('demand_liberaries.index');
    Route::get('demand-liberaries/create', 'DemandLiberariesController@create')->name('demand_liberaries.create');
    Route::post('demand-liberaries/store', 'DemandLiberariesController@store')->name('demand_liberaries.store');
    Route::get('demand-liberaries/edit/{id}', 'DemandLiberariesController@edit')->name('demand_liberaries.edit');
    Route::put('demand-liberaries/update/{id}', 'DemandLiberariesController@update')->name('demand_liberaries.update');
    Route::delete('demand-liberaries/destroy/{id}', 'DemandLiberariesController@destroy')->name('demand.destroy');
    

    Route::get('galleries/{id}', 'GalleriesController@index')->name('galleries');
    Route::post('galleries/store/{id}', 'GalleriesController@store')->name('gelleries.store');
    Route::delete('galleries/destroy/{id}', 'GalleriesController@destroy')->name('galleries.destroy');
    Route::post('ck-editor/uploads', 'BannerController@ckeditorUpload')->name('ckeditor.upload');

    Route::delete('category/destroy/{id}', 'GalleriesController@destroyCategory')->name('category.destroy');
    Route::delete('page_details/destroy/{id}', 'PagesController@pageDetailDestroy')->name('page_details.destroy');
//    Route::post('galleries/store/{id}', 'GalleriesController@store')->name('gelleries.store');

    Route::get('contacts', 'PagesController@contacts');
    Route::delete('contacts/destroy/{id}', 'PagesController@ContactsDestroy')->name('contact.destroy');

    Route::post('pages/updatetype', 'PagesController@updateType')->name('pages.updatetype');
    Route::get('services/{type?}', 'ServicesController@index')->name('services.index');
    Route::post('services/store', 'ServicesController@store')->name('services.store');
    Route::delete('services/destroy/{id}', 'ServicesController@destroy')->name('services.destroy');

    Route::resource('testimonials', 'TestimonialController');
    Route::resource('teams', 'TeamController');

    Route::resource('footers', 'FooterController');

    Route::match(['get', 'post'], 'states', 'Admin\StatesController@index');
    Route::get('states/create/{id?}', 'Admin\StatesController@create');
    Route::post('states/store', 'Admin\StatesController@store')->name('states.store');
    Route::delete('states-destroy/{id}', 'Admin\StatesController@destroy')->name('states.destroy');

    Route::match(['get', 'post'], 'cities/{state_id}', 'Admin\CitiesController@index');
    Route::get('cities-create/{state_code}/{id?}', 'Admin\CitiesController@create');
    Route::post('cities-store', 'Admin\CitiesController@store')->name('cities.store');
    Route::delete('cities-destroy/{id}', 'Admin\CitiesController@destroy')->name('cities.destroy');



    Route::match(['get', 'post'], 'schools', 'Admin\SchoolsController@index');
    Route::get('schools-create/{id?}', 'Admin\SchoolsController@create');
    Route::post('schools-store', 'Admin\SchoolsController@store')->name('schools.store');
    Route::delete('schools-destroy/{id}', 'Admin\SchoolsController@destroy')->name('schools.destroy');


    Route::get('cities-dd', 'Admin\SchoolsController@citiesDD');
    Route::get('class-dd', 'AdmissionsController@classDD')->name('class-dd');
    Route::get('child-dd', 'AdmissionsController@childDD')->name('child-dd');


    Route::match(['get', 'post'], 'school-classes/{id}', 'Admin\SchoolClassesController@index');
    Route::get('school-classes-create/{school_id}/{class_id?}', 'Admin\SchoolClassesController@create');
    Route::post('school-classes-store', 'Admin\SchoolClassesController@store')->name('school.classes.store');
    Route::delete('schools-classes-destroy/{id}', 'Admin\SchoolClassesController@destroy')->name('schools.classes.destroy');


    Route::get('admissions', 'AdmissionsController@index');
    Route::post('search_stripe', 'AdmissionsController@search_stripe')->name('admission.search_stripe');
    Route::get('questions/{type?}', 'QuestionsController@index')->name('questions.index');
    Route::post('questions/store', 'QuestionsController@store')->name('questions.store');
    Route::delete('questions/destroy/{id}', 'QuestionsController@destroy')->name('questions.destroy');

    Route::get('reports/search-all', 'Admin\ReportsController@searchAll')->name('reports.index');
    Route::get('dl-file-destroy/{id?}/{file?}', 'DemandLiberariesController@dlFileDestroy')->name('reports.index');
});
Route::get('load-students/{id}', 'Admin\SchoolClassesController@loadStd');

Route::prefix('parent')->name('parent.')->middleware('checkAuth')->group(function() {
    Route::get('dashboard', 'Parent\ParentsController@dashboard');
    Route::get('children/{user_id?}', 'Parent\ParentsController@children');
    Route::get('children-create/{id?}', 'Parent\ParentsController@childCreate');
    Route::post('children/store/{user_id?}', 'Parent\ParentsController@childStore')->name('children.store');
    Route::post('logout', 'Admin\UsersController@logout')->name('logout');
    Route::post('update-profile', 'Parent\ParentsController@updateProfile');
    Route::get('children-delete/{id}', 'Parent\ParentsController@deleteChild');
    Route::get('add-details/{id}', 'Parent\ParentsController@admissionDetails');
    Route::get('demand_library', 'Parent\ParentsController@demandLibrary');

    Route::get('stripesuccess', function () {
        return view('stripesuccess');
    });
    Route::post('stripepay', 'Parent\ParentsController@stripepay');
    Route::get('cancelsubscription/{id}', 'Parent\ParentsController@cancelstripesubscription');
});
Route::post('register-parent', 'Admin\UsersController@store');
Route::prefix('teacher')->name('teacher.')->middleware('checkAuth')->group(function() {
    Route::get('reports/search-all', 'Admin\ReportsController@searchAll')->name('reports.index');

    Route::get('demand-liberaries', 'DemandLiberariesController@index')->name('demand_liberaries.index');
    Route::get('dashboard', 'PagesController@dashboard');
    Route::match(['get', 'post'], 'schools', 'Admin\SchoolsController@index');
    Route::match(['get', 'post'], 'school-classes/{id}', 'Admin\SchoolClassesController@index');
    Route::post('logout', 'Admin\UsersController@logout')->name('logout');
    Route::get('change-password', 'Admin\UsersController@changePassword');
});
Route::get('thanks', 'FrontendController@thanks');
Route::get('get-liberary-details/{id}', 'DemandLiberariesController@getDetail');

Route::get('demo', function () {
    return redirect('/page/schedule-a-demo');
});
Route::get('school-partnerships', function () {
    return redirect('/page/school-partnerships');
});
Route::get('enroll-your-school.html', function () {
    return redirect('/page/schedule-a-demo');
});

Route::get('enrichmentatschool.html', function () {
    return redirect('/page/classes');
});
Route::get('enroll-your-child.html', function () {
    return redirect('/page/classes');
});


