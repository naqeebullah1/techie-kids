<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Team extends Model implements Auditable {

    use HasFactory,
        SoftDeletes,
        \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'name',
        'title',
        'image',
        'description',
        'is_active',
        'updated_by',
    ];

    protected static function boot() {
        parent::boot();
        
        static::creating(function($model) {
            $user = auth()->user();
            $model->created_by = $user->id;
            $model->updated_by = $user->id;
        });

        static::updating(function($model) {
//            dd('naq');
            $user = auth()->user();
            $model->updated_by = $user->id;
        });
    }

}
