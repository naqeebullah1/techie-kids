<div class="clearfix"></div>
<div class="row ">
    <div class="col-md-12">
        <div class="card">
            <h4 class="bold" style="padding:0px 10px;font-size:18px;"><a class="toggler" href="#" onClick="toggleSearch('searchfields')">{{ __('Advanced Search') }}<span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
            <?php
            $display = 'display:block;';
            $requestData = [
                "keyword" => null,
                "" => null,
            ];
            if (Request::all()) {
                $requestData = Request::all();
                $isData = 1;
            }
            if (isset($isData)) {
                $display = '';
            }
            extract($requestData, EXTR_PREFIX_ALL, "ex");
            ?>
            <div class="card-body" style="{{ $display }}" id="searchfields">
                <?php
                $user = Auth::user();
                $role = $user->ModelHasRoles->pluck('role_id')->toArray();
                
                $pf="backend";    
                if(in_array(2,$role)):
                $pf="teacher";    
                endif;
                ?>
                
                
                <form method="post" id="searchform" accept-charset="utf-8" action="{{url($pf.'/reports/search-all')}}">
                    @csrf
                    <div class="row">
                        <input type="hidden" name="sort_field" class="sort_field" value="">
                        <input type="hidden" name="sort_order" class="sort_order" value="">
                        <div class="col-lg-3">
                            <input autofocus="" type="text" name="keyword" value="{{ $ex_keyword }}" class=" searchfield form-control" placeholder="Keyword" id="keyword">     
                        </div>
                        <div class="col-lg-3">
                            <?php
                            $ddTypes = searchInThese();
                            ?>
                            <select id="only_this" name="only_this" class="form-control select2" >
                                <option value="">Select Type</option>
                                @foreach($ddTypes as $tk=>$typ)
                                <option value="<?=$tk?>"><?=$typ?></option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <!--<input type="reset" name="reset" class="btn btn-success" style="margin-top:25px;" value="Reset" onClick="resetForm('searchform')">-->
                            <input type="submit" name="search" class="btn btn-primary" value="Search" id="searchBnt">
                            <br>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>