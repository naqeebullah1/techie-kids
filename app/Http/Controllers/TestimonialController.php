<?php

namespace App\Http\Controllers;

use App\Models\Testimonial;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class TestimonialController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $testimonials_parents = Testimonial::where('type','1')->orderBy('is_active', 'desc')->get();
        $testimonials_schools = Testimonial::where('type','2')->orderBy('is_active', 'desc')->get();

        return view('testimonials.index', compact('testimonials_parents','testimonials_schools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $type = 'create';
        $testimonial = new Testimonial();
        return view('testimonials.form', compact('type', 'testimonial'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $valdidateData = $request->validate([
            'parent_name' => 'required',
            'relation_with_child' => 'required',
            'school' => 'required',
            'description' => 'required',
            'link' => 'nullable|image',
            'type' =>'required'
        ]);

//        $videoName = '';
//        $video = '';
//        if ($request->hasFile('link')) {
//            $video = $request->file('link');
//            $videoName = time() . '.' . $video->getClientOriginalExtension();
//        }

$imageName = '';
 if ($request->hasFile('link')) {
            $image = $request->file('link');

            $imageName = time() . '.' . $image->extension();

            $destinationPath = public_path('/frontend/images/testimonial');

            $img = Image::make($image->path());
            $img->resize(300, 300, function ($constraint) {

                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $imageName);
        }

        $data = array(
           'parent_name' => $request->parent_name,
            'relation_with_child' =>  $request->relation_with_child,
            'school' =>  $request->school,
            'description' =>  $request->description,
            'link' => $imageName,
            'type' => $request->type
        );

        $outcome = '';

        try {
            DB::beginTransaction();

            $obj = Testimonial::create($data);

//            if ($obj != '') {
//                $video->move(public_path('/video'), $videoName);
//            }

            DB::commit();
            $outcome = 'Testimonial has been added successfully.';
        } catch (QueryException $e) {
            DB::rollBack();
            dd($e);
        }

        return redirect()->route('admin.testimonials.index')->with('outcome', $outcome);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $testimonial = Testimonial::find($id);
        $type = 'update';
        return view('testimonials.form', compact('type', 'testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $valdidateData = $request->validate([
            'parent_name' => 'required',
            'relation_with_child' => 'required',
            'school' => 'required',
            'description' => 'required',
            'type' => 'required',
            'link' => 'nullable|image'
        ]);

//        $videoName = '';
//        $video = '';
//        if ($request->hasFile('link')) {
//            $video = $request->file('link');
//            $videoName = time() . '.' . $video->getClientOriginalExtension();
//        }

        $imageName = '';
         if ($request->hasFile('link')) {
            $image = $request->file('link');

            $imageName = time() . '.' . $image->extension();

            $destinationPath = public_path('/frontend/images/testimonial');

            $img = Image::make($image->path());
            $img->resize(300, 300, function ($constraint) {

                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $imageName);
        }

        $data = array(
           'parent_name' => $request->parent_name,
            'relation_with_child' =>  $request->relation_with_child,
            'school' =>  $request->school,
            'description' =>  $request->description,
            'type' =>  $request->type,
            'link' => $imageName
        );


        $outcome = '';

        try {
            DB::beginTransaction();
            $data['updated_by'] = '';
            Testimonial::find($id)
                    ->update($data);

            DB::commit();
            $outcome = 'Testimonial video has been updated successfully.';
        } catch (QueryException $e) {
            DB::rollBack();
            dd($e);
        }

        return redirect()->route('admin.testimonials.index')->with('outcome', $outcome);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $outcome = '';
        try {
            DB::beginTransaction();
            Testimonial::where('id', $id)->delete();
            $outcome = "Testimonial video deleted successfully.";

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

}
