<?php
$gallery = getGallery();
?>
<div class="subscribe__position">

    <div class="dacre__kidz__area section-padding--xs bg-image--2" style="background-image:url('<?= asset('images/thumbnails/' . $gallery->image) ?>')"><!--bg-image--2-->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section__title text-center">
                        <h1 class=""><?= $gallery->title ?></h1>
                        <p><strong>
                                <?= $gallery->description ?>    
                            </strong></p>
                    </div>
                </div>
            </div>
        </div>
        <section class="kids-code-slider slider" style="margin-top:20px;">
            <?php
            $images = $gallery->images;
            ?>
            @foreach($images as $image)
            <div class="slide">
                <img src="<?= asset('images/thumbnails/' . $image->image) ?>">
            </div>
            @endforeach

<!--            <div class="slide"><img src="https://techiekidsclub.com/wp-content/uploads/2022/09/Coach-dancing-with-kids-in-classroom-300x200.jpg"></div>
<div class="slide"><img src="https://techiekidsclub.com/wp-content/uploads/2022/09/Boy-on-rug-with-robot-300x185.jpg"></div>
<div class="slide"><img src="https://techiekidsclub.com/wp-content/uploads/2022/09/Kids-x-3-happy-on-rug-300x181.png"></div>
<div class="slide"><img src="https://techiekidsclub.com/wp-content/uploads/2022/09/Boy-with-lego-train-on-floor-300x200.jpg"></div>
<div class="slide"><img src="https://techiekidsclub.com/wp-content/uploads/2022/09/Kids-x-3-with-Squishy-Circuits-at-table-300x200.png"></div>
<div class="slide"><img src="https://techiekidsclub.com/wp-content/uploads/2022/09/Coach-and-kids-with-smart-board-300x196.jpg"></div>-->

        </section>
    </div>
</div>