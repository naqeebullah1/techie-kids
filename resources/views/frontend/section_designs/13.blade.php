<div class="ht__bradcaump__area">
    <div class="ht__bradcaump__container">
        <div class="ht_image_wrapper" style="background-position: center;background-size: cover;background-image:url('<?=asset('images/thumbnails/'.$section->image)?>')">
        <!--<img src="<?=asset('images/thumbnails/'.$section->image)?>" class="ht_banner_image" alt="{{$section->title}}">-->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="slider__activation" style="margin-top:10%">
                        <!-- Start Single Slide -->
                        <div class="slide">
                            <div class="slide__inner">
                                <h1><?=$section->title?></h1>
                            <?php if($section->sub_title) { ?>
                            <div class="slider__text">
                                    <h2><?=$section->sub_title?></h2>                                
                            </div>
                            <?php } ?>
                                                              
                        </div>
                        </div>
                        <!-- End Single Slide -->
                    </div>
                    </div>
                    </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        
                        <nav class="bradcaump-inner">
                            <a class="breadcrumb-item" href="<?=url('/')?>">Home</a>
                            <span class="brd-separetor"><img src="<?=asset('frontend/images/icons/brad.png')?>" alt="separator images"></span>
                            <span class="breadcrumb-item active"><?=$page->title?></span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>