@csrf
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row clearfix">
    <div class="col-md-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="Type" class="col-md-12 col-form-label text-md-left">Section Type</label>

            {{ Form::select('section_type',sectionTypes(),$page->section_type,['onchange'=>'toggleFields()','id'=>'section_type','class'=>'form-control select2']) }}

        </div>
    </div>
</div>
<div class="row clearfix">

    
    <div class="col-md-12 d-none  ss s-1 s-3 s-13 s-2 s-4 s-18 s-5 s-9 s-15 s-16 s-7 s-17 s-19 s-20">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="title" class="col-md-12 col-form-label text-md-left">Title</label>
            <input type="text" class="form-control " name="title" value="{{ $page->title }}" required="" autocomplete="off" autofocus="">
        </div>
    </div>
    <div class="col-md-12 d-none  ss s-2 s-13 s-3 s-5 s-18 s-9 s-15 s-16 s-17">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="link" class="col-md-12 col-form-label text-md-left">Sub Title</label>
            <input type="text" class="form-control " name="sub_title" value="{{ $page->sub_title }}" required="" autocomplete="off" autofocus="">
        </div>
    </div>
    <div class="col-md-12 d-none  ss s-2">
        <div class="form-group form-group-default required" aria-required="true">
            <label for="link" class="col-md-12 col-form-label text-md-left">Youtube Link</label>
            <input type="text" class="form-control " name="link" value="{{ $page->link }}" required="" autocomplete="off" autofocus="">
        </div>
    </div>
    <div class="col-md-12 d-none  ss s-2 s-3 s-13 s-4 s-5 mb-2 s-18">
        <label for="title" class="col-md-12 col-form-label text-md-left">Image</label>
        <input type="file" class="dropify"
               @if($page->image)
               data-default-file="{{ asset('images/thumbnails/'.$page->image) }}"
               @endif

               name="image">
    </div>
    <div class="col-md-12 d-none  ss s-3 s-2 s-4 s-18 s-12">
        <label for="section" class="col-md-12 col-form-label text-md-left">
            Content <span class="required">*</span>
        </label>
        <textarea class="section form-control" name="section" required="">{{ $page->section }}</textarea>
    </div>
    <div class="col-12 d-none ss s-1 s-2 s-4 s-3 s-15 not-required" style="margin-bottom:10px;">
                <div class="row">
                    <div class="col-md-4">
                        <label for="section" class="col-md-12 col-form-label text-md-left">Button </label>
                        <input type="text" class="form-control" name="btn_title" value="{{ $page->btn_title }}">
                    </div>
                    <div class="col-md-4">
                        <label for="section" class="col-md-12 col-form-label text-md-left">Button Icon</label>
                        <input type="text" class="form-control" name="btn_icon" value="{{ $page->btn_icon }}" >
                    </div>
                    <div class="col-md-4">
                        <label for="section" class="col-md-12 col-form-label text-md-left">Button Link</label>
                        <input type="text" class="form-control" name="btn_link" value="{{ $page->btn_link }}">
                    </div>
                </div>
            </div>
</div>

<!--This FOrm is used for Sections having 3 columns-->

<?php
$eo = new \App\Models\PageDetailSection;
$sections = [
    $eo,
    $eo,
    $eo
];
if ($page->sections && $page->sections->count()) {
    $sections = $page->sections;
}
?>
<div class="row clearfix s-1 ss s-19" >
    @foreach($sections as $k=>$section)
    <div class="col-12 mb-2" style="background: #f8f8f8;padding:10px">
        <h4 class="m-0 mb-2">Section {{ $k+1 }}</h4>
        <div class="row">

            <div class="col-md-12">
                <div class="form-group form-group-default required" aria-required="true">
                    <label for="title" class="col-md-12 col-form-label text-md-left">Title</label>
                    <input type="hidden" class="form-control " name="sec3[<?= $k ?>][id]" value="{{ $section->id }}" required="" autocomplete="off" autofocus="">
                    <input type="text" class="form-control " name="sec3[<?= $k ?>][title]" value="{{ $section->title }}" required="" autocomplete="off" autofocus="">
                </div>
            </div>
            <div class="col-md-12 s-1 ss">
                <label for="title" class="col-md-12 col-form-label text-md-left">Image</label>
                <input type="file" class="dropify" name="sec3[<?= $k ?>][image]" data-default-file="{{ asset('images/thumbnails/'.$section->icon) }}" value="{{ $section->icon }}">
            </div>

            <div class="col-md-12">
                <label for="section" class="col-md-12 col-form-label text-md-left">Content <span class="required">*</span></label>
                <textarea class="form-control editor" id="editorck<?php echo $k; ?>" name="sec3[<?= $k ?>][description]" required="">{{ $section->description }}</textarea>
            </div>
            <div class="col-12 ss s-1 not-required">
                <div class="row">
                    <div class="col-md-4">
                        <label for="section" class="col-md-12 col-form-label text-md-left">Button </label>
                        <input type="text" class="form-control" name="sec3[<?= $k ?>][btn_title]" value="{{ $section->btn_title }}">
                    </div>
                    <div class="col-md-4">
                        <label for="section" class="col-md-12 col-form-label text-md-left">Button Icon</label>
                        <input type="text" class="form-control" name="sec3[<?= $k ?>][btn_icon]" value="{{ $section->btn_icon }}" >
                    </div>
                    <div class="col-md-4">
                        <label for="section" class="col-md-12 col-form-label text-md-left">Button Link</label>
                        <input type="text" class="form-control" name="sec3[<?= $k ?>][btn_link]" value="{{ $section->btn_link }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
<!--This Section is used for 2 columns section-->
<?php
$eo = new \App\Models\PageDetailSection;
$sections = [
    $eo,
    $eo,
    $eo,
    $eo,
    $eo,
    $eo,
];
if ($page->sections && $page->sections->count()) {
    $sections = $page->sections;
}
?>

<?php
$eo = new \App\Models\PageDetailSection;
$sections = [
    $eo,
    $eo,
    $eo,
    $eo,
];
if ($page->sections && $page->sections->count()) {
    $sections = $page->sections;
}
?>
<div class="row clearfix s-15 s-16 ss " >
    @foreach($sections as $k=>$section)
    <div class="col-12 mb-2" style="background: #f8f8f8;padding:10px">
        <h4 class="m-0 mb-2">Section {{ $k+1 }}</h4>
        <div class="row">

            <div class="col-md-12">
                <div class="form-group form-group-default required" aria-required="true">
                    <label for="title" class="col-md-12 col-form-label text-md-left">Title</label>
                    <input type="hidden" class="form-control " name="section4[<?= $k ?>][id]" value="{{ $section->id }}" required="" autocomplete="off" autofocus="">
                    <input type="text" class="form-control " name="section4[<?= $k ?>][title]" value="{{ $section->title }}" required="" autocomplete="off" autofocus="">
                </div>
            </div>
            <!--<div class="col-md-12 ss s-16">
                <div class="form-group form-group-default required" aria-required="true">
                    <label for="icon" class="col-md-12 col-form-label text-md-left">Icon</label>
                    <input class="form-control " type="text" name="section4[<?= $k ?>][icon]" value="{{ $section->icon }}">
                </div>
            </div>-->
            <div class="col-md-12 ss s-15">
                <label for="title" class="col-md-12 col-form-label text-md-left">Image</label>
                <input type="file" class="dropify" name="section4[<?= $k ?>][image]" data-default-file="{{ ($section->icon)?asset('images/thumbnails/'.$section->icon):'' }}" value="{{ $section->icon }}">
            </div>
            <div class="col-md-12">

                <label for="section" class="col-md-12 col-form-label text-md-left">Content <span class="required">*</span></label>
                <textarea class="form-control ckeditor" name="section4[<?= $k ?>][description]" required="">{{ $section->description }}</textarea>
            </div>
        </div>
    </div>
    @endforeach
</div>








<?php
$eo = new \App\Models\PageDetailSection;
$sections = [
    $eo,
    $eo,
    $eo,
    $eo,
    $eo,
    $eo,
];
if ($page->sections && $page->sections->count()) {
    $sections = $page->sections;
}
?>




<div class="row clearfix s-5 ss " >
    @foreach($sections as $k=>$section)
    <div class="col-12 mb-2" style="background: #f8f8f8;padding:10px">
        <h4 class="m-0 mb-2">Section {{ $k+1 }}</h4>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group form-group-default required" aria-required="true">
                    <label for="title" class="col-md-12 col-form-label text-md-left">Title</label>
                    <input type="hidden" class="form-control " name="section5[<?= $k ?>][id]" value="{{ $section->id }}" required="" autocomplete="off" autofocus="">
                    <input type="hidden" class="form-control " name="section5[<?= $k ?>][icon]" value="{{ $section->icon }}" required="" autocomplete="off" autofocus="">
                    <input type="text" class="form-control " name="section5[<?= $k ?>][title]" value="{{ $section->title }}" required="" autocomplete="off" autofocus="">
                </div>
            </div>
            <!--<div class="col-md-6">
                <div class="form-group form-group-default required" aria-required="true">
                    <label for="title" class="col-md-12 col-form-label text-md-left">Icon</label>
                    <input type="text" class="form-control" name="section5[<?= $k ?>][icon]" value="{{ $section->icon }}" required="">
                </div>
            </div>-->
            <div class="col-md-12">
                <label for="title" class="col-md-12 col-form-label text-md-left">Image</label>
                <input type="file" class="dropify" name="section5[<?= $k ?>][icon]" data-default-file="{{ asset('images/thumbnails/'.$section->icon) }}" value="{{ $section->icon }}">
            </div>

            <div class="col-md-12">
                <label for="section" class="col-md-12 col-form-label text-md-left">Content <span class="required">*</span></label>
                <textarea class="form-control editor" name="section5[<?= $k ?>][description]" required="">{{ $section->description }}</textarea>
            </div>
        </div>
    </div>
    @endforeach
</div>
<div class="form-group row mt-2">
    <div class="col-md-12 offset-md-12 text-right">
        <button id="save-form" type="submit" class="btn btn-primary">
            @isset($create)
            {{ __('Create') }}
            @else
            {{ __('Update') }}
            @endisset
        </button>
    </div>
</div>
<script>
    $(document).ready(function () {
        toggleFields();
    });
    function toggleFields() {
        var section_type = $('#section_type').val();
        $('.ss').addClass('d-none');
        $('.s-' + section_type).removeClass('d-none');

        $('.ss').find('input[type="text"], textarea').attr('required', false);
        $('.s-' + section_type).find('input[type="text"]').attr('required', true);
        $('.not-required').find('input[type="text"]').attr('required', false);
        if(section_type == 9 ||section_type == 1) {
            $('.s-9').find('input[type="text"], textarea').attr('required', false);
            $('.s-9 .required').removeClass("required");
            $('.s-1').find('input[type="text"], textarea').attr('required', false);
            $('.s-1 .required').removeClass("required");
        }
        if(section_type == 19){
            CKEDITOR.replace( 'editorck0');
            CKEDITOR.replace( 'editorck1');
            CKEDITOR.replace( 'editorck2');
        }
        
    }
</script>