<!DOCTYPE html>
<html lang="en-US">

    <!-- Mirrored from thundersecurity.beecreatives.net/contact/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Dec 2022 18:25:22 GMT -->
    <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Contact &#8211; </title>
        <link rel="shortcut icon" sizes="16x16 32x32 64x64" href="<?= asset('public/frontend/wp-content/uploads/2022/11/fav.png') ?>">

        <meta name='robots' content='max-image-preview:large' />
        <link rel="alternate" type="application/rss+xml" title="Thunder Security &raquo; Feed" href="<?= asset('public/frontend') ?>/feed/index.html" />
        <link rel="alternate" type="application/rss+xml" title="Thunder Security &raquo; Comments Feed" href="<?= asset('public/frontend') ?>/comments/feed/index.html" />

        <link rel='stylesheet' id='elementor-frontend-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/css/frontend.min2e46.css?ver=3.9.2' media='all' />
        <link rel='stylesheet' id='elementor-post-1304-css' href='<?= asset('public/frontend') ?>/wp-content/uploads/elementor/css/post-13044e52.css?ver=1672161332' media='all' />
        <link rel='stylesheet' id='font-awesome-5-all-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/font-awesome/css/all.min2e46.css?ver=3.9.2' media='all' />
        <link rel='stylesheet' id='font-awesome-4-shim-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/font-awesome/css/v4-shims.min2e46.css?ver=3.9.2' media='all' />
        <link rel='stylesheet' id='elementor-post-1311-css' href='<?= asset('public/frontend') ?>/wp-content/uploads/elementor/css/post-13114e52.css?ver=1672161332' media='all' />
        <link rel='stylesheet' id='jkit-elements-main-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/jeg-elementor-kit/assets/css/elements/main28b8.css?ver=2.5.12' media='all' />
        <link rel='stylesheet' id='wp-block-library-css' href='<?= asset('public/frontend') ?>/wp-includes/css/dist/block-library/style.min6a4d.css?ver=6.1.1' media='all' />
        <link rel='stylesheet' id='classic-theme-styles-css' href='<?= asset('public/frontend') ?>/wp-includes/css/classic-themes.min68b3.css?ver=1' media='all' />
        <link rel='stylesheet' id='template-kit-export-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/template-kit-export/public/assets/css/template-kit-export-public.min365c.css?ver=1.0.21' media='all' />
        <link rel='stylesheet' id='swiper-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/assets/plugins/swiper/swiper.min6a4d.css?ver=6.1.1' media='all' />
        <link rel='stylesheet' id='qi-addons-for-elementor-grid-style-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/assets/css/grid.min6a4d.css?ver=6.1.1' media='all' />
        <link rel='stylesheet' id='qi-addons-for-elementor-helper-parts-style-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/assets/css/helper-parts.min6a4d.css?ver=6.1.1' media='all' />
        <link rel='stylesheet' id='qi-addons-for-elementor-style-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/assets/css/main.min6a4d.css?ver=6.1.1' media='all' />
        <link rel='stylesheet' id='elementor-icons-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min91ce.css?ver=5.16.0' media='all' />
        <link rel='stylesheet' id='elementor-post-5-css' href='<?= asset('public/frontend') ?>/wp-content/uploads/elementor/css/post-54e52.css?ver=1672161332' media='all' />
        <link rel='stylesheet' id='elementor-post-91-css' href='<?= asset('public/frontend') ?>/wp-content/uploads/elementor/css/post-91b6fb.css?ver=1672233308' media='all' />
        <link rel='stylesheet' id='hello-elementor-css' href='<?= asset('public/frontend') ?>/wp-content/themes/hello-elementor/style.minc141.css?ver=2.6.1' media='all' />
        <link rel='stylesheet' id='hello-elementor-theme-style-css' href='<?= asset('public/frontend') ?>/wp-content/themes/hello-elementor/theme.minc141.css?ver=2.6.1' media='all' />
        <link rel='stylesheet' id='elementor-icons-ekiticons-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementskit-lite/modules/elementskit-icon-pack/assets/css/ekiticonsf71b.css?ver=2.8.0' media='all' />
        <link rel='stylesheet' id='skb-cife-elegant_icon-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/skyboot-custom-icons-for-elementor/assets/css/elegant97de.css?ver=1.0.5' media='all' />
        <link rel='stylesheet' id='skb-cife-linearicons_icon-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/skyboot-custom-icons-for-elementor/assets/css/linearicons97de.css?ver=1.0.5' media='all' />
        <link rel='stylesheet' id='skb-cife-themify_icon-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/skyboot-custom-icons-for-elementor/assets/css/themify97de.css?ver=1.0.5' media='all' />
        <link rel='stylesheet' id='ekit-widget-styles-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementskit-lite/widgets/init/assets/css/widget-stylesf71b.css?ver=2.8.0' media='all' />
        <link rel='stylesheet' id='ekit-responsive-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementskit-lite/widgets/init/assets/css/responsivef71b.css?ver=2.8.0' media='all' />
        <link rel='stylesheet' id='google-fonts-1-css' href='https://fonts.googleapis.com/css?family=Inter%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;display=swap&amp;ver=6.1.1' media='all' />
        <link rel='stylesheet' id='elementor-icons-shared-0-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min52d5.css?ver=5.15.3' media='all' />
        <link rel='stylesheet' id='elementor-icons-fa-brands-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min52d5.css?ver=5.15.3' media='all' />
        <link rel='stylesheet' id='elementor-icons-skb_cife-elegant-icon-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/skyboot-custom-icons-for-elementor/assets/css/elegant97de.css?ver=1.0.5' media='all' />
        <link rel='stylesheet' id='elementor-icons-fa-solid-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min52d5.css?ver=5.15.3' media='all' />
        <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin><script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/font-awesome/js/v4-shims.min2e46.js?ver=3.9.2' id='font-awesome-4-shim-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/jquery/jquery.mina7a0.js?ver=3.6.1' id='jquery-core-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/jquery/jquery-migrate.mind617.js?ver=3.3.2' id='jquery-migrate-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/template-kit-export/public/assets/js/template-kit-export-public.min365c.js?ver=1.0.21' id='template-kit-export-js'></script>
        <link rel="https://api.w.org/" href="<?= asset('public/frontend') ?>/wp-json/index.html" /><link rel="alternate" type="application/json" href="<?= asset('public/frontend') ?>/wp-json/wp/v2/pages/91.json" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="<?= asset('public/frontend') ?>/xmlrpc0db0.html?rsd" />
        <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="<?= asset('public/frontend') ?>/wp-includes/wlwmanifest.xml" />
        <meta name="generator" content="WordPress 6.1.1" />
        <link rel="canonical" href="index.html" />
        <link rel='shortlink' href='<?= asset('public/frontend') ?>/index5cfa.html?p=91' />
        <link rel="alternate" type="application/json+oembed" href="<?= asset('public/frontend') ?>/wp-json/oembed/1.0/embed9b20.json?url=https%3A%2F%2Fthundersecurity.beecreatives.net%2Fcontact%2F" />
        <link rel="alternate" type="text/xml+oembed" href="<?= asset('public/frontend') ?>/wp-json/oembed/1.0/embed3c94?url=https%3A%2F%2Fthundersecurity.beecreatives.net%2Fcontact%2F&amp;format=xml" />
    </head>
    <body class="page-template page-template-elementor_header_footer page page-id-91 qodef-qi--no-touch qi-addons-for-elementor-1.5.6 jkit-color-scheme elementor-default elementor-template-full-width elementor-kit-5 elementor-page elementor-page-91">
        @include('layouts.partials.header')
        <div data-elementor-type="wp-page" data-elementor-id="91" class="elementor elementor-91">
<!--            <section class="elementor-section elementor-top-section elementor-element elementor-element-4ed5697 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle" data-id="4ed5697" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-background-overlay"></div>
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-ecee1ca" data-id="ecee1ca" data-element_type="column">
                        <div class="elementor-widget-wrap elementor-element-populated">
                            <div class="elementor-element elementor-element-b312422 elementor-widget elementor-widget-heading" data-id="b312422" data-element_type="widget" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                    <h2 class="elementor-heading-title elementor-size-default">Contact Us</h2>	
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-7e1d7a4 elementor-icon-list--layout-inline elementor-align-center elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="7e1d7a4" data-element_type="widget" data-widget_type="icon-list.default">
                                <div class="elementor-widget-container">
                                    <ul class="elementor-icon-list-items elementor-inline-items">
                                        <li class="elementor-icon-list-item elementor-inline-item">
                                            <span class="elementor-icon-list-text">Home</span>
                                        </li>
                                        <li class="elementor-icon-list-item elementor-inline-item">
                                            <span class="elementor-icon-list-text">/ Contact Us</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>-->
            <!--           
            <section class="elementor-section elementor-top-section elementor-element elementor-element-4ed5697 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle" data-id="4ed5697" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-ecee1ca" data-id="ecee1ca" data-element_type="column">
                                    <div class="elementor-widget-wrap elementor-element-populated">
                                        <div class="elementor-element elementor-element-b312422 elementor-widget elementor-widget-heading" data-id="b312422" data-element_type="widget" data-widget_type="heading.default">
                                            <div class="elementor-widget-container">
                                                <h2 style="font-size:2.25em;line-height: 2;" class="elementor-heading-title ">Find a Security Camera Installer in St. Petersburg, FL</h2>	
                                                <h3 style="font-size:1.5em;margin-bottom: 20px" class="elementor-heading-title ">PROVIDING SECURITY CAMERA SERVICES AT AFFORDABLE PRICES</h3>
                                                <p class="elementor-heading-title " style="font-size:1.0625em;line-height: 1.4;margin-bottom: 30px">Thank you for visiting the website of Express Data Connects, LLC. If you want to modernize your home with innovative technology, we can help you reach your goals. We provide TV mounting, network wiring and security camera services in St. Petersburg, FL. We also offer a variety of handyman services to help homeowners maintain beautiful and functional living spaces.</p>
                                                <p class="elementor-heading-title " style="font-size:1.0625em;line-height: 1.4;margin-bottom: 30px">Please use the form on this page to email us. You can also call 813-557-9250 to speak with us immediately.</p>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </section>-->
       <!--<section style="padding:50px 0;background-color: #1E3143;" class="elementor-section elementor-top-section elementor-element elementor-element-80b61af elementor-section-height-min-height elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-items-middle" data-id="80b61af" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">-->
            <?php
            $sideDetails = siteDetails();
            ?>
            <section style="padding:50px" class="elementor-section elementor-top-section elementor-element elementor-element-4ed5697 elementor-section-height-min-height elementor-section-boxed elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-items-middle" data-id="4ed5697" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-background-overlay" style="background-image: url('<?= asset('public/frontend/wp-content/uploads/2022/11/cctv-surveillance-camera-in-singapore.jpg') ?>')"></div>
                                <!--style="background-image: url('<?= asset('public/frontend/wp-content/uploads/2022/11/cctv-surveillance-camera-in-singapore.jpg') ?>')"-->
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-9250804 animated-slow elementor-invisible" data-id="9250804" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;zoomIn&quot;}">
                        <div class="elementor-widget-wrap elementor-element-populated">
                            <div class="elementor-element elementor-element-13cbc5f elementor-widget elementor-widget-elementskit-heading" data-id="13cbc5f" data-element_type="widget" data-widget_type="elementskit-heading.default">
                                <div class="elementor-widget-container">
                                    <div class="ekit-wid-con" >
                                        <div class="ekit-heading elementskit-section-title-wraper text_center   ekit_heading_tablet-   ekit_heading_mobile-">
                                            <?= $sideDetails->description ?>   
                                        </div>	
                                    </div>
                                </div>
                                <!--                            <div class="elementor-element elementor-element-ca7609b elementor-align-center elementor-widget elementor-widget-button" data-id="ca7609b" data-element_type="widget" data-widget_type="button.default">
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-button-wrapper">
                                                                        <a href="#" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                                                                            <span class="elementor-button-content-wrapper">
                                                                                <span class="elementor-button-text">Contact Us</span>
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>-->
                            </div>
                        </div>
                    </div>
            </section>
            <section class="elementor-section elementor-top-section elementor-element elementor-element-a0e9453 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="a0e9453" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-90e1626 animated-slow elementor-invisible" data-id="90e1626" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;slideInLeft&quot;}">
                        <div class="elementor-widget-wrap elementor-element-populated">
                            <div class="elementor-element elementor-element-636b1cf elementor-widget elementor-widget-metform" data-id="636b1cf" data-element_type="widget" data-widget_type="metform.default">
                                <div class="elementor-widget-container">
                                    <div id="mf-response-props-id-1576" data-previous-steps-style="" data-editswitchopen="" data-response_type="alert" data-erroricon="fas fa-exclamation-triangle" data-successicon="fas fa-check" data-messageposition="top" class="   mf-scroll-top-no">
                                        <div class="formpicker_warper formpicker_warper_editable" data-metform-formpicker-key="1576" >

                                            <div class="elementor-widget-container">

                                                <div
                                                    id="metform-wrap-636b1cf-1576"
                                                    class="mf-form-wrapper"
                                                    data-form-id="1576"
                                                    data-action="<?= asset('public/frontend') ?>/wp-json/metform/v1/entries/insert/1576.json"
                                                    data-wp-nonce="ee7bbe7e72"
                                                    data-form-nonce="fc3df8a080"
                                                    data-quiz-summery = "false"
                                                    data-save-progress = "false"
                                                    data-form-type="general-form"
                                                    data-stop-vertical-effect=""
                                                    >

                                                    <form action="<?= route('sendMessage') ?>" method="post" class="metform-form-content">
                                                        @csrf
                                                        <div class="mf-main-response-wrap mf-response-msg-wrap" data-show="0">
                                                            <div class="mf-response-msg">
                                                                <i class="mf-success-icon fas fa-check"></i>
                                                                <p></p>
                                                            </div>
                                                        </div>
                                                        <div class="metform-form-main-wrapper">
                                                            <div data-elementor-type="wp-post" data-elementor-id="1576" class="elementor elementor-1576">
                                                                <section
                                                                    class="elementor-section elementor-top-section elementor-element elementor-element-fcd7001 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                                    data-id="fcd7001"
                                                                    data-element_type="section"
                                                                    >
                                                                        <?php
                                                                        $f = $l = $e = $s = $m = '';
                                                                        if (old('contact')) {
                                                                            $cont = old('contact');
                                                                            $f = $cont['first_name'];
                                                                            $l = $cont['last_name'];
                                                                            $e = $cont['email'];
                                                                            $s = $cont['subject'];
                                                                            $m = $cont['message'];
                                                                        }
                                                                        ?>

                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-cd7b8c6" data-id="cd7b8c6" data-element_type="column">
                                                                            <div class="elementor-widget-wrap elementor-element-populated">

                                                                                <div class="elementor-element elementor-element-6afb75a elementor-widget elementor-widget-mf-text">
                                                                                    <style>
                                                                                        .alert-danger{
                                                                                            color: #721c24;
                                                                                            background-color: #f8d7da;
                                                                                            border-color: #f5c6cb;  
                                                                                        }
                                                                                        .alert{
                                                                                            position: relative;
                                                                                            padding: 0.75rem 1.25rem;
                                                                                            margin-bottom: 1rem;
                                                                                            border: 1px solid transparent;
                                                                                            border-radius: 0.25rem;

                                                                                        }
                                                                                        .alert-success{
                                                                                            color: #155724;
                                                                                            background-color: #d4edda;
                                                                                            border-color: #c3e6cb;
                                                                                        }
                                                                                    </style>
                                                                                    @if($errors->any())
                                                                                    <?= implode('', $errors->all('<div class="alert alert-danger">:message</div>')) ?>
                                                                                    @endif

                                                                                    @if(session()->has('error'))
                                                                                    <div class="alert alert-danger">
                                                                                        {{Session::get('error')}}
                                                                                    </div>
                                                                                    @endif
                                                                                    @if(session()->has('success'))
                                                                                    <div class="alert alert-success" role="alert">
                                                                                        {{Session::get('success')}}
                                                                                    </div>
                                                                                    @endif


                                                                                </div>
                                                                                <div class="elementor-element elementor-element-6afb75a elementor-widget elementor-widget-mf-text">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div style="display: flex;" class="mf-input-wrapper">
                                                                                            <input value="<?= $f ?>" type="text" style="margin-right: 10px;" class="mf-input" id="mf-input-text-6afb75a" name="contact[first_name]" placeholder="First Name " aria-invalid="false" />
                                                                                            <input value="<?= $l ?>" type="text" class="mf-input" id="mf-input-text-6afb75a" name="contact[last_name]" placeholder="Last Name " aria-invalid="false" />
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-element elementor-element-6e5dd33 elementor-widget elementor-widget-mf-email">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="mf-input-wrapper">
                                                                                            <input value="<?= $e ?>" type="email" class="mf-input" id="mf-input-email-6e5dd33" name="contact[email]" placeholder="Enter Your Email " aria-invalid="false" value="" /></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-element elementor-element-6e5dd33 elementor-widget elementor-widget-mf-email">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="mf-input-wrapper">
                                                                                            <input value="<?= $s ?>" type="subject" class="mf-input" name="contact[subject]" placeholder="Subject" aria-invalid="false" value="" /></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-element elementor-element-d415ac9 elementor-widget elementor-widget-mf-textarea">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="mf-input-wrapper">
                                                                                            <textarea class="mf-input mf-textarea" id="mf-input-text-area-d415ac9" name="contact[message]" placeholder="Your Message " cols="30" rows="10" aria-invalid="false"><?= $m ?></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="margin-bottom: 0;" class="elementor-element elementor-element-d415ac9 elementor-widget elementor-widget-mf-textarea">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="mf-input-wrapper">
                                                                                            <h5>What Service Are You Interested In Today?</h5>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <?php
                                                                                $systemServices = services();
                                                                                ?>

                                                                                <div class="elementor-element elementor-element-d415ac9 elementor-widget elementor-widget-mf-textarea">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="mf-input-wrapper">
                                                                                            @foreach($systemServices as $k=>$systemService )
                                                                                            <label>
                                                                                                <input value="<?=$systemService->id?>" type="checkbox" name="service[]">
                                                                                                <?=$systemService->title?>
                                                                                            </label>
                                                                                            <br>
                                                                                            @endforeach
                                                                                        </div>
                                                                                    </div>
                                                                                </div>



                                                                                <div
                                                                                    class="elementor-element elementor-element-b08b88f mf-btn--left mf-btn--mobile-justify elementor-widget elementor-widget-mf-button"
                                                                                    data-id="b08b88f"
                                                                                    data-element_type="widget"
                                                                                    data-widget_type="mf-button.default"
                                                                                    >
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="mf-btn-wraper" data-mf-form-conditional-logic-requirement="">
                                                                                            <button type="submit" class="metform-btn metform-submit-btn" id=""><span>Send Message </span></button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>



                                            </div>
                                        </div>
                                    </div>		
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-9b419fe" data-id="9b419fe" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-widget-wrap elementor-element-populated">
                            <div class="elementor-element elementor-element-3f3fb56 elementor-widget-divider--view-line_text elementor-widget-divider--element-align-center elementor-widget elementor-widget-divider" data-id="3f3fb56" data-element_type="widget" data-widget_type="divider.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-divider">
                                        <span class="elementor-divider-separator">
                                            <span class="elementor-divider__text elementor-divider__element">
                                                Contact	Us			</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div style="margin-bottom:0;" class="elementor-element elementor-element-5a8a0d3 elementor-widget elementor-widget-elementskit-heading" data-id="5a8a0d3" data-element_type="widget" data-widget_type="elementskit-heading.default">
                                <div style="margin-bottom:0;" class="elementor-widget-container">
                                    <div class="ekit-wid-con" ><div class="ekit-heading elementskit-section-title-wraper text_left   ekit_heading_tablet-   ekit_heading_mobile-text_left">
                                            <h2 class="ekit-heading--title elementskit-section-title " style="font-size: 32px;line-height: 1">EXPRESS DATA CONNECTS, LLC</h2>			
                                            <div class='ekit-heading__description'>
                                                <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus,ullamcorper mattis, pulvinar dapibus leo.</p>-->
                                            </div>
                                        </div></div>		
                                </div>
                            </div>

                            <div class="elementor-element elementor-element-22cd972 elementor-view-framed elementor-shape-square elementor-position-left elementor-vertical-align-bottom animated-slow elementor-mobile-position-top elementor-invisible elementor-widget elementor-widget-icon-box" data-id="22cd972" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;slideInUp&quot;}" data-widget_type="icon-box.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-icon-box-wrapper">
                                        <div class="elementor-icon-box-icon">
                                            <span class="elementor-icon elementor-animation-" >
                                                <i aria-hidden="true" class="fas fa-map-marker-alt"></i>				</span>
                                        </div>
                                        <div class="elementor-icon-box-content">
                                            <h4 class="elementor-icon-box-title">
                                                <span>
                                                    Location			
                                                </span>
                                            </h4>
                                            <p class="elementor-icon-box-description">
                                                <?= $sideDetails->address ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-985987e elementor-view-framed elementor-shape-square elementor-position-left elementor-vertical-align-bottom animated-slow elementor-mobile-position-top elementor-invisible elementor-widget elementor-widget-icon-box" data-id="985987e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;slideInUp&quot;}" data-widget_type="icon-box.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-icon-box-wrapper">
                                        <div class="elementor-icon-box-icon">
                                            <span class="elementor-icon elementor-animation-" >
                                                <i aria-hidden="true" class="fas fa-phone-alt"></i>				</span>
                                        </div>
                                        <div class="elementor-icon-box-content">
                                            <h4 class="elementor-icon-box-title">
                                                <span  >
                                                    Phone					</span>
                                            </h4>
                                            <p class="elementor-icon-box-description">
                                                <span style="font-weight: bold">Primary:</span> <?= $sideDetails->contact ?>
                                                <span style="font-weight: bold">Secondary: </span><?= $sideDetails->contact2 ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-01c331a elementor-view-framed elementor-shape-square elementor-position-left elementor-vertical-align-bottom animated-slow elementor-mobile-position-top elementor-invisible elementor-widget elementor-widget-icon-box" data-id="01c331a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;slideInUp&quot;}" data-widget_type="icon-box.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-icon-box-wrapper">
                                        <div class="elementor-icon-box-icon">
                                            <span class="elementor-icon elementor-animation-" >
                                                <i aria-hidden="true" class="fas fa-envelope"></i>				</span>
                                        </div>
                                        <div class="elementor-icon-box-content">
                                            <h4 class="elementor-icon-box-title">
                                                <span  >
                                                    Email					</span>
                                            </h4>
                                            <p class="elementor-icon-box-description">
                                                <?= $sideDetails->email ?>   
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-01c331a elementor-view-framed elementor-shape-square elementor-position-left elementor-vertical-align-bottom animated-slow elementor-mobile-position-top elementor-invisible elementor-widget elementor-widget-icon-box" data-id="01c331a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;slideInUp&quot;}" data-widget_type="icon-box.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-icon-box-wrapper">
                                        <div class="elementor-icon-box-icon">
                                            <span class="elementor-icon elementor-animation-" >
                                                <i aria-hidden="true" class="fas fa-clock"></i>				</span>
                                        </div>
                                        <div class="elementor-icon-box-content">
                                            <h4 class="elementor-icon-box-title">
                                                <span  >
                                                    Hours
                                                </span>
                                            </h4>
                                            <p class="elementor-icon-box-description">
                                                <?= $sideDetails->hours ?>   
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </section>
        </div>
        @include('layouts.partials.footer')
        <link rel='stylesheet' id='jeg-dynamic-style-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/jeg-elementor-kit/lib/jeg-framework/assets/css/jeg-dynamic-styles077c.css?ver=1.2.9' media='all' />
        <link rel='stylesheet' id='elementor-post-1576-css' href='<?= asset('public/frontend') ?>/wp-content/uploads/elementor/css/post-1576b6fb.css?ver=1672233308' media='all' />
        <link rel='stylesheet' id='metform-ui-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/metform/public/assets/css/metform-ui0226.css?ver=3.1.2' media='all' />
        <link rel='stylesheet' id='metform-style-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/metform/public/assets/css/style0226.css?ver=3.1.2' media='all' />
        <link rel='stylesheet' id='elementor-post-278-css' href='<?= asset('public/frontend') ?>/wp-content/uploads/elementor/css/post-2784809.css?ver=1672161333' media='all' />
        <link rel='stylesheet' id='e-animations-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/animations/animations.min2e46.css?ver=3.9.2' media='all' />
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/jquery/ui/core.min3f14.js?ver=1.13.2' id='jquery-ui-core-js'></script>
        <script id='qi-addons-for-elementor-script-js-extra'>
var qodefQiAddonsGlobal = {"vars": {"adminBarHeight": 0, "iconArrowLeft": "<svg  xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 34.2 32.3\" xml:space=\"preserve\" style=\"stroke-width: 2;\"><line x1=\"0.5\" y1=\"16\" x2=\"33.5\" y2=\"16\"\/><line x1=\"0.3\" y1=\"16.5\" x2=\"16.2\" y2=\"0.7\"\/><line x1=\"0\" y1=\"15.4\" x2=\"16.2\" y2=\"31.6\"\/><\/svg>", "iconArrowRight": "<svg  xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 34.2 32.3\" xml:space=\"preserve\" style=\"stroke-width: 2;\"><line x1=\"0\" y1=\"16\" x2=\"33\" y2=\"16\"\/><line x1=\"17.3\" y1=\"0.7\" x2=\"33.2\" y2=\"16.5\"\/><line x1=\"17.3\" y1=\"31.6\" x2=\"33.5\" y2=\"15.4\"\/><\/svg>", "iconClose": "<svg  xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 9.1 9.1\" xml:space=\"preserve\"><g><path d=\"M8.5,0L9,0.6L5.1,4.5L9,8.5L8.5,9L4.5,5.1L0.6,9L0,8.5L4,4.5L0,0.6L0.6,0L4.5,4L8.5,0z\"\/><\/g><\/svg>"}};
        </script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/assets/js/main.min6a4d.js?ver=6.1.1' id='qi-addons-for-elementor-script-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/themes/hello-elementor/assets/js/hello-frontend.min8a54.js?ver=1.0.0' id='hello-theme-frontend-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementskit-lite/libs/framework/assets/js/frontend-scriptf71b.js?ver=2.8.0' id='elementskit-framework-js-frontend-js'></script>

        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementskit-lite/widgets/init/assets/js/widget-scriptsf71b.js?ver=2.8.0' id='ekit-widget-scripts-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/js/webpack.runtime.min2e46.js?ver=3.9.2' id='elementor-webpack-runtime-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/js/frontend-modules.min2e46.js?ver=3.9.2' id='elementor-frontend-modules-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min05da.js?ver=4.0.2' id='elementor-waypoints-js'></script>
        <script id='elementor-frontend-js-before'>
var elementorFrontendConfig = {"environmentMode": {"edit": false, "wpPreview": false, "isScriptDebug": false}, "i18n": {"shareOnFacebook": "Share on Facebook", "shareOnTwitter": "Share on Twitter", "pinIt": "Pin it", "download": "Download", "downloadImage": "Download image", "fullscreen": "Fullscreen", "zoom": "Zoom", "share": "Share", "playVideo": "Play Video", "previous": "Previous", "next": "Next", "close": "Close"}, "is_rtl": false, "breakpoints": {"xs": 0, "sm": 480, "md": 768, "lg": 1025, "xl": 1440, "xxl": 1600}, "responsive": {"breakpoints": {"mobile": {"label": "Mobile", "value": 767, "default_value": 767, "direction": "max", "is_enabled": true}, "mobile_extra": {"label": "Mobile Extra", "value": 880, "default_value": 880, "direction": "max", "is_enabled": false}, "tablet": {"label": "Tablet", "value": 1024, "default_value": 1024, "direction": "max", "is_enabled": true}, "tablet_extra": {"label": "Tablet Extra", "value": 1200, "default_value": 1200, "direction": "max", "is_enabled": false}, "laptop": {"label": "Laptop", "value": 1366, "default_value": 1366, "direction": "max", "is_enabled": false}, "widescreen": {"label": "Widescreen", "value": 2400, "default_value": 2400, "direction": "min", "is_enabled": false}}}, "version": "3.9.2", "is_static": false, "experimentalFeatures": {"e_dom_optimization": true, "e_optimized_assets_loading": true, "a11y_improvements": true, "additional_custom_breakpoints": true, "e_import_export": true, "e_hidden_wordpress_widgets": true, "hello-theme-header-footer": true, "landing-pages": true, "elements-color-picker": true, "favorite-widgets": true, "admin-top-bar": true, "kit-elements-defaults": true}, "urls": {"assets": "https:\/\/thundersecurity.beecreatives.net\/wp-content\/plugins\/elementor\/assets\/"}, "settings": {"page": [], "editorPreferences": []}, "kit": {"active_breakpoints": ["viewport_mobile", "viewport_tablet"], "global_image_lightbox": "yes", "lightbox_enable_counter": "yes", "lightbox_enable_fullscreen": "yes", "lightbox_enable_zoom": "yes", "lightbox_enable_share": "yes", "lightbox_title_src": "title", "lightbox_description_src": "description", "hello_header_logo_type": "title", "hello_header_menu_layout": "horizontal", "hello_footer_logo_type": "logo"}, "post": {"id": 91, "title": "Contact%20%E2%80%93%20Thunder%20Security", "excerpt": "", "featuredImage": false}};
        </script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/js/frontend.min2e46.js?ver=3.9.2' id='elementor-frontend-js'></script>
        <script id='elementor-frontend-js-after'>
var jkit_ajax_url = "<?= asset('public/frontend') ?>/indexe2f2.html?jkit-ajax-request=jkit_elements", jkit_nonce = "0766250c51";
        </script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/jeg-elementor-kit/assets/js/elements/sticky-element28b8.js?ver=2.5.12' id='jkit-sticky-element-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/metform/public/assets/js/htm0226.js?ver=3.1.2' id='htm-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/vendor/regenerator-runtime.min3937.js?ver=0.13.9' id='regenerator-runtime-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/vendor/wp-polyfill.min2c7c.js?ver=3.15.0' id='wp-polyfill-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/vendor/react.minbc7c.js?ver=17.0.1' id='react-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/vendor/react-dom.minbc7c.js?ver=17.0.1' id='react-dom-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/escape-html.min0311.js?ver=03e27a7b6ae14f7afaa6' id='wp-escape-html-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/element.min6188.js?ver=47162ff4492c7ec4956b' id='wp-element-js'></script>
        <script id='metform-app-js-extra'>
var mf = {"postType": "page", "restURI": "https:\/\/thundersecurity.beecreatives.net\/wp-json\/metform\/v1\/forms\/views\/"};
        </script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/metform/public/assets/js/app0226.js?ver=3.1.2' id='metform-app-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/hooks.min6c65.js?ver=4169d3cf8e8d95a3d6d5' id='wp-hooks-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/i18n.mine57b.js?ver=9e794f35a71bb98672ae' id='wp-i18n-js'></script>
        <script id='wp-i18n-js-after'>
wp.i18n.setLocaleData({'text direction\u0004ltr': ['ltr']});
        </script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/inc/plugins/elementor/assets/js/elementor6a4d.js?ver=6.1.1' id='qi-addons-for-elementor-elementor-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementskit-lite/widgets/init/assets/js/animate-circlef71b.js?ver=2.8.0' id='animate-circle-js'></script>
        <script id='elementskit-elementor-js-extra'>
var ekit_config = {"ajaxurl": "https:\/\/thundersecurity.beecreatives.net\/wp-admin\/admin-ajax.php", "nonce": "8375f5f2b5"};
        </script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementskit-lite/widgets/init/assets/js/elementorf71b.js?ver=2.8.0' id='elementskit-elementor-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/assets/plugins/swiper/swiper.min6a4d.js?ver=6.1.1' id='swiper-js'></script>

    </body>

    <!-- Mirrored from thundersecurity.beecreatives.net/contact/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Dec 2022 18:25:25 GMT -->
</html>
