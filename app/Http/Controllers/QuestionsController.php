<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;
use Illuminate\Support\Facades\DB;

class QuestionsController extends Controller {

    public function index($type = null) {
        $service = '';
        if ($type) {
            $service = Question::find($type);
            if (!$service) {
                $service = new Question;
            }
        }

        $banners = Question::paginate(10);
        return view('admin.questions.index', compact('banners', 'type', 'service'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = [
            'question' => ['required'],
            'answer' => ['required'],
        ];
        $data = request()->validate($rules);
        try {
            DB::beginTransaction();

            $active = 1;
            $id = $request->id;
            $page = [
                'question' => $request->question,
                'answer' => $request->answer,
                'faq_for' => $request->faq_for,
            ];
            $page['status'] = $active;
            if ($id != 'create') {
                Question::find($id)->update($page);
                $outcome = 'FAQ updated successfully.';
            } else {
                Question::create($page);
                $outcome = 'FAQ created successfully.';
            }

            DB::commit();
            return redirect()->route('admin.questions.index')->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

//        if (Gate::denies('banner.index', 'update')) {
//            abort(403);
//        }
        $outcome = '';
        try {
            DB::beginTransaction();
            $page = Question::find($id);
//            $page->pageDetails->delete();
            $page->delete();
            $outcome = "Faq deleted successfully.";

            // save audit
//            $user = Auth::user();
//            $description = $user->displayName() . ' has deleted Banner.';
//            $user->auditTrails()->create(['description' => $description, 'menu_id' => 7]);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

}
