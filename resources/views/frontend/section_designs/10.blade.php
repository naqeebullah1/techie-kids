

<!-- FAQ section start-->
<section class="junior__testimonial__area bg-image--1 section-padding--lg">
    <div class="container">
        <div class="section__title text-center">
            <h2 class="title__line">FAQs For Schools</h2>
        </div>
        <main class="card">

            <div class="acc-container">
                <?php
                $questions = getFAQ('schools');
//                dd($questions);
                ?>
                @foreach($questions as $question)
                <button class="acc-btn"><?=$question->question?></button>

                <div class="acc-content">
                    <p>
                        <?=$question->answer?>
                    </p>
                </div>
                @endforeach

                
            </div>
        </main>
</section>
<!-- FAQ section end-->
