<!-- Strat Slider Area -->
<div class="slide__carosel owl-carousel owl-theme">
    <div class="slider__area bg-pngimage--1  d-flex fullscreen justify-content-start align-items-center" style="background-image:url('<?= asset('frontend/images/slider/techie-kid-slider-01.jpeg') ?>')">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="slider__activation">
                        <!-- Start Single Slide -->
                        <div class="slide">
                            <div class="slide__inner">
                                <h1><?= $slider->title ?></h1>
                                <div class="slider__text">
                                    <?= $slider->description ?>
                                </div>
                                <div class="slider__btn">
                                    <a class="dcare__btn" href="#">Find Your School</a>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Slide -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Slider Area -->
