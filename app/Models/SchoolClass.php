<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolClass extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable,
        SoftDeletes,
        HasFactory;

    protected $guarded = [];
    
    protected $casts = [
        'start_at'  => 'date:h:i a',
    ];

    public function teacher() {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function school() {
        return $this->belongsTo(School::class);
    }
    
    public function scopeDropdown($query) {
        return $query->selectRaw('CONCAT(title,"  (", TIME_FORMAT(start_at, "%h:%i %p"),")") as value,id');
    }
    
    
    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $user = auth()->user();
            $model->created_by = $user->id;
            $model->updated_by = $user->id;
        });

        static::updating(function($model) {
            $user = auth()->user();
            $model->updated_by = $user->id;
        });
    }

}
