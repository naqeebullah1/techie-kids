@extends('layouts.services_theme')
@section('content')
<?php
$siteDetail = siteDetails();
?>
@include('frontend.services.banner',['banner'=>['title'=>$service->title,'bc'=>'Service Details']])

@include('frontend.services.service_section')
@endsection
