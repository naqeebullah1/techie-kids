<!DOCTYPE html>
<html lang="en-US">

    <!-- Mirrored from thundersecurity.beecreatives.net/services-details/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Dec 2022 18:25:01 GMT -->
    <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Services Details &#8211; <?=$service->title?></title>
        <link rel="shortcut icon" sizes="16x16 32x32 64x64" href="<?=asset('public/frontend/wp-content/uploads/2022/11/fav.png')?>">
        <meta name='robots' content='max-image-preview:large' />
        <link rel="alternate" type="application/rss+xml" title="Thunder Security &raquo; Feed" href="<?= asset('public/frontend') ?>/feed/index.html" />
        <link rel="alternate" type="application/rss+xml" title="Thunder Security &raquo; Comments Feed" href="<?= asset('public/frontend') ?>/comments/feed/index.html" />
        <link rel='stylesheet' id='elementor-frontend-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/css/frontend.min2e46.css?ver=3.9.2' media='all' />
        <link rel='stylesheet' id='elementor-post-1304-css' href='<?= asset('public/frontend') ?>/wp-content/uploads/elementor/css/post-13044e52.css?ver=1672161332' media='all' />
        <link rel='stylesheet' id='font-awesome-5-all-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/font-awesome/css/all.min2e46.css?ver=3.9.2' media='all' />
        <link rel='stylesheet' id='font-awesome-4-shim-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/font-awesome/css/v4-shims.min2e46.css?ver=3.9.2' media='all' />
        <link rel='stylesheet' id='elementor-post-1311-css' href='<?= asset('public/frontend') ?>/wp-content/uploads/elementor/css/post-13114e52.css?ver=1672161332' media='all' />
        <link rel='stylesheet' id='jkit-elements-main-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/jeg-elementor-kit/assets/css/elements/main28b8.css?ver=2.5.12' media='all' />
        <link rel='stylesheet' id='wp-block-library-css' href='<?= asset('public/frontend') ?>/wp-includes/css/dist/block-library/style.min6a4d.css?ver=6.1.1' media='all' />
        <link rel='stylesheet' id='classic-theme-styles-css' href='<?= asset('public/frontend') ?>/wp-includes/css/classic-themes.min68b3.css?ver=1' media='all' />
        <link rel='stylesheet' id='template-kit-export-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/template-kit-export/public/assets/css/template-kit-export-public.min365c.css?ver=1.0.21' media='all' />
        <link rel='stylesheet' id='swiper-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/assets/plugins/swiper/swiper.min6a4d.css?ver=6.1.1' media='all' />
        <link rel='stylesheet' id='qi-addons-for-elementor-grid-style-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/assets/css/grid.min6a4d.css?ver=6.1.1' media='all' />
        <link rel='stylesheet' id='qi-addons-for-elementor-helper-parts-style-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/assets/css/helper-parts.min6a4d.css?ver=6.1.1' media='all' />
        <link rel='stylesheet' id='qi-addons-for-elementor-style-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/assets/css/main.min6a4d.css?ver=6.1.1' media='all' />
        <link rel='stylesheet' id='elementor-icons-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min91ce.css?ver=5.16.0' media='all' />
        <link rel='stylesheet' id='elementor-post-5-css' href='<?= asset('public/frontend') ?>/wp-content/uploads/elementor/css/post-54e52.css?ver=1672161332' media='all' />
        <link rel='stylesheet' id='elementor-post-1114-css' href='<?= asset('public/frontend') ?>/wp-content/uploads/elementor/css/post-1114108d.css?ver=1672250132' media='all' />
        <link rel='stylesheet' id='hello-elementor-css' href='<?= asset('public/frontend') ?>/wp-content/themes/hello-elementor/style.minc141.css?ver=2.6.1' media='all' />
        <link rel='stylesheet' id='hello-elementor-theme-style-css' href='<?= asset('public/frontend') ?>/wp-content/themes/hello-elementor/theme.minc141.css?ver=2.6.1' media='all' />
        <link rel='stylesheet' id='elementor-icons-ekiticons-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementskit-lite/modules/elementskit-icon-pack/assets/css/ekiticonsf71b.css?ver=2.8.0' media='all' />
        <link rel='stylesheet' id='skb-cife-elegant_icon-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/skyboot-custom-icons-for-elementor/assets/css/elegant97de.css?ver=1.0.5' media='all' />
        <link rel='stylesheet' id='skb-cife-linearicons_icon-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/skyboot-custom-icons-for-elementor/assets/css/linearicons97de.css?ver=1.0.5' media='all' />
        <link rel='stylesheet' id='skb-cife-themify_icon-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/skyboot-custom-icons-for-elementor/assets/css/themify97de.css?ver=1.0.5' media='all' />
        <link rel='stylesheet' id='ekit-widget-styles-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementskit-lite/widgets/init/assets/css/widget-stylesf71b.css?ver=2.8.0' media='all' />
        <link rel='stylesheet' id='ekit-responsive-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementskit-lite/widgets/init/assets/css/responsivef71b.css?ver=2.8.0' media='all' />
        <link rel='stylesheet' id='google-fonts-1-css' href='https://fonts.googleapis.com/css?family=Inter%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;display=swap&amp;ver=6.1.1' media='all' />
        <link rel='stylesheet' id='elementor-icons-shared-0-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min52d5.css?ver=5.15.3' media='all' />
        <link rel='stylesheet' id='elementor-icons-fa-brands-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min52d5.css?ver=5.15.3' media='all' />
        <link rel='stylesheet' id='elementor-icons-skb_cife-elegant-icon-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/skyboot-custom-icons-for-elementor/assets/css/elegant97de.css?ver=1.0.5' media='all' />
        <link rel='stylesheet' id='elementor-icons-fa-solid-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min52d5.css?ver=5.15.3' media='all' />
        <link rel='stylesheet' id='elementor-icons-skb_cife-themify-icon-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/skyboot-custom-icons-for-elementor/assets/css/themify97de.css?ver=1.0.5' media='all' />
        <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin><script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/font-awesome/js/v4-shims.min2e46.js?ver=3.9.2' id='font-awesome-4-shim-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/jquery/jquery.mina7a0.js?ver=3.6.1' id='jquery-core-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/jquery/jquery-migrate.mind617.js?ver=3.3.2' id='jquery-migrate-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/template-kit-export/public/assets/js/template-kit-export-public.min365c.js?ver=1.0.21' id='template-kit-export-js'></script>
        <link rel="https://api.w.org/" href="<?= asset('public/frontend') ?>/wp-json/index.html" /><link rel="alternate" type="application/json" href="<?= asset('public/frontend') ?>/wp-json/wp/v2/pages/1114.json" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="<?= asset('public/frontend') ?>/xmlrpc0db0.html?rsd" />
        <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="<?= asset('public/frontend') ?>/wp-includes/wlwmanifest.xml" />
        <meta name="generator" content="WordPress 6.1.1" />
        <link rel="canonical" href="index.html" />
        <link rel='shortlink' href='<?= asset('public/frontend') ?>/indexe1d7.html?p=1114' />
        <link rel="alternate" type="application/json+oembed" href="<?= asset('public/frontend') ?>/wp-json/oembed/1.0/embedfb1c.json?url=https%3A%2F%2Fthundersecurity.beecreatives.net%2Fservices-details%2F" />
        <link rel="alternate" type="text/xml+oembed" href="<?= asset('public/frontend') ?>/wp-json/oembed/1.0/embed5e18?url=https%3A%2F%2Fthundersecurity.beecreatives.net%2Fservices-details%2F&amp;format=xml" />
    </head>
    <body class="page-template-default page page-id-1114 qodef-qi--no-touch qi-addons-for-elementor-1.5.6 jkit-color-scheme elementor-default elementor-kit-5 elementor-page elementor-page-1114">
        @include('layouts.partials.header')

        <main id="content" class="site-main post-1114 page type-page status-publish hentry" role="main">
            <div class="page-content">
                <div data-elementor-type="wp-page" data-elementor-id="1114" class="elementor elementor-1114">
                @yield('content')   
                    
                </div>
                
            </div>

            <section id="comments" class="comments-area">




            </section><!-- .comments-area -->
        </main>
        @include('layouts.partials.footer')
        <link rel='stylesheet' id='jeg-dynamic-style-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/jeg-elementor-kit/lib/jeg-framework/assets/css/jeg-dynamic-styles077c.css?ver=1.2.9' media='all' />
        <link rel='stylesheet' id='elementor-post-278-css' href='<?= asset('public/frontend') ?>/wp-content/uploads/elementor/css/post-2784809.css?ver=1672161333' media='all' />
        <link rel='stylesheet' id='metform-ui-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/metform/public/assets/css/metform-ui0226.css?ver=3.1.2' media='all' />
        <link rel='stylesheet' id='metform-style-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/metform/public/assets/css/style0226.css?ver=3.1.2' media='all' />
        <link rel='stylesheet' id='e-animations-css' href='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/animations/animations.min2e46.css?ver=3.9.2' media='all' />
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/jquery/ui/core.min3f14.js?ver=1.13.2' id='jquery-ui-core-js'></script>
        <script id='qi-addons-for-elementor-script-js-extra'>
            var qodefQiAddonsGlobal = {"vars": {"adminBarHeight": 0, "iconArrowLeft": "<svg  xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 34.2 32.3\" xml:space=\"preserve\" style=\"stroke-width: 2;\"><line x1=\"0.5\" y1=\"16\" x2=\"33.5\" y2=\"16\"\/><line x1=\"0.3\" y1=\"16.5\" x2=\"16.2\" y2=\"0.7\"\/><line x1=\"0\" y1=\"15.4\" x2=\"16.2\" y2=\"31.6\"\/><\/svg>", "iconArrowRight": "<svg  xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 34.2 32.3\" xml:space=\"preserve\" style=\"stroke-width: 2;\"><line x1=\"0\" y1=\"16\" x2=\"33\" y2=\"16\"\/><line x1=\"17.3\" y1=\"0.7\" x2=\"33.2\" y2=\"16.5\"\/><line x1=\"17.3\" y1=\"31.6\" x2=\"33.5\" y2=\"15.4\"\/><\/svg>", "iconClose": "<svg  xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 9.1 9.1\" xml:space=\"preserve\"><g><path d=\"M8.5,0L9,0.6L5.1,4.5L9,8.5L8.5,9L4.5,5.1L0.6,9L0,8.5L4,4.5L0,0.6L0.6,0L4.5,4L8.5,0z\"\/><\/g><\/svg>"}};
        </script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/assets/js/main.min6a4d.js?ver=6.1.1' id='qi-addons-for-elementor-script-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/themes/hello-elementor/assets/js/hello-frontend.min8a54.js?ver=1.0.0' id='hello-theme-frontend-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementskit-lite/libs/framework/assets/js/frontend-scriptf71b.js?ver=2.8.0' id='elementskit-framework-js-frontend-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementskit-lite/widgets/init/assets/js/widget-scriptsf71b.js?ver=2.8.0' id='ekit-widget-scripts-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/js/webpack.runtime.min2e46.js?ver=3.9.2' id='elementor-webpack-runtime-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/js/frontend-modules.min2e46.js?ver=3.9.2' id='elementor-frontend-modules-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min05da.js?ver=4.0.2' id='elementor-waypoints-js'></script>
        <script id='elementor-frontend-js-before'>
            var elementorFrontendConfig = {"environmentMode": {"edit": false, "wpPreview": false, "isScriptDebug": false}, "i18n": {"shareOnFacebook": "Share on Facebook", "shareOnTwitter": "Share on Twitter", "pinIt": "Pin it", "download": "Download", "downloadImage": "Download image", "fullscreen": "Fullscreen", "zoom": "Zoom", "share": "Share", "playVideo": "Play Video", "previous": "Previous", "next": "Next", "close": "Close"}, "is_rtl": false, "breakpoints": {"xs": 0, "sm": 480, "md": 768, "lg": 1025, "xl": 1440, "xxl": 1600}, "responsive": {"breakpoints": {"mobile": {"label": "Mobile", "value": 767, "default_value": 767, "direction": "max", "is_enabled": true}, "mobile_extra": {"label": "Mobile Extra", "value": 880, "default_value": 880, "direction": "max", "is_enabled": false}, "tablet": {"label": "Tablet", "value": 1024, "default_value": 1024, "direction": "max", "is_enabled": true}, "tablet_extra": {"label": "Tablet Extra", "value": 1200, "default_value": 1200, "direction": "max", "is_enabled": false}, "laptop": {"label": "Laptop", "value": 1366, "default_value": 1366, "direction": "max", "is_enabled": false}, "widescreen": {"label": "Widescreen", "value": 2400, "default_value": 2400, "direction": "min", "is_enabled": false}}}, "version": "3.9.2", "is_static": false, "experimentalFeatures": {"e_dom_optimization": true, "e_optimized_assets_loading": true, "a11y_improvements": true, "additional_custom_breakpoints": true, "e_import_export": true, "e_hidden_wordpress_widgets": true, "hello-theme-header-footer": true, "landing-pages": true, "elements-color-picker": true, "favorite-widgets": true, "admin-top-bar": true, "kit-elements-defaults": true}, "urls": {"assets": "https:\/\/thundersecurity.beecreatives.net\/wp-content\/plugins\/elementor\/assets\/"}, "settings": {"page": [], "editorPreferences": []}, "kit": {"active_breakpoints": ["viewport_mobile", "viewport_tablet"], "global_image_lightbox": "yes", "lightbox_enable_counter": "yes", "lightbox_enable_fullscreen": "yes", "lightbox_enable_zoom": "yes", "lightbox_enable_share": "yes", "lightbox_title_src": "title", "lightbox_description_src": "description", "hello_header_logo_type": "title", "hello_header_menu_layout": "horizontal", "hello_footer_logo_type": "logo"}, "post": {"id": 1114, "title": "Services%20Details%20%E2%80%93%20Thunder%20Security", "excerpt": "", "featuredImage": false}};
        </script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/js/frontend.min2e46.js?ver=3.9.2' id='elementor-frontend-js'></script>
        <script id='elementor-frontend-js-after'>
            var jkit_ajax_url = "<?= asset('public/frontend') ?>/indexe2f2.html?jkit-ajax-request=jkit_elements", jkit_nonce = "0766250c51";
        </script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/jeg-elementor-kit/assets/js/elements/sticky-element28b8.js?ver=2.5.12' id='jkit-sticky-element-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementor/assets/lib/jquery-numerator/jquery-numerator.min3958.js?ver=0.2.1' id='jquery-numerator-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/jeg-elementor-kit/assets/js/isotope/isotope.min7c45.js?ver=3.0.6' id='isotope-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/inc/masonry/assets/js/plugins/packery-mode.pkgd.min6a4d.js?ver=6.1.1' id='packery-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/metform/public/assets/js/htm0226.js?ver=3.1.2' id='htm-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/vendor/regenerator-runtime.min3937.js?ver=0.13.9' id='regenerator-runtime-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/vendor/wp-polyfill.min2c7c.js?ver=3.15.0' id='wp-polyfill-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/vendor/react.minbc7c.js?ver=17.0.1' id='react-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/vendor/react-dom.minbc7c.js?ver=17.0.1' id='react-dom-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/escape-html.min0311.js?ver=03e27a7b6ae14f7afaa6' id='wp-escape-html-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/element.min6188.js?ver=47162ff4492c7ec4956b' id='wp-element-js'></script>
        <script id='metform-app-js-extra'>
            var mf = {"postType": "page", "restURI": "https:\/\/thundersecurity.beecreatives.net\/wp-json\/metform\/v1\/forms\/views\/"};
        </script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/metform/public/assets/js/app0226.js?ver=3.1.2' id='metform-app-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/hooks.min6c65.js?ver=4169d3cf8e8d95a3d6d5' id='wp-hooks-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-includes/js/dist/i18n.mine57b.js?ver=9e794f35a71bb98672ae' id='wp-i18n-js'></script>
        <script id='wp-i18n-js-after'>
            wp.i18n.setLocaleData({'text direction\u0004ltr': ['ltr']});
        </script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/inc/plugins/elementor/assets/js/elementor6a4d.js?ver=6.1.1' id='qi-addons-for-elementor-elementor-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementskit-lite/widgets/init/assets/js/animate-circlef71b.js?ver=2.8.0' id='animate-circle-js'></script>
        <script id='elementskit-elementor-js-extra'>
            var ekit_config = {"ajaxurl": "https:\/\/thundersecurity.beecreatives.net\/wp-admin\/admin-ajax.php", "nonce": "8375f5f2b5"};
        </script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/elementskit-lite/widgets/init/assets/js/elementorf71b.js?ver=2.8.0' id='elementskit-elementor-js'></script>
        <script src='<?= asset('public/frontend') ?>/wp-content/plugins/qi-addons-for-elementor/assets/plugins/swiper/swiper.min6a4d.js?ver=6.1.1' id='swiper-js'></script>

    </body>

    <!-- Mirrored from thundersecurity.beecreatives.net/services-details/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Dec 2022 18:25:04 GMT -->
</html>
