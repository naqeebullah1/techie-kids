<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admission;
use App\Models\AdmissionDetail;
use App\Models\School;
use App\Models\SchoolClass;
use App\Models\User;
use App\Models\Student;

class AdmissionsController extends Controller {

    public function index(Request $request) {
        $admissions = Admission::paginate(15);
        
        if ($request->ajax()) {
            $view = view('admin.admissions.table', compact('admissions'))
                    ->render();
            return response()->json(['html' => $view]);
        }
        
        $schools = School::query();
        $schools = $schools->whereHas('school_classes', function($q) {
                //return $q->whereDate('school_classes.start_on', '>', date('Y-m-d'));
                return $q->whereNotNull('school_classes.start_on')->where('status','=','1');
        });
        $schools = $schools->orderBy('name','ASC')->get();
        
        $parents = User::with('ModelHasRoles')
                    ->singleRole(3)
                    ->orderBy('last_name', 'ASC')->get();
         
        return view('admin.admissions.index', compact('admissions','schools','parents'));
    }
    
     public function classDD() {
        $sId = request()->school_id;
        $value = request()->class_id;

        $options = $this->classList($sId);
               
        $view = view('partials.classdropdown', compact('options', 'value'))
                ->render();
//        $empty="<option>Select</option>"
        return response()->json(['html' => $view]);
    }
    
    public function classList($sId) {
        $class = SchoolClass::dropdown()
                ->where('school_id','=',$sId)->where('status','=','1')->orderBy('title','ASC');
        
        return $class->get();
    }
    
    public function childDD() {
        $sId = request()->parent_id;
        $value = request()->child_id;

        $options = $this->childList($sId);
               
        $view = view('partials.childdropdown', compact('options', 'value'))
                ->render();
//        $empty="<option>Select</option>"
        return response()->json(['html' => $view]);
    }
    
    public function childList($sId) {
        $class = Student::dropdown()
                ->where('user_id','=',$sId)->orderBy('first_name','ASC');
        
        return $class->get();
    }  
    
  
    
    
    public function search_stripe(Request $request) {
            
        if($request) {
            
            $school_id = $request->school_id;
            $class_id = $request->class_id;
            $parent_id = $request->parent_id;
            $child_id = $request->child_id;
            $stripe_subid = $request->subscription_id;
            
            echo "School ".$school_id . " - Class ".$class_id . " - Parent ".$parent_id . " - Child ".$child_id." - Stripe ".$stripe_subid;
            
        } else {
            
        }
        
        
        
    }
}
