<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\State;
use Illuminate\Support\Facades\DB;

class StatesController extends Controller {

    public function index(Request $request) {
        $states = State::paginate(15);
        if ($request->ajax()) {
//            $pageNo = $request->input('page', 1);
//            if ($request->is_fresh_data == 1) {
//                $pageNo = 1;
//            }
            $view = view('admin.states.table', compact('states'))
//                    ->with('i', ($pageNo - 1) * $records)
                    ->render();
            return response()->json(['html' => $view]);
        }
        return view('admin.states.index', compact('states'));
    }

    public function create($id = null) {
        $state = new State();
        if ($id) {
            $state = State::find($id);
        }
        $view = view('admin.states.form', compact('state'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $state = $request->record;
            $id = $request->id;
//            dd($);
            if ($id) {
                $state['updated_by'] = auth()->user()->id;
                $state = State::find($id)->update($state);
                $outcome = "State Updated Successfully";
            } else {
                $state = State::create($state);
                $outcome = "State Created Successfully";
            }

            DB::commit();
            return redirect()->back()->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function destroy($id) {

        $outcome = '';
        try {
            DB::beginTransaction();
            $state = State::find($id);

            if ($state->cities->count()) {
                return redirect()->back()->with('error', 'Some cities are associated to this state. Please delete cities first');
            }

            $state->delete();

            $outcome = "State deleted successfully.";

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

}
