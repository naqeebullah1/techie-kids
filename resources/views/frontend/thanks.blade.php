@extends('layouts.front_theme')
@section('content')
@include('frontend.partials.banner',['data'=>['title'=>'Thank You']])
<section class="junior__welcome__area section-padding--xs bg-pngimage--2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-center">
                    <h2 class="title__line">THANK YOU</h2>
                    <p><strong>Your order has been placed successfully, Our team member will contact you soon</strong></p>
                </div>
            </div>
        </div>
</section>
@endsection
@section('script')
<script>
    function removeRow($this) {
        if (confirm('Are You Sure?')) {
            $($this).parents('tr').remove();
        }
    }
    function changeName($this) {
        $($this).siblings('input').val($($this).children('option:selected').text());
    }
</script>
@endsection
