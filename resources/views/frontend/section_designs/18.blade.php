<!-- End Bradcaump area -->

<!-- Start Contact Form -->
<a id="contactsection"></a>
<section class="contact__box section-padding--lg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 bg-image--19" style="background-image: url('<?= asset('images/thumbnails/' . $section->image) ?>') ">
            </div>
            <div class="col-md-8">

                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-md-12">
                        <div class="section__title text-center">
                            <h2 class="title__line">
                                <?= $section->title ?>   
                            </h2>
                            <p style="font-weight: bold">
                                <?= $section->sub_title ?>
                            </p>
                        <?php
echo $section->section;
                        ?>
                        </div>

                    </div>
                </div>
                
                <div class="row mt--40" style="justify-content: center">
                    <div class="col-lg-12 ">
                        <?php $style= ""; ?>
                            @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{Session::get('success')}}
                                    </div>
                            <?php $style="display:none;" ?>        
                            @endif
                        <div class="contact-form-wrap" style="<?php echo $style; ?>">
                            
                            <form action="<?= route('sendMessage') ?>" method="post">
                                <input type="hidden" name="for_demo" value="1" />
                                @if($errors->any())
                                <?= implode('', $errors->all('<div class="alert alert-danger">:message</div>')) ?>
                                @endif

                                @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    {{Session::get('error')}}
                                </div>
                                @endif
                                @csrf
                                <?php
                                $f = $l = $e = $s = $t = $p = $s_n = $s_o = '';
                                if (old('contact')) {
                                    $cont = old('contact');
                                    $f = $cont['first_name'];
                                    $l = $cont['last_name'];
                                    $e = $cont['email'];
                                    $t = $cont['title'];
                                    $p = $cont['phone'];
                                    $s_n = $cont['school_name'];
                                    $s_o = $cont['select_one'];
                                }
                                ?>
                                <div class="single-contact-form name">
                                    <input type="text" name="contact[first_name]" value="<?= $f ?>" required placeholder="First Name*">
                                    <input type="text" name="contact[last_name]" value="<?= $l ?>" required placeholder="Last Name*">
                                </div>
                                <div class="single-contact-form name">
                                    <input type="text" name="contact[school_name]" value="<?= $s_n ?>" required placeholder="School Name*">
                                    <input type="text" name="contact[phone]" value="<?= $p ?>" pattern='^\(\d{3}\) \d{3}-\d{4}?$' required class="phone"placeholder="School Phone*">
                                </div>
                                <div class="single-contact-form name">
                                    <input type="email" value="<?= $e ?>" name="contact[email]" required placeholder="E-Mail*">
                                    <input type="text" name="contact[title]" value="<?= $t ?>" required placeholder="Contact Title*">
                                </div>
                                <div class="single-contact-form" required>
                                    <select name="contact[select_one]">
                                        <option value="">Please Select One*</option>
                                        <option value="YES" <?php if($s_o == "YES") { echo "selected='selected'";} ?>>Yes! Contact me to schedule Demo Classes at my school</option>
                                        <option value="NO" <?php if($s_o == "NO") { echo "selected='selected'";} ?>>I'm not quite ready to schedule, but I'd like more information</option>
                                    </select>
                                </div>
                                <div class="contact-btn">
                                    <button type="submit" class="dcare__btn">submit</button>
                                </div>
                            </form>
                        </div> 
                        <div class="form-output">
                            <p class="form-messege"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- End Contact Form -->