<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmissionDetailsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('admission_details', function (Blueprint $table) {
            $table->id();
            $table->integer('admission_id')->unsigned();
            $table->integer('student_id')->unsigned();
            $table->integer('school_id');
            $table->integer('school_class_id');

            $table->float('class_charges', 6, 2);
            $table->tinyInteger('status');


            $table->integer('created_by')->unsigned()->default(0);
            $table->integer('updated_by')->unsigned()->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('admission_details');
    }

}
