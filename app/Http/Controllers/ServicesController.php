<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;
use Illuminate\Support\Facades\DB;

class ServicesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = null) {
        $service = '';
        if ($type) {
            $service = Service::find($type);
            if (!$service) {
                $service = new Service;
            }
        }

        $banners = Service::paginate(10);
        return view('services/index', compact('banners', 'type', 'service'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = [
            'title' => ['required'],
            'section' => ['required'],
        ];
        if ($request->id == 'create') {
            $rules['image'] = ['required'];
        }
        $data = request()->validate($rules);
        try {
            DB::beginTransaction();

            $active = 1;
            $id = $request->id;
            $page = ['title' => $request->title,
                'description' => $request->section,
                'short_description' => $request->short_description
            ];
            $page['status'] = $active;
            if ($request->hasFile('image')) {
                $res = app('App\Http\Controllers\BannerController')->resizeImagePost($request);
                $page['image'] = $res;
            }
            if ($id != 'create') {
                Service::find($id)->update($page);
                $outcome = 'New Category has been updated successfully.';
            } else {
                Service::create($page);
                $outcome = 'New Category has been created successfully.';
            }

            DB::commit();
            return redirect()->route('admin.services.index')->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

//        if (Gate::denies('banner.index', 'update')) {
//            abort(403);
//        }
        $outcome = '';
        try {
            DB::beginTransaction();
            $page = Service::find($id);
//            $page->pageDetails->delete();
            $page->delete();
            $outcome = "Service deleted successfully.";

            // save audit
//            $user = Auth::user();
//            $description = $user->displayName() . ' has deleted Banner.';
//            $user->auditTrails()->create(['description' => $description, 'menu_id' => 7]);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

}
