@extends('master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="<?= route('admin.footers.index') ?>" class="pull-right mb-2 btn btn-success">Go Back</a>

            <div class="card">
                <div class="card-header">
                    <div class="card-title pull-left">
                        @if ($type == 'create')
                            Create New Site Detail
                            @php
                                $path = route('admin.footers.store');
                            @endphp
                        @else
                            Update Site Detail
                            @php
                                $path = route('admin.footers.update', $footer->id);
                            @endphp
                        @endif
                    </div>
                </div>
                <div class="card-body">

                    <form method="post" enctype="multipart/form-data" action="{{ $path }}">
                        @if ($type == 'update')
                            @method('patch')
                        @endif
                        @csrf
                        <div class="row mb-2">
                            <div class="col-lg col-md-12 col-sm-12 col-12">
                                <label>Contact Number</label>
                                <input type="text" id="contact" class="form-control @error('contact') is-invalid @enderror"
                                    value="{{ old('contact') ?? $footer->contact }}" name="footer[contact]" />
                                @error('contact')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg col-md-12 col-sm-12 col-12">
                                <label>Second Contact Number</label>
                                <input type="text" id="contact2" class="form-control @error('contact2') is-invalid @enderror"
                                    value="{{ old('contact2') ?? $footer->contact2 }}" name="footer[contact2]" />
                                @error('contact2')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            
                            <div class="col-lg-3 col-md-12 col-sm-12 col-12">
                                <label>Email Address</label>
                                <input type="email" id="contact" class="form-control @error('email') is-invalid @enderror"
                                    value="{{ old('email') ?? $footer->email }}" name="footer[email]" />
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                       <div class="col-lg-4 col-md-12 col-sm-12 col-12">
                                <label>Address</label>
                                <input type="text" id="address" class="form-control @error('address') is-invalid @enderror"
                                    value="{{ old('address') ?? $footer->address }}" name="footer[address]" />
                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                                <label>Facebook Link</label>
                                <input type="url" id="fb_link" class="form-control @error('fb_link') is-invalid @enderror"
                                    value="{{ old('fb_link') ?? $footer->fb_link }}" name="footer[fb_link]" />
                                @error('fb_link')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                                <label>Instagram Link</label>
                                <input type="url" id="insta_link" class="form-control @error('insta_link') is-invalid @enderror"
                                    value="{{ old('insta_link') ?? $footer->insta_link }}" name="footer[insta_link]" />
                                @error('insta_link')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                                <label>Pinterest Link</label>
                                <input type="url" id="twitter_link" class="form-control @error('twitter_link') is-invalid @enderror"
                                    value="{{ old('twitter_link') ?? $footer->twitter_link }}" name="footer[twitter_link]" />
                                @error('twitter_link')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                      
                            <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                                <label>Youtube Link</label>
                                <input type="url" id="youtube_link" class="form-control @error('youtube_link') is-invalid @enderror"
                                    value="{{ old('youtube_link') ?? $footer->youtube_link }}" name="footer[youtube_link]" />
                                @error('youtube_link')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="col-12">Contact Page Info</h4>
                            <div class="col-lg-12 mb-2 col-md-12 col-sm-12 col-12">
                                <label>Description</label>
                                <textarea name="section" class="" id="description"><?=$footer->description?></textarea>
                                @error('footer.description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                <label>Hours</label>
                                <textarea name="hours" class="" id="description"><?=$footer->hours?></textarea>
                                @error('footer.description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                <label>Privacy Policy</label>
                                <textarea required name="privacy" class="ckeditor" id="privacy"><?=$footer->privacy?></textarea>
                                @error('footer.privacy')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                <label>Terms & Conditions</label>
                                <textarea required name="terms" class="ckeditor" id="terms"><?=$footer->terms?></textarea>
                                @error('footer.terms')
                                    <span class="invalid-feedback"  role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-12 text-right">
                                <button type="submit" class="mt-2 btn btn-success">
                                    @if ($type == 'create')
                                        Add
                                    @else
                                        Update
                                    @endif
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
