@extends('master')
@section('content')
<div class="mb-2">
    <h1 class="float-left">States</h1>
    <div class="clearfix"></div>

    <div class="pull-left">
        <b>Legends: </b>
        <i style="padding: 1px 9px;" data-toggle="tooltip" data-placement="top" title="" class="rounded-circle bg-success ml-2 mr-2" data-original-title="Active"></i>
        <i style="padding: 1px 9px;" class="rounded-circle bg-danger mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive"></i>
    </div>
    @can('states-edit')
    <a onclick="event.preventDefault();loadForm(this)" class="btn btn-success float-right" href="<?= url('backend/states/create') ?>">Create State</a>
    @endcan
    <div class="clearfix"></div>
</div>


<div class="record-form"></div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <!--            <div class="card-header">
                            <div class="card-title pull-left">States</div>
                        </div>-->
            <div class="card-body">
                <div class="row table-responsive" id="no-more-tables">
                    @include('admin.states.table')
                </div>

            </div>
        </div>
    </div>
</div>
@include('partials.loadmorejs')
@endsection
@section('script')
@if(Session::has('outcome'))
<script>
    $(function () {
        $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
    })

</script>
@endif
@endsection