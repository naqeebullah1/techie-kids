<div class="row">
    <div class="col-md-12">
        <a href="<?= route('admin.banners.index') ?>" class="pull-right mb-2 btn btn-success">Go Back</a>

        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">
                    @if ($type == 'create')
                    Create New Banner
                    @else
                    Update Banner
                    @endif
                </div>
            </div>
            <div class="card-body">
                <form method="post" enctype="multipart/form-data" action="{{ route('admin.banners.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <input type="hidden" class="form-control" value="{{ $type }}" name="id" />
                            <label>Title</label>
                            <input type="text" onchange=""
                                   class="form-control @error('title') is-invalid @enderror" value="{{ $banner->title }}"
                                   name="title" />
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label>Description</label>
                            <textarea class="section form-control" name="section" required="">{{ $banner->description }}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label>Button Title</label>
                            <input type="text" onchange=""
                                   class="form-control @error('btn_title') is-invalid @enderror" value="{{ $banner->btn_title }}"
                                   name="btn_title" />
                                    @error('btn_title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label>Button Link</label>
                            <input type="text" onchange=""
                                   class="form-control @error('btn_link') is-invalid @enderror" value="{{ $banner->btn_link }}"
                                   name="btn_link" />
                                    @error('btn_link')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                            <label>Image<small class="text-danger"> Preferred image dimensions to upload is 1600*550 </small></label>
                            <input
                                @if ($banner->image) data-default-file="{{ asset('images/thumbnails/' . $banner->image) }}" @endif
                                type="file" class="form-control dropify"
                                name="image">
                                <button type="submit" class="mt-2 pull-right btn btn-success">
                                @if ($type == 'create')
                                Add
                                @else
                                Update
                                @endif
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
