<?php

function menus($type = "main header") {
    $menus = App\Models\Page::where('active', 1)
            ->where('header_type', $type)->orderby('ordinal', 'ASC')
            ->get();
    return $menus;
}

function categories() {
    $menus = App\Models\Category::where('active', 1)->get();
    return $menus;
}

function sectionTypes() {
    return [
        '' => 'Select type',
        1 => '3 columns section',
        2 => 'Right Side Video',
        3 => 'Right Side Image',
        4 => 'Left Side Image',
        5 => '3 1 3 Section',
        6 => 'Gallery',
        7 => 'Testimonials Parents',
        8 => 'FAQ For Parents',
        10 => 'FAQ For Schools',
        9 => 'Schools',
        11 => 'Contact Us Form',
        12 => 'Our Team',
        13 => 'Banner Section',
        14 => 'Home Page Banner',
        15 => '4 Sections design',
        16 => 'Tabs Section',
        17 => 'Grid View Gallery',
        18 => 'Demo Class Form',
        19 => 'Demo Steps',
        20 => 'Testimonials Schools',
    ];
}

function services($id = null) {
    $services = \App\Models\Service::where('status', 1)->get();
    return $services;
}

function siteDetails() {
    $services = \App\Models\Footer::first();
    return $services;
}

function trimText($in) {
    $ch = 100;
    return strlen($in) > $ch ? substr($in, 0, $ch) . "..." : $in;
}

function weekDays() {
    return ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];
}

function formatDate($date) {
    if ($date && $date != '0000-00-00') {
        return date('m/d/Y', strtotime($date));
    }
    return "";
}

function formatTime($date) {
    if ($date) {

        return date('h:i A', strtotime($date));
    }
}

function basicRoles() {
    return [1 => 'Admin', 2 => 'Teacher', 3 => 'Parents', 4 => 'Regional Director'];
}

function getGallery() {
    return App\Models\Category::where('active', 1)->first();
}

function getFAQ($type) {
    return App\Models\Question::where('faq_for', $type)->where('status', 1)->get();
}

function team() {
    $director = App\Models\Team::where('is_active', 1)->where('title', 'Like', '%director%')->get();
    $coach = App\Models\Team::where('is_active', 1)->where('title', 'Like', '%coach%')->get();

    return ['director' => $director, 'coach' => $coach];
}

function testimonial_parents() {
    $testimonial_parents = App\Models\Testimonial::where('is_active', 1)->where('type', '1')->get()->toArray();

    return $testimonial_parents;
}

function testimonial_schools() {
    $testimonial_schools = App\Models\Testimonial::where('is_active', 1)->where('type', '2')->get()->toArray();

    return $testimonial_schools;
}

function searchInThese() {
    return [
        4 => 'Regional Manager',
        2 => 'Teachers',
        3 => 'Parents',
        's' => 'Students',
        'sc' => 'Schools',
        'c' => 'Classes',
    ];
}

function auth_user_roles() {
    return auth()->user()->ModelHasRoles->pluck('role_id')->toArray();
}

?>