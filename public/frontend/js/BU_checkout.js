// Get API Key
let STRIPE_PUBLISHABLE_KEY = document.currentScript.getAttribute('STRIPE_PUBLISHABLE_KEY');
let STRIPE_PAY_URL = document.currentScript.getAttribute('STRIPE_PAY_URL');

// Create an instance of the Stripe object and set your publishable API key
const stripe = Stripe(STRIPE_PUBLISHABLE_KEY);

// Select subscription form element
const subscrFrm = document.querySelector("#subscrFrm");

// Attach an event handler to subscription form
subscrFrm.addEventListener("submit", handleSubscrSubmit);

let elements = stripe.elements();
var style = {
    base: {
        lineHeight: "30px",
        fontSize: "16px",
        border: "1px solid #ced4da",
    }
};
let cardElement = elements.create('card', { style: style });
cardElement.mount('#card-element');

cardElement.on('change', function (event) {
    displayError(event);
});

function displayError(event) {
    if (event.error) {
        showMessage(event.error.message);
    }
}

async function handleSubscrSubmit(e) {
    e.preventDefault();
    setLoading(true);
    
    //let subscr_plan_id = document.getElementById("subscr_plan").value;
    let customer_name = document.getElementById("first_name").value + ' ' + document.getElementById("last_name").value;
    let customer_email = document.getElementById("email").value;
    let customer_amount = document.getElementById("charges").value;
    // Post the subscription info to the server-side script
    
    fetch(STRIPE_PAY_URL, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ request_type:'create_customer_subscription', name: customer_name, email: customer_email, amount:customer_amount }),
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);
        if (data.subscriptionId && data.clientSecret) {
            paymentProcess(data.subscriptionId, data.clientSecret, data.customerId);
        } else {
            showMessage(data.error);
        }
        
        setLoading(false);
    })
    .catch(console.error);
}

function paymentProcess(subscriptionId, clientSecret, customerId){
    //let subscr_plan_id = document.getElementById("subscr_plan").value;
    let customer_name = document.getElementById("first_name").value + ' ' + document.getElementById("last_name").value;
    let customer_email = document.getElementById("email").value;
    let customer_amount = document.getElementById("charges").value;
    
    // Create payment method and confirm payment intent.
    stripe.confirmCardPayment(clientSecret, {
        payment_method: {
            card: cardElement,
            billing_details: {
                name: customer_name,
            },
        }
    }).then((result) => {
        if(result.error) {
            showMessage(result.error.message);
            setLoading(false);
        } else {
            // Successful subscription payment
            // Post the transaction info to the server-side script and redirect to the payment status page
           fetch("https://needanecommercesite.com/techie_kids/parent/stripepay", {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({ request_type:'payment_insert', subscription_id: subscriptionId, customer_id: customerId,payment_intent: result.paymentIntent }),
            })
            .then(response => response.json())
            .then(data => {
                if (data.payment_id) {
                    /*window.location.href = 'payment-status.php?sid='+data.payment_id;*/
                    //window.location.href = 'https://needanecommercesite.com/techie_kids/thanks';
                } else {
                    showMessage(data.error);
                    setLoading(false);
                }
            })
            .catch(console.error);
        }
    });
}

// Display message
function showMessage(messageText) {
    const messageContainer = document.querySelector("#paymentResponse");
    
    messageContainer.classList.remove("hidden");
    messageContainer.textContent = messageText;
    
    setTimeout(function () {
        messageContainer.classList.add("hidden");
        messageText.textContent = "";
    }, 5000);
}

// Show a spinner on payment submission
function setLoading(isLoading) {
    if (isLoading) {
        // Disable the button and show a spinner
        document.querySelector("#submitBtn").disabled = true;
        document.querySelector("#spinner").classList.remove("hidden");
        document.querySelector("#buttonText").classList.add("hidden");
    } else {
        // Enable the button and hide spinner
        document.querySelector("#submitBtn").disabled = false;
        document.querySelector("#spinner").classList.add("hidden");
        document.querySelector("#buttonText").classList.remove("hidden");
    }
}