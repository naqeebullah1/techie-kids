@extends('layouts.front_theme')
@section('content')
    @include('frontend.section_designs.about_content_head', ['title' => 'Profile']);
    
<div class="page_content_wrap page_paddings_no">
                <div class="content_wrap">
                    <div class="content">
                        <article class="post_item post_item_single services hentry services_group-main services_group-small">
                            <section class="post_content">
                                <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_2 margin_top_huge margin_bottom_huge">
                                                    <div class="column-1_2 sc_column_item">
                                                        <figure class="sc_image sc_image_shape_square">
                                                            <img src="<?= asset('images/teams/' . $teamMember->image) ?>" alt="" />
                                                        </figure>
                                                    </div>
                                                    <div class="column-1_2 sc_column_item">
                                                        <h2 class="sc_title sc_title_regular margin_top_null">
                                                            <?= $teamMember->name ?>
                                                        </h2>
                                                        <h6 class="sc_title sc_title_regular margin_top_null">
                                                         <?= $teamMember->title ?>
                                                        </h6>
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                               <?= $teamMember->description ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vc_row-full-width"></div>
                                
                            </section>
                        </article>
                        <section class="related_wrap related_wrap_empty"></section>
                    </div>
                </div>
            </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <div class="page_content_wrap page_paddings_no">
        <!--<div class="content_wrap">-->
            <div class="content">
                <article class="post_item post_item_single services hentry services_group-main services_group-small">
                    <section class="post_content section-shadow">
                       
                            <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid">
                                <div class="sc_services_button sc_item_button margin_top_tiny ml-3">
                                    <a href="<?= url('/page/about') ?>"
                                        class="sc_button sc_button_square sc_button_style_filled sc_button_size_medium float-left">Back</a>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                           
                                            <div
                                                class="columns_wrap sc_columns columns_nofluid sc_columns_count_2 margin_top_huge">
                                                <div class="column-1_2 sc_column_item">
                                                    <figure class="sc_image sc_image_shape_square">
                                                        <img src="<?= asset('images/teams/' . $teamMember->image) ?>"
                                                            alt="" width="100%"
                                                            class=" border rounded shadow-lg mb-5 bg-white pl-3 pt-3 pb-3 pr-3" />
                                                    </figure>
                                                </div>
                                                <div class="column-1_2 sc_column_item team-detail">
                                                    <h4 class="sc_title sc_title_regular margin_top_null txt-color">
                                                        <u><?= $teamMember->name ?></u>
                                                    </h4>
                                                    <h6 class="sc_title sc_title_regular margin_top_null">
                                                        <?= $teamMember->title ?>
                                                    </h6>
                                                    <?= $teamMember->description ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="vc_row-full-width"></div>
                    </section>
                </article>
                <section class="related_wrap related_wrap_empty"></section>
            <!--</div>-->
        </div>
    </div>
@endsection
