      <div class="subscribe__position">
    <?php
    
    if($section->title != "" || $section->subtitle != "") {?>
    <style>
        .subscribe__position .section__title--2 h2.title__line::before {
                left: 0px;
                right: 0px;
                margin-left: auto;
                margin-right: auto;
        }
    </style> 
    <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section__title--2" style="text-align:center;padding: 40px 10px 0px 10px;">
                            <p><strong>{{$section->title}}</strong></p>
                            <h2 class="title__line" data-shadow="{{$section->sub_title}}">{{$section->sub_title}}</h2>
                        </div>
                    </div>
                </div>
            </div> 
            <?php } ?>
    <div class="dacre__kidz__area section-padding--lg bg-image--16">
        <section class="customer-logos slider">
            @foreach($schools as $school)
            <div class="slide">
                <img src="<?=asset('images/uploads/'.$school->image)?>" onerror="this.src='{{asset('frontend/images')}}/logo-noimage.png'" title="{{$school->name}}">
            </div>
            @endforeach()
        </section>
    </div>
</div>