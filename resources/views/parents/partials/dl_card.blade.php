<div class="container">
    <div class="row class__grid__page">
        <!-- Start Single Courses -->
        @forelse($banners as $school)
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="courses">
                <div class="courses__inner">
                    <div class="courses__wrap">
                        <div class="courses__content">
                            <h4>
                                <?= $school->title ?>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="courses__thumb">
                    <iframe width="100%" height="200px" src="<?= $school->link ?>"  allowfullscreen></iframe>َ
                </div>
                <div class="courses__inner">
                    <div class="courses__wrap">
                        <div class="courses__content">
                            <div class="wel__btn">
                                <h5 style="text-decoration: underline;margin-bottom: 10px;font-weight: bold;">Reading Material</h5>
                                <?php
                                $files = explode(',', $school->file);
                                foreach ($files as $file):

                                    if ($file && file_exists(public_path() . '/images/thumbnails/' . $file)) {
                                        $img = 'images/thumbnails/' . $file;
                                        ?>
                                        <!---->
                                        <a class="d-block" href="<?= url($img); ?>" style="font-size: 16px;" target="_blank">
                                            <i class="fa fa-file-pdf-o" style="font-size: 20px;"></i>
                                            <?= $file ?>
                                        </a>

                                        <?php
                                    }
                                endforeach;
                                ?>
                            </div>
                            <?php
                            $des = $school->description;
                            $small = strlen($des) > 200 ? substr($des, 0, 200) . " <a href='javascript:;'> Read More</a>" : $des;
                            if (strlen($des) > 200) {
                                $des = $des . " <a href='javascript:;'> Less</a>";
                            }
                            ?>
                            <div>
                                <p class="d-content less-content" style="height: 70px;" onclick="toggleMore(this, '.more-content')">
                                    <?= $small; ?>
                                </p>
                                <p class="d-content more-content d-none" onclick="toggleMore(this, '.less-content')">
                                    <?= $des; ?>
                                    <!--<a href='javascript:;'> Less</a>-->
                                </p>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        @empty
        <h2 class="text-danger">No Record Found</h2>
        @endforelse
        <!-- End Single Courses -->

    </div>
    <div class="row mt-5">
        <div class="col-lg-12 p-center">
            {{ $banners->links()->with('onclick','getMore(this)')->with('type',request()->type) }}
        </div>
    </div>
</div>