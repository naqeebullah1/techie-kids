@extends('master')

@section('content')
<style>
    .editable-clear-x{
        display: none !important;
    }
    .form-control{
        padding:.6rem 1rem !important;
    }
    .table-responsive th,
    .table-responsive td {
        white-space: nowrap;
        border: 1px solid #ebedf2 !important;
    }
    .modal-lg{
        width: 1250px !important;
    }
</style>
@if($type)
{{--@can('banner.index', 'update')--}}
@include('services.form')
{{--@endcan--}}
@else

<div class="row">
    <div class="col-md-12">
        <a href="<?= route('admin.services.index', 'create') ?>" class="pull-right mb-2 btn btn-success">Add New</a>
        <div class="card">

            <div class="card-header">
                <div class="card-title pull-left">{{ __('Services') }}</div>
            </div>
            <div class="card-body">
                <div class="row">
                    <table class="sortable table table-bordered draggable">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Short Description</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($banners as $k=>$b)
                            <tr>
                                <td>
                                    <?= $b->title ?>
                                </td>
                                <td>
                                    <?= $b->short_description ?>
                                </td>
                                <td>
                                    <img src="{{asset('images/thumbnails/'. $b->image)}}" width="100px">
                                </td>
                                <td>
                                    <a href="{{ url('backend/change-status/services/'.$b->id,$b->status) }}">

                                        <span class="span-status {{ $b->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                                            {{ $b->status == 1 ? __('Active') : __('Blocked') }}
                                        </span>
                                    </a>
                                </td>

                                <td nowrap class="sortable-handle">
                                    <a href="{{ route('admin.services.index',$b->id) }}" class="btn btn-primary">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button type="submit"
                                            onclick="event.preventDefault(); setUrl('{{ route('admin.services.destroy', $b->id) }}'); deleteConfirm('destroy', 'Associated data with this page will also be deleted, Are you sure?')"
                                            class="btn btn-icon btn-round btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <form 
                        id="destroy"
                        method="post" id="destroy" 
                        action="">
                        @csrf
                        @method('DELETE')
                    </form>

                </div>

            </div>
        </div>
{{ $banners->links() }}
    </div>
</div>
@endif
@endsection
@section('script')
@if(Session::has('outcome'))
<script>
    $(function () {
    $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
    })
</script>
@endif
<script>
 function setUrl($url){
        $('form#destroy').attr('action',$url);
    }
</script>

@endsection