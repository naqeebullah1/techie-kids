<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable,
        SoftDeletes,
        HasFactory;

    protected $guarded = [];

    public function admission_details() {
        return $this->hasMany(AdmissionDetail::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
    
    public function scopeDropdown($query) {
        return $query->selectRaw('CONCAT(first_name," ", last_name) as value,id');
    }
    
    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $user = auth()->user();
            if ($user) {
                $model->created_by = $user->id;
                $model->updated_by = $user->id;
            }
        });

        static::updating(function($model) {
            $user = auth()->user();
            $model->updated_by = $user->id;
        });
    }

}
