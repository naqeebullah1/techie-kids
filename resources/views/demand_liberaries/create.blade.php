@extends('master')
@section('title','Create Demand Liberaries')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h4 class="semi-bold text-center light-heading">{{ __('Create Demand Library') }}</h4>

                <div class="card-body">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('admin.demand_liberaries.store') }}">
                        @include('demand_liberaries.form',['create'=>true])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('style')
<link rel="stylesheet" href="<?= asset('plugins/tagify/dist/tagify.css') ?>">
@endsection



@section('script')
<script src="<?= asset('plugins/tagify'); ?>/dist/jQuery.tagify.min.js"></script>
<script data-name="basic">
(function(){
// The DOM element you wish to replace with Tagify
var input = document.querySelector('input[name="category[tags]"]');

// initialize Tagify on the above input node reference
new Tagify(input)
})()
</script>
@endsection