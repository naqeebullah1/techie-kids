<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Team;
use Image;

class TeamController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $teams = Team::orderBy('is_active', 'desc')->get();
        return view('teams.index', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $team = new Team();

        return view('teams.create', compact('team'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $validateData = $request->validate([
            'name' => 'required|max:255',
            'title' => 'required|max:255',
            'image' => 'required|image',
            'section' => 'required',
        ]);

        $data = array(
            'name' => $validateData['name'],
            'title' => $validateData['title'],
            'description' => $validateData['section']
        );

        $imageName = '';

        if ($request->hasFile('image')) {
            $image = $request->file('image');

            $imageName = time() . '.' . $image->extension();

            $destinationPath = public_path('/images/teams');

            $img = Image::make($image->path());
            $img->resize(1000, 1000, function ($constraint) {

                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $imageName);
        }

        $data['image'] = $imageName;

        $outcome = '';
        try {
            DB::beginTransaction();

            Team::create($data);

            DB::commit();
            $outcome = 'New team member has been added successfully.';
        } catch (QueryException $e) {
            DB::rollBack();
            dd($e);
        }

        return redirect()->route('admin.teams.index')->with('outcome', $outcome);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $team = Team::find($id);

        return view('teams.edit', compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'title' => 'required|max:255',
            'image' => 'nullable|image',
            'section' => 'required',
        ]);
        $data = array(
            'name' => $validateData['name'],
            'title' => $validateData['title'],
            'description' => $validateData['section']
        );

        $imageName = '';
        if ($request->hasFile('image')) {
            $image = $request->file('image');

            $imageName = time() . '.' . $image->extension();

            $destinationPath = public_path('/images/teams');

            $img = Image::make($image->path());
            $img->resize(1000, 1000, function ($constraint) {

                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $imageName);
            $data['image'] = $imageName;
        }


        $outcome = '';
        try {
            DB::beginTransaction();
            $data['updated_by'] = '';
            Team::find($id)->update($data);
//            dd($data);

            DB::commit();
            $outcome = 'Team member has been updated successfully.';
        } catch (QueryException $e) {
            DB::rollBack();
            dd($e);
        }

        return redirect()->route('admin.teams.index')->with('outcome', $outcome);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $outcome = '';
        try {
            DB::beginTransaction();

            Team::destroy($id);

            DB::commit();
            $outcome = "Team Member deleted successfully.";
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

}
