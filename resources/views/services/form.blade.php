<div class="row">
    <div class="col-md-12">
                <a href="<?= route('admin.services.index') ?>" class="pull-right mb-2 btn btn-success">Go Back</a>

        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">
                @if($type=='create')
                    Create New Service
                    @else
                    Update Service
                @endif
                </div>
            </div>
            <div class="card-body">
                <form method="post" enctype="multipart/form-data" action="{{ route('admin.services.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label>Title</label>
                            <input type="hidden" class="form-control" value="{{ $type }}" name="id"/>
                            <input type="text" onchange="" class="form-control" value="{{ $service->title }}" name="title"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label>Short Description</label>
                            <textarea class="form-control" name="short_description"><?= $service->short_description ?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label>Description</label>
                            <textarea class="form-control" name="section"><?= $service->description ?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label>Image</label>

                            <input 
                                @if($service->image)
                                data-default-file="{{asset('images/thumbnails/'. $service->image)}}" 
                                @endif
                                type="file" class="form-control dropify" name="image">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12 text-right">
                            <button type="submit" class="mt-2 btn btn-success">
                                @if($type=='create')
                                Add
                                @else
                                Update
                                @endif
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>