<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/x-icon" href="<?= asset('gps_frontend') ?>/images/favicon.png" />
    <title>{{ config('app.name', 'Content Management System') }} - {{ ucwords(request()->segment(2)) }}</title>
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/css/animations.css' type='text/css' media='all' />
    <link rel='stylesheet'
        href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700|Lora:400,400i,700,700i|Merriweather:300,300i,400,400i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Poppins:300,400,500,600,700|Raleway:100,200,300,400,500,600,700,800,900&amp;subset=latin-ext'
        type='text/css' media='all'>
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/js/vendor/revslider/settings.css' type='text/css'
    media='all' />
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/css/fontello/css/fontello.css' type='text/css'
    media='all' />
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/css/core.animation.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/css/shortcodes.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/css/plugin.tribe-events.css' type='text/css'
    media='all' />
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/css/skin.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/css/custom-style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/css/responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/css/skin.responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/js/vendor/comp/comp.min.css' type='text/css'
    media='all' />
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/css/custom.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/css/core.messages.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?= asset('gps_frontend') ?>/js/vendor/swiper/swiper.min.css' type='text/css'
    media='all' />
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/mediaelement/5.0.5/mediaelementplayer.min.css'
        type='text/css' media='all' />
        

    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> --}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('owl-carousel/dist/assets/owl.carousel.min.css')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" />

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"> 
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</head>

<body
    class="indexp home page body_style_wide body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide vc_responsive">
    <a id="toc_home" class="sc_anchor" title="Home"
        data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site"
        data-icon="icon-home" data-separator="yes"></a>
    <a id="toc_top" class="sc_anchor" title="To Top"
        data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page"
        data-icon="icon-double-up" data-url="" data-separator="yes"></a>
    <div class="body_wrap">
        <div class="page_wrap">
            <div class="top_panel_fixed_wrap"></div>
            <input type="hidden" name="slug" id="slug" value="@isset($page->slug){{ $page->slug }}@endisset">
            @include('layouts.partials.header')

            @yield('content')

            <footer class="footer_wrap scheme_dark">
                <div class="footer_wrap_inner widget_area_inner" style="background-attachment: fixed;background-size: 100% 50% !important;background-image: url('{{ asset('gps_frontend/images/footerbg.png') }}') !important;">
                    <div class="content_wrap">
                        <div class="row">
                            <div class="col-md-5 pt-5">
                                <a href="<?php echo url('/'); ?>">
                                    <img src="{{asset('images/footer-logo.png')}}"
                                        class="logo_main" alt="" width="350" height="100">
                                </a>
                            </div>
                            <div class="col-md-3">
                                <h5 class="widget_title"><strong>Helpful Links</strong></h5>
                                <p class="pl-5"><img src="{{ asset('gps_frontend/images/Group 82.svg') }}"></p>
                                <h6 class="post_title">
                                    <a href="<?= url('page/home') ?>" class="footer-txt-style">Home</a>
                                </h6>
                                <h6 class="post_title">
                                    <a href="<?= url('page/services') ?>" class="footer-txt-style">Services</a>
                                </h6>
                                <h6 class="post_title">
                                    <a href="<?= url('page/about') ?>" class="footer-txt-style">About</a>
                                </h6>
                                <h6 class="post_title">
                                    <a href="<?= url('page/contact') ?>" class="footer-txt-style">Contact</a>
                                </h6>
                            </div>
                            <div class="col-md-4">
                                <h5 class="widget_title">Contact Us</h5>
                                <p class="pl-4"><img src="{{ asset('gps_frontend/images/Group 82.svg') }}"></p>
                                <div class="container">
                                    <div class="row">
                                      
                                        <div class="col-12 footer-txt-style pl-0">
                                            <img src="{{ asset('gps_frontend/images/placeholder.svg') }}">
                                            <span>{{ $contact->address1 }}</span>
                                            <br>
                                            {{-- <span class="pl-4"><strong>Address Two: </strong>{{ $contact->address2 }}</span> --}}
                                        </div>
                                      
                                        <div class="col-12 pl-2 pt-3">
                                            <img src="{{ asset('gps_frontend/images/smartphone.svg') }}" alt="">
                                            <a href="tel:<?= $contact->phone1 ?>" class="footer-txt-style"><?= $contact->phone1 ?></a>
                                        </div>
                                        {{-- <div class="col-2 "> </div> --}}
                                        <div class="col-12 pl-2 pt-3">
                                            <img src="{{ asset('gps_frontend/images/mail.svg') }}">
                                            <a href="mailto:<?= $contact->email ?>" class="footer-txt-style"><?= $contact->email ?></a>
                                            </div>
                                        {{-- <div class="col-12 pt-4 pl-0">
                                            <div
                                                class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                                                <div class="sc_socials_item">
                                                    <a href="#" target="_blank"
                                                        class="social_icons social_twitter">
                                                        <span class="icon-twitter"></span>
                                                    </a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a href="#" target="_blank"
                                                        class="social_icons social_gplus">
                                                        <span class="icon-gplus"></span>
                                                    </a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a href="#" target="_blank"
                                                        class="social_icons social_facebook">
                                                        <span class="icon-facebook"></span>
                                                    </a>
                                                </div>
                                                <div class="sc_socials_item">
                                                    <a href="#" target="_blank"
                                                        class="social_icons social_digg">
                                                        <span class="icon-digg"></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div> --}}

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            {{-- <div class="copyright_wrap copyright_style_text">
                <div class="copyright_wrap_inner p-3" style="background-color: #2b363d !important;">
                    <div class="content_wrap_outer">
                        <div class="content_wrap">
                          
                            <div class="copyright_text text-white">Developed By: <a
                                    href="https://www.cyberclouds.com">Cyberclouds</a></div>
                        </div>
                    </div>
                </div>
            </div> --}}
           
        </div>
    </div>

    <a href="<?= asset('gps_frontend') ?>/#" class="scroll_to_top icon-up" title="Scroll to top"></a>
    <div class="custom_html_section"></div>
    <!--<script src="//code.jquery.com/jquery-1.10.2.js"></script>-->
       <script src="{{ asset('owl-carousel/dist/owl.carousel.min.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js" type="text/javascript"></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/vendor/jquery/jquery.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/vendor/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/custom/custom.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/vendor/jquery/fcc8474e79.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/vendor/esg/jquery.themepunch.tools.min.js'></script>

<!--<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->
    <script type='text/javascript'
        src='<?= asset('gps_frontend') ?>/js/vendor/revslider/jquery.themepunch.revolution.min.js'></script>
    <script type="text/javascript"
        src="<?= asset('gps_frontend') ?>/js/vendor/revslider/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript"
        src="<?= asset('gps_frontend') ?>/js/vendor/revslider/extensions/revolution.extension.layeranimation.min.js">
    </script>
    <script type="text/javascript"
        src="<?= asset('gps_frontend') ?>/js/vendor/revslider/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript"
        src="<?= asset('gps_frontend') ?>/js/vendor/revslider/extensions/revolution.extension.parallax.min.js"></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/vendor/modernizr.min.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/vendor/jquery/core.min.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/vendor/superfish.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/custom/jquery.slidemenu.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/custom/core.utils.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/custom/core.init.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/custom/init.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/custom/social-share.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/custom/embed.min.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/custom/shortcodes.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/custom/core.messages.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/vendor/magnific/jquery.magnific-popup.min.js'>
    </script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/vendor/comp/comp_front.min.js'></script>
    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/vendor/swiper/swiper.min.js'></script>
    <script type='text/javascript' src='http://maps.google.com/maps/api/js?key='></script>
  

    <script type='text/javascript' src='<?= asset('gps_frontend') ?>/js/vendor/core.googlemap.js'></script>
     <script>
    $(document).ready(function () {
//    alert();
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            navText: ['<i class="fa-solid fa-circle-arrow-left mr-2"></i>', '<i class="fa-solid fa-circle-arrow-right"></i>'],
            center: true,
            dots: false,
            pagination: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 4
                }
            }
        })
    });
</script>



    <script>
        $(function() {
                $('#message-form').validate({
                    rules: {
                        first_name: {
                            lettersonly: true,
                            required: true,
                            maxlength: 30,
                            minlength: 3
                        },
                        last_name: {
                            lettersonly: true,
                            required: true,
                            maxlength: 30,
                            minlength: 3
                        },
                        email: {
                            required: true,
                            email: true,
                        },
                        subject: {
                            alphanumeric: true,
                            required: true,
                            maxlength: 20,
                            minlength: 3
                        },
                        message: {
                            alphanumeric: true,
                            required: true,
                            maxlength: 255,
                        },
    
                    },
    
                    message: {
                        first_name: {
                            lettersonly: "please enter characters only",
                            required: "Enter your first name, please.",
                            maxlength: "First Name too long.",
                            minlength: "Min 3 Char"
                        },
                        last_name: {
                            lettersonly: "please enter characters only",
                            required: "Enter your last name, please.",
                            maxlength: "First Name too long.",
                            minlength: "Min 3 Char"
                        },
                        email: {
                            required: "Enter your email, please.",
                            email: "Please insert your email",
                        },
                        subject: {
                            alphanumeric: "please enter alphanumeric only",
                            required: "Enter your message, please.",
                            maxlength: "Description too long.",
                        },
                        message: {
                            alphanumeric: "please enter alphanumeric only",
                            required: "Enter your message, please.",
                            maxlength: "Description too long.",
                        },
                    },
    
                    submitHandler: function(form) {
                        form.submit();
                    }
                   
    
                });
            });
    </script>
      <script type='text/javascript'>
      
        $(document).ready(function() {
            
            if ($('#slug').val() == 'home') {
                $('.home').css("color", "#78ba46");
            } else if ($('#slug').val() == 'about') {
                $('.about').css("color", "#78ba46");
            } else if ($('#slug').val() == 'services') {
                $('.services').css("color", "#78ba46");
            } else if ($('#slug').val() == 'gallery') {
                $('.gallery').css("color", "#78ba46");
            } else if ($('#slug').val() == 'contact') {
                $('.contact').css("color", "#78ba46");
            }

            if ($('.carousel-item').length <= 3) {
                $('.carousel').carousel({
                    interval: false,
                });
                $('.carousel-control-next').hide();
                $('.carousel-control-prev').hide();

                if ($(window).width() >= 320 && $(window).width() <= 767) {
                    $('.carousel').carousel({
                        interval: true,
                    });
                    $('.carousel-control-next').show();
                    $('.carousel-control-prev').show();

                }
            }

            //Enable swiping...
            $(".carousel-inner").swipe({
                //Generic swipe handler for all directions
                swipeLeft: function(event, direction, distance, duration, fingerCount) {
                    $(this).parent().carousel('next');
                },
                swipeRight: function() {
                    $(this).parent().carousel('prev');
                },
                //Default is 75px, set to 0 for demo so any distance triggers swipe
                threshold: 0
            });
        });
        $('#carousel-example').on('slide.bs.carousel', function(e) {
            var $e = $(e.relatedTarget);
            var idx = $e.index();
            var itemsPerSlide = 4;
            var totalItems = $('.carousel-item').length;

            if (idx >= totalItems - (itemsPerSlide - 1)) {
                var it = itemsPerSlide - (totalItems - idx);
                for (var i = 0; i < it; i++) {
                    // append slides to end
                    if (e.direction == "left") {
                        $('.carousel-item').eq(i).appendTo('.carousel-inner');
                    } else {
                        $('.carousel-item').eq(0).appendTo('.carousel-inner');
                    }
                }
            }
        });
    </script>
                   </body>

</html>
