<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class contactMail extends Mailable {

    use Queueable,
        SerializesModels;

    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details) {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
   public function build() {
        //$subject = $this->details['subject'] . ' - ' . $this->details['first_name'];
        if(isset($this->details['select_one']) && $this->details['select_one'] != "") {
        return $this->subject("Techie Kids Club School Inquiry - ". $this->details['school_name'])
                        ->view('emails.contact');    
        } else {
            return $this->subject("Techie Kids Club Inquiry - ". $this->details['first_name'] . " " .$this->details['last_name'])
                        ->view('emails.contact');
        }
        
    }

}
