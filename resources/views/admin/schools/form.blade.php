<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">
                    @if(request()->id)
                    {{ __('Edit School') }}
                    @else
                    {{ __('Create School') }}
                    @endif
                </div>
            </div>
            <div class="card-body">
                <form method="post" enctype="multipart/form-data" action="{{ route('admin.schools.store') }}">
                    @csrf
                    <div class="row mb-2">
                        <div class="col-lg-6">
                            <label>Regional Manager</label>
                            <select name="record[user_id]" class="select2">
                                @include('partials.dropdowns',['options'=>$managers,'value'=>$school->user_id])
                            </select>
                        </div>

                        <div class="col-lg-6">
                            <label>School Name</label>
                            <input value="{{ $school->id }}" type="hidden" class="form-control" name="id"/>
                            <input value="{{ $school->name }}" type="text" class="form-control" name="record[name]"/>

                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-lg-6">
                            <label>Contact</label>
                            <input value="{{ $school->contact }}" type="text" class="form-control" name="record[contact]"/>
                        </div>
                        <div class="col-lg-6">
                            <label>Email</label>
                            <input value="{{ $school->email }}" type="email" class="form-control" name="record[email]"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <label>State</label>
                            <select onchange="citiesDD(1)" id="state_id" name="record[state_id]" class="select2">
                                @include('partials.dropdowns',['options'=>$states,'value'=>$school->state_id])
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label>City</label>
                            <select id="city_id" name="record[city_id]" class="select2">
                                @include('partials.dropdowns',['options'=>$cities,'value'=>$school->city_id])
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label>Address</label>
                            <input value="{{ $school->address }}" type="text" class="form-control" name="record[address]"/>
                        </div>

                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label>Logo</label>
                            <input type="file" name="image" data-default-file="<?= ($school->image) ? asset('images/uploads/' . $school->image) : '' ?>" class="dropify">
                        </div>
                    </div>

                    <div class="text-right">
                        <a href="javascript:" onclick="removeForm()" class="mt-2 btn btn-success">Cancel</a>
                        @if(request()->id)
                        <button type="submit" class="mt-2 btn btn-success">Update</button>
                        @else
                        <button type="submit" class="mt-2 btn btn-success">Add</button>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function citiesDD(isChanged = null) {
        var $url = "{{ url('backend/cities-dd') }}";
        var sId = '{{$school->state_id}}';
        var cId = '{{$school->city_id}}';
        if (isChanged) {
            sId = $('#state_id').val();
            cId = '';
        }
        $url = $url + "?state_id=" + sId + "&city_id=" + cId;

        $.get($url, function (data) {
            $('#city_id').html(data.html);
//            $('.select2').select2();
        });
    }
</script>