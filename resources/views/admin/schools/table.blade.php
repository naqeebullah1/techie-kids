<table class="sortable table table-bordered table-small">
    <thead>
        <tr>
            <th>Logo</th>
            <th>Name</th>
            <th>State</th>
            <th>City</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($schools as $school)
        <?php
                if($school->image == "") {
                    $school_image = asset('frontend/images')."/logo-noimage.png";
                } else {
                    $school_image = asset('/images/uploads/' . $school->image);
                }
            ?>
        <tr>
            <td>
                <img src="{{$school_image}}" width="100px">

            </td>
            <td>
                <?= $school->name ?>
            </td>
            <td>
                <?= $school->state->state ?>
            </td>
            <td>
                <?= $school->city->city ?>
            </td>
            <td>
                @can('schools-edit')
                <a href="{{ url('backend/change-status/schools/'.$school->id,$school->status) }}">

                    <span class="span-status {{ $school->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                        {{ $school->status == 1 ? __('Active') : __('Blocked') }}
                    </span>
                </a>

                @else
                <span class="span-status {{ $school->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                    {{ $school->status == 1 ? __('Active') : __('Blocked') }}
                </span>
                @endcan
            </td>



            <td nowrap>
                @can('school-classes-list')
                <?php
                    $url="backend";
                if(in_array(2, $roles)){
                    $url="teacher";
                }
                ?>
                <a href="<?= url($url.'/school-classes/' . $school->id) ?>"
                   class="btn btn-small btn-sm btn-success">
                    Classes
                </a>
                @endcan
                @can('schools-edit')
                <a onclick="event.preventDefault(); loadForm(this)" href="<?= url('backend/schools-create/' . $school->id) ?>"
                   class="btn btn-small btn-sm btn-primary">
                    <i class="fa fa-edit"></i>
                </a>
                <button type="submit"
                        onclick="event.preventDefault(); setUrl('{{ route('admin.schools.destroy', $school->id) }}'); deleteConfirm('destroy', 'Associated data with this page will also be deleted, Are you sure?')"
                        class="btn btn-icon btn-round btn-danger btn-small btn-sm">
                    <i class="fa fa-trash"></i>
                </button>
                @endcan
            </td>
        </tr>
        @endforeach
        @can('page.edit')
        <tr>

            <td colspan="6">
                <input type="submit" value="Update" class="btn btn-success m-auto d-block"/>
            </td>

        </tr>    
        @endcan
    </tbody>
</table>
<form 
    id="destroy"
    method="post" id="destroy" 
    action="">
    @csrf
    @method('DELETE')
</form>
{{ $schools->links() }}
