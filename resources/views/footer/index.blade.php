@extends('master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title pull-left">{{ __('Site Detail') }}</div>
                </div>
                <div class="card-body">
                    <div class="row table-responsive">
                        <table class="sortable table table-bordered draggable">
                            <thead>
                                <tr>
                                    {{-- <th>Title</th> --}}
                                    <th>Contact</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($footer as $k => $f)
                                    <tr>
                                        <td>
<?= $f->contact ?>
                                        </td>
                                        <td>
<?= $f->email ?>
                                        </td>
                                        <td>
<?= $f->address ?>
                                        </td>
                                        
                                        <td class="sortable-handle">
@can('setting-edit')                                            
<a href="{{ route('admin.footers.edit', $f->id) }}"
                                                class="btn btn-primary">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            
@endcan
                                            </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <form id="destroy" method="post" id="destroy" action="">
                            @csrf
                            @method('DELETE')
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
    {{-- @endif --}}
                                        @endsection
                                        @section('script')
                                            @if (Session::has('outcome'))
                                                <script>
                                                    $(function() {
                                                        $.toaster({
                                                            priority: 'success',
                                                            title: 'Success',
                                                            message: "{{ Session::get('outcome') }}"
                                                        });
                                                    })
                                                </script>
                                            @endif
                                            <script>
                                                function setUrl($url) {
                                                    $('form#destroy').attr('action', $url);
                                                }
                                            </script>
                                        @endsection
