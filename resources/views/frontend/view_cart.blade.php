@extends('layouts.front_theme')
@section('content')
@include('frontend.partials.banner',['data'=>['title'=>'Cart Details']])
<section class="htc__checkout bg--white section-padding--lg">
    <!-- Checkout Section Start-->
    <div class="checkout-section">
        <div class="container">
            <div class="row">
                @if(Session::has('success'))
                <div class="col-12 mb-3">
                    <div class="alert alert-success">
                        <p class="text-success"><?= Session::get('success') ?></p>
                    </div>
                </div>
                @endif

                @if(Session::has('error'))
                <p class="text-danger" style="font-weight: bold;font-size: 32px;"><?= Session::get('error') ?></p>
                @endif
                @if(session('cart_info'))

                <!-- Order Details -->
                <div class="col-lg-12 col-12 mb-30">
                    <div class="order-details-wrapper">
                        <h2>your order</h2>
                        <div class="order-details">
                            <!--<form action="#">-->

                            <!--                            <ul>
                                                            <li><p class="strong">product</p><p class="strong">total</p></li>-->
                            <form action="" method="post" id="cartForm">
                                @csrf
                                <div style="padding: 0 20px;">

                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>S.NO</th>
                                                <th>Logo</th>
                                                <th>School</th>
                                                <th>Class</th>
                                                <th>Charges</th>
                                                <th>Student</th>
                                                <th>Remove</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <?php
                                                $st = 0;
                                                $sn = 1;
                                                ?>
                                                @foreach(session('cart_info') as $item)

                                                <?php
                                                $st = $st + $item['class_charges'];
                                                ?>

                                                <td>
                                                    <input type="hidden" name="cart[<?= $sn ?>][school_logo]" value="<?= $item['school_logo'] ?>">
                                                    <input type="hidden" name="cart[<?= $sn ?>][school_name]" value="<?= $item['school_name'] ?>">
                                                    <input type="hidden" name="cart[<?= $sn ?>][title]" value="<?= $item['title'] ?>">
                                                    <input type="hidden" name="cart[<?= $sn ?>][class_charges]" value="<?= $item['class_charges'] ?>">
                                                    <input type="hidden" name="cart[<?= $sn ?>][school_class_id]" value="<?= $item['school_class_id'] ?>">
                                                    <input type="hidden" name="cart[<?= $sn ?>][school_id]" value="<?= $item['school_id'] ?>">
                                                    <?= $sn; ?>
                                                </td>
                                                <td>
                                                    <img width="150px" src="<?= $item['school_logo'] ?>" onerror="this.src='{{asset('frontend/images')}}/logo-noimage.png'">
                                                </td>
                                                <td>
                                                    <?= $item['school_name'] ?>
                                                </td>
                                                <td>
                                                    <?= $item['title'] ?>
                                                </td>
                                                <td>
                                                    <?= $item['class_charges'] ?>
                                                </td>
                                                <td>
                                                    <input type="hidden" name="cart[<?= $sn ?>][student_name]" value="<?= $item['student_name'] ?>">
                                                    <select onchange="changeName(this)" id="child" class="form-control" name="cart[<?= $sn ?>][student_id]">
                                                        @foreach(Auth::user()->students as $student)
                                                        <option <?= ($item['student_id'] == $student->id) ? 'selected' : '' ?> value="<?= $student->id ?>"><?= $student->first_name . ' ' . $student->last_name ?></option>
                                                        @endforeach
                                                    </select>
                                                    
                                    <input required type="text" name="cart[<?= $sn ?>][classroom_name]" value="{{$item['classroom_name']}}" id="classroom_name_<?= $sn ?>" class="form-control classroom_name" placeholder="Enter classroom name">
                                    
                                
                                                </td>
                                                <td>
                                                    <input type="checkbox" onChange="removeRow(this)">
                                                </td>
                                            </tr>
                                            <?php $sn++; ?>

                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                <div style="text-align: right;padding: 0 20px;">
                                    <button type="submit" class="dcare__btn">Update Cart</button>
                                    <a style="color:#fff;" onClick="checkoutAction()"><button type="button" class="dcare__btn btn-info">Checkout</button></a>
                                </div>
                            </form>


                        </div>
                    </div>

                </div>
                @else
                <div class="col-12">
                <p class="text-danger text-center" style="font-weight: bold;font-size: 32px;">Cart is empty</p>
                </div>
                @endif
            </div>
        </div>
    </div><!-- Checkout Section End-->             
</section>  
@endsection
@section('script')
<script>
    function removeRow($this) {
        $.confirm({
            title: 'Are you sure?',
            content: 'You are about to delete an item from cart',
            buttons: {
                confirm: function () {
                   $($this).parents('tr').remove();
                    $("#cartForm").submit();
                },
                cancel: function () {
                }
            }
        });
       /* if (confirm('You are about to delete an item from cart, Are you sure?')) {
            
        }*/
    }
    function changeName($this) {
        $($this).siblings('input').val($($this).children('option:selected').text());
    }
    function checkoutAction() {
        var allGood = 1;
       $(".classroom_name").each(function( index ) {
           if($(this).val() == "") {
               allGood = 0;
           }
       });
       if(allGood == 0) {
            $.alert({
            title: 'Alert!',
            content: 'Classroom name is required, please check and update cart'
            });
            return false;
       } else {
           window.location.assign('{{url("/")}}/checkout');
       }
        
    }
</script>
@endsection
