<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DemandLiberary;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DemandLiberariesController extends Controller {

    public function index(Request $request) {
        $banners = DemandLiberary::query();
        if (request()->ajax()) {
            if (request()->type) {
                $search = request()->type;
//                dd($search);
                $banners = $banners->where(function($q) use($search) {
                    return $q->where('title', 'like', '%' . $search . '%')
                                    ->orWhere('description', 'like', '%' . $search . '%')
                                    ->orWhere('tags', 'like', '%' . $search . '%')
                                    ->orWhere('tags', 'like', '%' . $search . '%');
                });
            }
            $banners = $banners->paginate(10);
            $view = view('demand_liberaries.table', compact('banners'))
                    ->render();
            return $view;
        }
        $banners = $banners->paginate(10);


        return view('demand_liberaries/index', compact('banners'));
    }

    public function dlFileDestroy($id, $file) {
//        dd(request()->all());
        $dl = DemandLiberary::find($id);
        $files = explode(',', $dl->file);
        if (($key = array_search($file, $files)) !== false) {
            unset($files[$key]);
        }
//        if (empty($files)) {
//            return array('success'=>0,'message'=>"");
//        }
        $files = implode(',', $files);
        $dl->file = $files;
        $dl->update();
    }

    public function create() {
        $category = new DemandLiberary;
        return view('demand_liberaries.create', compact('category'));
    }

    public function store(Request $request) {
//        if (Gate::denies('banner.index', 'update')) {
//            abort(403);
//        }
//        dd($request->all());
        $data = request()->validate([
            'category.title' => ['required'],
            'category.tags' => ['required'],
            'image' => ['required'],
            'category.description' => ['required'],
        ]);
        try {
            DB::beginTransaction();
            $user = Auth::user();
            $active = 1;
            $page = $request->category;
            $page['active'] = $active;
            if ($request->hasFile('image')) {
                $images = $request->image;
                $imageNames = [];
                foreach ($images as $img):
                    //$res = app('App\Http\Controllers\BannerController')->resizeImagePost($request, $img);
                    $originName = $img->getClientOriginalName();
                    $fileName = pathinfo($originName, PATHINFO_FILENAME);
                    $extension = $img->getClientOriginalExtension();
                    $fileName = $fileName. "-". date('ymdhis') . '.' . $extension;

                    $img->move(public_path('images/thumbnails'), $fileName);
                
                    $imageNames[] = $fileName;            
                endforeach;
            }

            $page['file'] = implode(',', $imageNames);
            $banner = DemandLiberary::create($page);
            DB::commit();
            $outcome = 'Demand Liberary created successfully.';
            return redirect()->route('admin.demand_liberaries.index')->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function edit($id) {
        $category = DemandLiberary::find($id);
        return view('demand_liberaries.edit', compact('id', 'category'));
    }

    public function update(Request $request, $id) {

        $data = request()->validate([
            'category.title' => ['required'],
            'category.tags' => ['required'],
            'category.description' => ['required'],
        ]);
        try {
            DB::beginTransaction();
            $user = Auth::user();
            $active = 1;
            $page = $request->category;
            $page['active'] = $active;
            $banner = DemandLiberary::find($id);
            $imageNames = explode(',', $banner->file);

            if ($request->hasFile('image')) {
                $images = $request->image;
                foreach ($images as $img):
                    //$res = app('App\Http\Controllers\BannerController')->resizeImagePost($request, $img);
                    $originName = $img->getClientOriginalName();
                    $fileName = pathinfo($originName, PATHINFO_FILENAME);
                    $extension = $img->getClientOriginalExtension();
                    //$fileName = date('ymdhis') . '.' . $extension;
                    $fileName = $fileName. "-". date('ymdhis') . '.' . $extension;
                    
                    $img->move(public_path('images/thumbnails'), $fileName);
                
                    $imageNames[] = $fileName;
                    //$imageNames[] = $res;
                endforeach;
            }

            $page['file'] = implode(',', $imageNames);

            $banner->update($page);
            DB::commit();
            $outcome = 'Demand Liberary updated successfully.';
            return redirect()->route('admin.demand_liberaries.index')->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function getDetail($id) {
        return DemandLiberary::find($id);
    }
    
    public function destroy(DemandLiberary $hierarchy, $id) {
        $outcome = '';
        try {
            DB::beginTransaction();
            DemandLiberary::where('id', $id)->delete();
            $outcome = "Demand Library deleted successfully.";

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }
}
