<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLinkInPageDetailsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('page_details', function (Blueprint $table) {
            $table->string('link')->nullable();
            $table->string('sub_title')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('page_details', function (Blueprint $table) {
            $table->dropColumn('link');
            $table->dropColumn('sub_title');
        });
    }

}
