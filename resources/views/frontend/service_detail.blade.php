@extends('layouts.front_theme')
@section('content')
@include('frontend.section_designs.service_detail_head', ['title' => 'Service Detail']);
<div class="page_content_wrap page_paddings_no">
    <div class="content_wrap">
        <div class="content">
            <article class="post_item post_item_single services hentry services_group-main services_group-small">
                <section class="post_content">
                    <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_2 margin_top_huge margin_bottom_huge">
                                        <div class="column-1_2 sc_column_item">
                                            <figure class="sc_image sc_image_shape_square">
                                                <img src="<?= asset('images/thumbnails/' . $serviceDetail->image) ?>" alt="" />
                                            </figure>
                                        </div>
                                        <div class="column-1_2 sc_column_item">
                                            <h2 class="sc_title sc_title_regular margin_top_null">
                                                <?= $serviceDetail->title ?>
                                            </h2>
<!--                                            <h6 class="sc_title sc_title_regular margin_top_null">
                                                 $serviceDetail->title 
                                            </h6>-->
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <?= $serviceDetail->description ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width"></div>

                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
@endsection
