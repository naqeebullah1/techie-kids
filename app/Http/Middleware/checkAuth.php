<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class checkAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        if (!Auth::check()) {
            return redirect('/')->with('error', 'You are not allowed to access that location');
        } else {
            $currentURL = explode('/', url()->current());
            $user = Auth::user();
            $role = $user->ModelHasRoles->first()->role_id;
//            dd(!in_array('teacher', $currentURL));
            if ($role == 2 && !in_array('teacher', $currentURL)) {
                abort(403);
            }
            if ($role == 3 && !in_array('parent', $currentURL)) {
                abort(403);
            }
        }
        return $next($request);
    }

}
