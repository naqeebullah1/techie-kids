<?php
$roles = auth()->user()->ModelHasRoles->pluck('role_id')->toArray();
?>

@if(request()->only_this)
<div class="bg-transparent pt-2 pb-2 text-right">
    <a href="javascript:;"  onclick="printTable()" class="btn btn-success">Export To Excel</a>
</div>
@endif
@if(request()->only_this==4 || !request()->only_this)

@if (!in_array(4, $roles))
<div class="4-container">
    @include('admin.reports.includes.regional_director_table')    
</div>
@endif
@endif
@if (!in_array(2, $roles))
@if(request()->only_this==2 || !request()->only_this)
<div class="2-container">
    @include('admin.reports.includes.teachers_table')    
</div>
@endif
@endif
@if (!in_array(2, $roles))
@if(request()->only_this==3 || !request()->only_this)
<div class="3-container">
    @include('admin.reports.includes.parents_table')    
</div>
@endif
@endif
@if(request()->only_this == 'sc' || !request()->only_this)
<div class="schools-container">
    @include('admin.reports.includes.schools_table')    
</div>
@endif
@if(request()->only_this == 'c' || !request()->only_this)
<div class="school-classes-container">
    @include('admin.reports.includes.school_classes_table')    
</div>
@endif

@if(request()->only_this == 's' || !request()->only_this)
<div class="students-container">
    @include('admin.reports.includes.students_table')    
</div>
@endif