@extends('layouts.front_theme')
@section('title','Forgot Password')
@section('content')


<div class="ht__bradcaump__area">
    <div class="ht__bradcaump__container">
       
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        
                        <nav class="bradcaump-inner">
                            <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
                            <span class="brd-separetor"><img src="{{asset('frontend/images/icons/brad.png')}}" alt="separator images"></span>
                            <span class="breadcrumb-item active">Reset Password</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>

<section class="section-padding--xs bg-pngimage--2">

    <div class="row justify-content-center">
        <div class="col-xs-11 col-sm-11 col-md-8 col-lg-5 p-0" style="border: 1px solid #ccc;box-shadow: 1px 1px 20px rgb(0 0 0 / 15%);">

                
<div class="tab-content p-4">
                
                <div class="tab-pane fade show active">
                  @include('partials.alerts')
                    <div class="login-wrap p-0">
                <h3 class="mb-4 text-center">You can reset your password</h3>
                <form id="form-resetpass" class="signin-form" role="form" action="{{url('/')}}/changePassword" method="POST" autocomplete="off">
                  <input type="hidden" name="reset_token" value="{{$token}}" />
                    @include('admin.users.partials.changepassword')
                <div class="form-group">
                    <button type="submit" id="submitForm" class="form-control btn btn-primary submit px-3">Recover Password</button>
                </div>
                <div class="form-group d-md-flex">
                              <div class="w-100 text-md-right">
                                    <a href="{{url('/')}}/parentlogin" class="text-info m-l-10">Back To Login</a>
                              </div>
                        </div>
              </form>
              </div>
                </div>
            </div>
            </div>
            </div>

          
@endsection