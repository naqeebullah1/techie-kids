@extends('master')
@section('title','Contact Details')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h4 class="semi-bold text-center light-heading">{{ __('Contact Information Setting') }}</h4>

                <div class="card-body">
                    <form id="contact-form" method="POST" enctype="multipart/form-data" action="{{ route('admin.pages.store') }}">
                        @csrf
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif


                        <div class="row clearfix">

                            <input type="hidden" value="{{ $page->id }}" name="id" required="" autocomplete="off" autofocus="">
                            <div class="col-md-6">
                                <div class="form-group form-group-default required" aria-required="true">
                                    <label for="phone1" class="col-md-12 col-form-label text-md-left">Phone 1</label>
                                    <input type="text" class="form-control " name="contact[phone1]" value="{{ $page->phone1 }}" required="" autocomplete="off" autofocus="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-default" aria-required="true">
                                    <label for="phone2" class="col-md-12 col-form-label text-md-left">Phone 2</label>
                                    <input type="text" class="form-control " name="contact[phone2]" value="{{ $page->phone2 }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-default" aria-required="true">
                                    <label for="fax" class="col-md-12 col-form-label text-md-left">Fax</label>
                                    <input type="text" class="form-control " name="contact[fax]" value="{{ $page->fax }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-default required" aria-required="true">
                                    <label for="fax" class="col-md-12 col-form-label text-md-left">Email</label>
                                    <input type="email" class="form-control " required="" name="contact[email]" value="{{ $page->email }}">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group form-group-default required" aria-required="true">
                                    <label for="address1" class="col-md-12 col-form-label text-md-left">Adderess1</label>
                                    <input type="text" class="form-control " name="contact[address1]" required="" value="{{ $page->address1 }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-default" aria-required="true">
                                    <label for="address2" class="col-md-12 col-form-label text-md-left">Adderess2</label>
                                    <input type="text" class="form-control " name="contact[address2]" value="{{ $page->address2 }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-default required" aria-required="true">
                                    <label for="lat" class="col-md-12 col-form-label text-md-left">Latitude</label>
                                    <input type="text" class="form-control " name="contact[lat]" required="" value="{{ $page->lat }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-default required" aria-required="true">
                                    <label for="lng" class="col-md-12 col-form-label text-md-left">Longitude</label>
                                    <input type="text" class="form-control " name="contact[lng]" required="" value="{{ $page->lng }}">
                                </div>
                            </div>



                        </div>
                        <div class="form-group row mt-2">
                            <div class="col-md-12 offset-md-12 text-right">
                                <button id="save-form" type="submit" class="btn btn-primary">
                                    @isset($create)
                                    {{ __('Create') }}
                                    @else
                                    {{ __('Update') }}
                                    @endisset
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#contact-form').validate();
    })
</script>
@endsection