@extends('master')
@section('title','Dashboard')
@section('content')
<style>
    .nav-item{
        border: 1px solid #069d4d;
    }
</style>
<div>
    <h1 class=" col-12">List Parents</h1>
    

    @include('admin.users.partials.parent_search_form')
    <div class="pull-left">
        <b>Legends: </b>
        <i style="padding: 1px 9px;" data-toggle="tooltip" data-placement="top" title="" class="rounded-circle bg-success ml-2 mr-2" data-original-title="Active"></i>
        <i style="padding: 1px 9px;" class="rounded-circle bg-danger mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive"></i>
    </div>
   <!-- @can('user-create')
    <a href="{{route('admin.users.create')}}" class="btn btn-success float-right" role="button">Create User</a>
    @endcan	-->
    <div class="clearfix"></div>
</div>
@include('admin.users.partials.loop.parentlist')    

@include('partials.loadmorejs')
@endsection
