<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStripeFieldsInAdmissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admissions', function (Blueprint $table) {
//  `stripe_payment_intent_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
//  `paid_amount` float(10,2) NOT NULL,
//  `paid_amount_currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
//  `plan_interval` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
//  `plan_interval_count` tinyint(2) NOT NULL DEFAULT '1',
//  `customer_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
//  `customer_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
//  `plan_period_start` datetime DEFAULT NULL,
//  `plan_period_end` datetime DEFAULT NULL,
//  `plan_cancel_at` datetime DEFAULT NULL,    
            $table->string('payment_method')->nullable();
            $table->integer('stripe_subscription_id')->nullable();
            $table->integer('stripe_customer_id')->nullable();
            $table->string('stripe_payment_intent_id')->nullable();
            $table->string('paid_amount')->nullable();
            $table->string('paid_amount_currency')->nullable();
            $table->string('plan_interval')->nullable();
            $table->string('plan_interval_count')->nullable();
            $table->string('customer_name')->nullable();
            $table->string('customer_email')->nullable();
            $table->string('plan_period_start')->nullable();
            $table->string('plan_period_end')->nullable();
            $table->string('plan_cancel_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admissions', function (Blueprint $table) {
            //
        });
    }
}
