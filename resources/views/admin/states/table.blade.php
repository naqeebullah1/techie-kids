<table class="sortable table table-bordered table-small">
    <thead>
        <tr>
            <th>State</th>
            <th>State Code</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($states as $state)
        <tr>
            <td>
                <?= $state->state ?>
            </td>
            <td>
                <?= $state->state_code ?>
            </td>
            <td>               
                @can('states-edit')

                <a href="{{ url('backend/change-status/states/'.$state->id,$state->status) }}">

                    <span class="span-status {{ $state->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                        {{ $state->status == 1 ? __('Active') : __('Blocked') }}
                    </span>
                </a>
                @else
                    <span class="span-status {{ $state->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                        {{ $state->status == 1 ? __('Active') : __('Blocked') }}
                    </span>
                @endcan
            </td>
            <td>
                @can('cities-list')
                <a href="<?= url('backend/cities/' . $state->state_code) ?>"
                   class="btn btn-small btn-sm btn-success">
                    Cities
                </a>
                @endcan
                @can('states-edit')
                <a onclick="event.preventDefault(); loadForm(this)" href="<?= url('backend/states/create/' . $state->id) ?>"
                   class="btn btn-small btn-sm btn-primary">
                    <i class="fa fa-edit"></i>
                </a>
                <button type="submit"
                        onclick="event.preventDefault(); setUrl('{{ route('admin.states.destroy', $state->id) }}'); deleteConfirm('destroy', 'Are you sure?')"
                        class="btn btn-icon btn-round btn-danger btn-small btn-sm">
                    <i class="fa fa-trash"></i>
                </button>
                @endcan
            </td>
        </tr>
        @endforeach

    </tbody>
</table>
<form 
    id="destroy"
    method="post" id="destroy" 
    action="">
    @csrf
    @method('DELETE')
</form>
{{ $states->links() }}
