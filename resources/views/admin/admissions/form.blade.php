<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">
                    @if(request()->id)
                    {{ __('Edit State') }}
                    @else
                    {{ __('Create State') }}
                    @endif
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('admin.states.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <label>State</label>
                            <input value="{{ $state->id }}" type="hidden" class="form-control" name="id"/>
                            <input value="{{ $state->state }}" type="text" class="form-control" name="record[state]"/>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <label>State Code</label>
                            <input 
                                @if(request()->id)
                                disabled
                                @endif
                                type="text" value="{{ $state->state_code }}" class="form-control" name="record[state_code]"/>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                    <div class="text-right">
                        <a href="javascript:" onclick="removeForm()" class="mt-2 btn btn-success">Cancel</a>
                        @if(request()->id)
                        <button type="submit" class="mt-2 btn btn-success">Update</button>
                        @else
                        <button type="submit" class="mt-2 btn btn-success">Add</button>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>