<h4 class="font-weight-bold">Students</h4>
<div style="padding:10px;background: white">
    <table class="sortable table table-striped table-custom table-small">
        <thead>
            <tr>
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
                <?php
                $roles = auth_user_roles();
                ?>
                @if(!in_array(2,$roles))
                <th>Parent</th>
                <th>Phone</th>
                <th>Email</th>
                @endif
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse($students as $city)
            <tr>
                <td>
                    @if($city->image)
                    <img src="<?= asset('images/uploads/' . $city->image) ?>" width="100px">
                    @else
                    <img src="<?= asset('images/uploads/no-image.jpg') ?>" width="100px">
                    @endif
                </td>
                <td>
                    <?= $city->first_name  ?>
                </td>
                <td>
                    <?= $city->last_name  ?>
                </td>
                 
                @if(!in_array(2,$roles))
                <td>
                    <?= $city->parent ?>
                </td>
                <td>
                    <?= $city->phone1 ?>
                </td>
                <td>
                    <?= $city->email ?>
                </td>
                @endif
                <td>
                    <a class="btn btn-info" href="<?php echo url('/') ?>/parent/children/<?php echo $city->parentId; ?>">View Details</a>
                </td>

            </tr>
            @empty
            <tr>
                <td colspan="100%" class="text-danger text-center">No record exists</td>
            </tr>
            @endforelse
        </tbody>
    </table>
    {{ $students->links()->with('type','students') }}
</div>
