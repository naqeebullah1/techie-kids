@extends('master')
@section('content')
<div class="row">
    <div class="col-md-12">
        {{-- @can('testimonial-create') --}}
        <a href="{{ route('admin.testimonials.create') }}" class="pull-right mb-2 btn btn-success">Add New</a>
        {{-- @endcan --}}
        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">{{ __('Testimonial') }}</div>
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs custom-tabs">
                    <li class="nav-item">
                        <a class="nav-link  active " onclick="return changeTab(this)" aria-current="page" href="#tab1">Parents</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " onclick="return changeTab(this)" aria-current="page" href="#tab2">Schools</a>
                    </li> 
                </ul>
                <div id="tab1" class=" tab-container " style="padding:10px;">
                <div class="row table-responsive">
                    <table class="sortable table table-bordered draggable">
                        <thead>
                            <tr>
                                <th>Parent</th>
                                <th>Relation</th>
                                <th>School</th>
                                <th>Comment</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($testimonials_parents as $k => $testimonial)
                            <tr>
                                <td>
                                    <?= $testimonial->parent_name ?>
                                </td>
                                <td>
                                    <?= $testimonial->relation_with_child ?>
                                </td>
                                <td>
                                    <?= $testimonial->school ?>
                                </td>
                                <td>
                                    <?= $testimonial->description ?>
                                </td>
                                
                                <td>
                                    <a
                                        href="{{ url('backend/change-status/testimonials/' . $testimonial->id, $testimonial->is_active) }}">

                                        <span
                                            class="span-status {{ $testimonial->is_active == 1 ? 'active-span-bg' : 'disabled-span-bg' }}">
                                            {{ $testimonial->is_active == 1 ? __('Active') : __('Blocked') }}
                                        </span>
                                    </a>
                                </td>

                                <td nowrap class="sortable-handle">
                                    <a href="{{ route('admin.testimonials.edit', $testimonial->id) }}"
                                       class="btn btn-primary">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button type="submit"
                                            onclick="event.preventDefault(); setUrl('{{ route('admin.testimonials.destroy', $testimonial->id) }}'); deleteConfirm('destroy', 'Associated data with this page will also be deleted, Are you sure?')"
                                            class="btn btn-icon btn-round btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <form id="destroy" method="post" id="destroy" action="">
                        @csrf
                        @method('DELETE')
                    </form>

                </div>
            </div> <!--  End Of Parent Testimonial-->
            <div id="tab2" class=" tab-container d-none" style="padding:10px;">
                <div class="row table-responsive">
                    <table class="sortable table table-bordered draggable">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Teacher</th>
                                <th>School</th>
                                <th>Comment</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($testimonials_schools as $k => $testimonial)
                            <tr>
                                <td>
                                    <img src="{{asset('frontend/images/testimonial')}}/<?= $testimonial->link ?>" width="80px" alt="No Image" onerror="this.src='{{asset('frontend/images/testimonial')}}/no-image.png'"/>
                                </td>
                                <td>
                                    <?= $testimonial->parent_name ?>
                                </td>
                                <td>
                                    <?= $testimonial->school ?>
                                </td>
                                <td>
                                    <?= $testimonial->description ?>
                                </td>
                                
                                <td>
                                    <a
                                        href="{{ url('backend/change-status/testimonials/' . $testimonial->id, $testimonial->is_active) }}">

                                        <span
                                            class="span-status {{ $testimonial->is_active == 1 ? 'active-span-bg' : 'disabled-span-bg' }}">
                                            {{ $testimonial->is_active == 1 ? __('Active') : __('Blocked') }}
                                        </span>
                                    </a>
                                </td>

                                <td nowrap class="sortable-handle">
                                    <a href="{{ route('admin.testimonials.edit', $testimonial->id) }}"
                                       class="btn btn-primary">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button type="submit"
                                            onclick="event.preventDefault(); setUrl('{{ route('admin.testimonials.destroy', $testimonial->id) }}'); deleteConfirm('destroy', 'Associated data with this page will also be deleted, Are you sure?')"
                                            class="btn btn-icon btn-round btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <form id="destroy" method="post" id="destroy" action="">
                        @csrf
                        @method('DELETE')
                    </form>

                </div>
            </div> <!--End of School Testimonials-->
            </div>
        </div>
    </div>
</div>
{{-- @endif --}}
@endsection
@section('script')
@if (Session::has('outcome'))
<script>
    $(function() {
    $.toaster({
    priority: 'success',
            title: 'Success',
            message: "{{ Session::get('outcome') }}"
    });
    })
</script>
@endif
<script>
            function setUrl($url) {
            $('form#destroy').attr('action', $url);
            }
            function changeTab($this) {

        var $tab = $($this).attr('href');
        
        $('.custom-tabs .nav-link').removeClass('active');
        $('.tab-container').addClass('d-none');
        $($this).addClass('active');
        $($tab).removeClass('d-none');
        return false;
    }
</script>
@endsection
