@extends('layouts.front_theme')
@section('content')
@include('frontend.partials.banner',['data'=>['title'=>'Checkout']])
<script src="https://js.stripe.com/v3/"></script>
<script src="{{asset('frontend/js/checkout.js')}}" STRIPE_PAY_URL="{{url('parent/stripepay')}}" STRIPE_PUBLISHABLE_KEY="<?php echo (env('STRIPE_PUBLISHABLE_KEY')) ? env('STRIPE_PUBLISHABLE_KEY') : "pk_live_51HyGh4B3WkD7XcJGGYMsUEIh1yNeIJZUxSQtmqi7qOMHWn9w31bFjqHkK3eADqnraBI7OhcysyZhj0StX9U6yV8b00THERnQeb" ?>" defer></script>

<section class="htc__checkout bg--white section-padding--lg">
    <!-- Checkout Section Start-->
    <div class="checkout-section">
        <div class="container">
            <div class="row">
                @if(Session::has('success'))
                <p class="text-success" style="font-weight: bold;font-size: 32px;"><?= Session::get('success') ?></p>
                @endif

                @if(Session::has('error'))
                <p class="text-danger" style="font-weight: bold;font-size: 32px;"><?= Session::get('error') ?></p>
                @endif
                @if(session('cart_info'))
                <p class="alert alert-danger hidden" style="width: 100%;margin-left: 15px;margin-right: 15px;" id="paymentResponse"></p>

                <div class="col-lg-12 col-12 mb-30">
                    <div class="row">
                        <div class="col-lg-6 col-12">
                            <!-- Checkout Accordion Start -->
                            <div id="checkout-accordion">

                                <!-- Checkout Method -->
                                <div class="single-accordion">
                                    <a class="accordion-head" data-toggle="collapse" data-parent="#checkout-accordion" href="#checkout-method">1. checkout method</a>
                                    <div id="checkout-method" class="collapse show">
                                        <div class="checkout-method accordion-body fix">

                                            <form id="subscrFrm">
                                                <div class="d-flex form-group">
                                                    <div class="mr-2" style="flex-grow: 1">
                                                        <label>First NAME</label>
                                                        <input value="<?= collect(session('cart_info'))->sum('class_charges') ?>" type="hidden" id="charges" required="" autofocus="">
                                                        <input value="<?= auth()->user()->first_name ?>" type="text" id="first_name" class="form-control" placeholder="First Name" required="" autofocus="">
                                                    </div>

                                                    <div style="flex-grow: 1">
                                                        <label>Last NAME</label>
                                                        <input type="text" value="<?= auth()->user()->last_name ?>" id="last_name" class="form-control" placeholder="Last name" required="" autofocus="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>EMAIL</label>
                                                    <input value="<?= auth()->user()->email ?>" type="email" id="email" class="form-control" placeholder="Enter email" required="">
                                                </div>

                                                <div class="form-group">
                                                    <label>CARD INFO</label>
                                                    <div id="card-element">
                                                        <!-- Stripe.js will create card input elements here -->
                                                    </div>
                                                </div>
                                                <input name="cart_index" type="hidden" value="" id="cart_index">
                                                <!-- Form submit button -->
                                                <!--<button id="submitBtn" class="btn btn-success">-->
                                                <!--    <div class="spinner hidden" id="spinner"></div>-->
                                                <!--    <span id="buttonText">Proceed</span>-->
                                                <!--</button>-->
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- Checkout Accordion Start -->
                        </div>

                        <!-- Order Details -->
                        <div class="col-lg-6 col-12 ">

                            <div class="order-details-wrapper">
                                <h2>your order</h2>
                                <div class="order-details disabled">
                                    <!--<form action="#">-->
                                    <ul>
                                        <li class="d-flex">
                                            <p class="strong" style="width:80%">product</p>
                                            <p class="strong text-center" >Amount</p>
                                        </li>
                                        <?php
                                        $st = 0;
                                        //dd(session('cart_info'));
                                        ?>
                                        @foreach(session('cart_info') as $i=>$item)
                                        <?php
                                        $st = $st + $item['class_charges'];
                                        ?>
                                        <li>
                                            <p style="width:80%"><strong>School:</strong> <?= $item['school_name'] ?><br /> <strong>Class:</strong> <?= $item['title'] ?> <br /> <strong>Student:</strong> <?= $item['student_name'] ?><br /> <strong>Classroom:</strong> <?= $item['classroom_name'] ?></p>
                                            <p class="text-center">$<?= $item['class_charges'] ?></p>

                                            <div class="cart-check text-info">
                                                <label class="float-left" style="width:75%">
                                                <input type="checkbox" value="1" id="checkmark-<?=$i?>" class="checkmark-cart pay-<?=$i?>" onChange="enableBtn(id)" />
                                                &nbsp;By clicking here you agree that you will be charged <strong>${{$item['class_charges']}}</strong> every month.
                                                </label>
                                                <button  disabled="disabled" id="pay-<?=$i?>" onclick="setIndex({{$i}})" class="btn btn-success btn-small btn-sm float-right btn-pay">Pay Now</button>
                                            </div>
                                        </li>
                                        @endforeach

                                        <li><p class="strong">order total</p><p class="strong float-right text-center" style="padding-left: 20px;">$<?= number_format($st, 2) ?></p></li>
                                        <!--<li><button class="dcare__btn">place order</button></li>-->
                                    </ul>
                                   
                                    <!--</form>-->
                                </div>
                            </div>

                        </div>                    
                    </div>
                </div>

                @else
                <p class="text-danger text-center" style="font-weight: bold;font-size: 32px;margin:auto;">Cart is empty</p>

                @endif
            </div>
        </div>
    </div><!-- Checkout Section End-->             
</section>  
@endsection
@section('style')
<style>
    .order-details{
        background: white;
        border: 1px solid #f1f2f3;
    }
    .order-details.disabled,#checkout-method.disabled{
        /*pointer-events: none !important;*/
        background: #f1f2f3; 
    }
</style>
@endsection
@section('script')
<script>
    function setIndex($index){
        $('#cart_index').val($index);
    }
</script>
<script>
    
    function addToCart($url) {
        $url = $url + '?student_id=' + $('#child').val() + '&classroom_name=' + $('#classroom_name').val();
        $.get($url, function (data) {
            $('.cart-container').html(data.html);
            $('#item-count').html(data.count);
        });
        toastr.success('Item added to cart');
        return false;
    }
    function enableBtn(id) {
      var allowbtn = $("#"+id).attr('class').split(' ')[1];
        if($("#"+id).is(':checked')) {
            $("#"+allowbtn).attr('disabled',false);
            $("#"+allowbtn).attr('form','subscrFrm');
        } else {
            $("#"+allowbtn).attr('disabled',true);
            $("#"+allowbtn).removeAttr('form');
        }
    }
</script>
@endsection

