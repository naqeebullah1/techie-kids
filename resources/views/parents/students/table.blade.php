<table class="sortable table table-bordered table-small">
    <thead>
        <tr>
            <th>Picture</th>
            <th>First Name</th>
            <th>Last Name</th>
        </tr>
    </thead>
    <tbody>

        @forelse($students as $child)
        <tr>
            <td width="150px"><img width="150px" src="{{asset('images/uploads/'.$child->image)}}" onerror="this.src='{{asset('frontend/images')}}/no-image.png'"></td>
            <td>{{ $child->first_name }}</td>
            <td>{{ $child->last_name }}</td>
            <!--<td>{{ formatDate($child->dob) }}</td>-->
<!--            <td>
                <a class="btn btn-success btn-sm btn-small" href="<?= url('parent/children-create/' . $child->id) ?>" onclick="event.preventDefault();loadForm(this)"><i class="fa fa-edit"></i></a>
            </td>-->
        </tr>
        @empty
        <tr>
            <td colspan="100%">No Record Found</td>
        </tr>
        @endforelse
    </tbody>
</table>
<form 
    id="destroy"
    method="post" id="destroy" 
    action="">
    @csrf
    @method('DELETE')
</form>
{{ $students->links() }}

