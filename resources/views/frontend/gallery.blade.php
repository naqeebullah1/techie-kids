@extends('layouts.front_theme')
@section('content')
<!--  Page Content, class footer-fixed if footer is fixed  -->
<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <div id="flexslider" class="fullpage-wrap small">
        <ul class="slides">
           <?php
            $image='img'.rand(1,5).'.jpeg';
            ?>
            <li class="flex-active-slide" style="background-image: url('{{ asset('login/images/'.$image) }}'); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
                 <div class="container text text-center">
                    <h1 class="white margin-bottom-small">Gallery</h1>
                </div>
                <div class="gradient dark"></div>
            </li>
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Gallery</li>
            </ol>
        </ul>
    </div>
    <!--  END Slider  -->
    <div id="page-wrap" class="content-section fullpage-wrap">
        <!--  Gallery Section  -->
        <section id="gallery" data-isotope="load-simple">
            
            <div class="masonry-items equal four-columns">
                <!--  Lightbox trek -->
                <?php
                $images=$gallery->images;
                ?>
                @foreach($images as $image)
                <div class="one-item">
                    <div class="image-bg" style="background-image:url(<?= asset('images/thumbnails/'.$image->image) ?>)"></div>
                    <div class="content figure">
                        <i class="pd-icon-camera"></i>
                        <a href="<?= asset('images/thumbnails/'.$image->image) ?>" class="link lightbox"></a>
                    </div>
                </div>
                @endforeach
                <!--  END Lightbox trek -->

            </div>
        </section>
        <!--  END Gallery Section  -->
    </div>
</div>
<!--  END Page Content, class footer-fixed if footer is fixed  -->
@endsection