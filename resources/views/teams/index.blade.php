@extends('master')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('admin.teams.create') }}" class="pull-right mb-2 btn btn-success">Add New</a>
            <div class="card">
                <div class="card-header">
                    <div class="card-title pull-left">{{ __('Teams') }}</div>
                </div>
                <div class="card-body">
                    <div class="row table-responsive">
                        <table class="sortable table table-bordered draggable">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Speciality</th>
                                    <!--<th>Description</th>-->
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($teams as $t)
                                    <tr>
                                        <td>
                                            <?= $t->name ?>
                                        </td>
                                        <td>
                                            <?= $t->title ?>
                                        </td>
                                        <!--<td>-->
                                        <!--    <?= $t->description ?>-->
                                        <!--</td>-->
                                        <td>
                                            <img src="{{ asset('images/teams/' . $t->image) }}" width="100px">
                                        </td>
                                        <td>
                                            <a href="{{ url('backend/change-status/teams/' . $t->id, $t->is_active) }}">

                                                <span
                                                    class="span-status {{ $t->is_active == 1 ? 'active-span-bg' : 'disabled-span-bg' }}">
                                                    {{ $t->is_active == 1 ? __('Active') : __('Blocked') }}
                                                </span>
                                            </a>
                                        </td>

                                        <td class="sortable-handle">
                                            <a href="{{ route('admin.teams.edit', $t->id) }}" class="btn btn-primary">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <button type="submit"
                                                onclick="event.preventDefault(); setUrl('{{ route('admin.teams.destroy', $t->id) }}'); deleteConfirm('destroy', 'Associated data with this page will also be deleted, Are you sure?')"
                                                class="btn btn-icon btn-round btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <form id="destroy" method="post" id="destroy" action="">
                            @csrf
                            @method('DELETE')
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
    {{-- @endif --}}
@endsection
@section('script')
    @if (Session::has('outcome'))
        <script>
            $(function() {
                $.toaster({
                    priority: 'success',
                    title: 'Success',
                    message: "{{ Session::get('outcome') }}"
                });
            })
        </script>
    @endif
    <script>
        function setUrl($url) {
            $('form#destroy').attr('action', $url);
        }
    </script>
@endsection
