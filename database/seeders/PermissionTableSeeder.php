<?php
  
namespace Database\Seeders;
  
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
  
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * php artisan db:seed --class=PermissionTableSeeder
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Permission::truncate();


        $permissions = [
           ['name' => 'user-list','page'=>'Users','title'=>'List'],
           ['name' => 'user-create','page'=>'Users','title'=>'Create'],
           ['name' => 'user-edit','page'=>'Users','title'=>'Edit'],
           ['name' => 'user-delete','page'=>'Users','title'=>'Delete'],
        
            ['name' => 'role-list','page'=>'Roles','title'=>'List'],
           ['name' => 'role-create','page'=>'Roles','title'=>'Create'],
           ['name' => 'role-edit','page'=>'Roles','title'=>'Edit'],
           ['name' => 'role-delete','page'=>'Roles','title'=>'Delete'],
            
            
           ['name' => 'banner-list','page'=>'Banners','title'=>'List'],
           ['name' => 'banner-create','page'=>'Banners','title'=>'Create'],
           ['name' => 'banner-edit','page'=>'Banners','title'=>'Edit'],
           ['name' => 'banner-delete','page'=>'Banners','title'=>'Delete'],
            
            
           ['name' => 'page-list','page'=>'Pages','title'=>'List'],
           ['name' => 'page-create','page'=>'pages','title'=>'Create'],
           ['name' => 'page-edit','page'=>'Pages','title'=>'Edit'],
           ['name' => 'page-delete','page'=>'Pages','title'=>'Delete'],
            
            
           ['name' => 'gallery-list','page'=>'Gallery','title'=>'List'],
           ['name' => 'gallery-create','page'=>'Gallery','title'=>'Create'],
           ['name' => 'gallery-edit','page'=>'Gallery','title'=>'Edit'],
           ['name' => 'gallery-delete','page'=>'Gallery','title'=>'Delete'],
            
            
           ['name' => 'setting-list','page'=>'Settings','title'=>'List'],
           ['name' => 'setting-create','page'=>'Settings','title'=>'Create'],
           ['name' => 'setting-edit','page'=>'Settings','title'=>'Edit'],
           ['name' => 'setting-delete','page'=>'Settings','title'=>'Delete'],
            
            
           ['name' => 'states-list','page'=>'States','title'=>'List'],
           ['name' => 'states-create','page'=>'States','title'=>'Create'],
           ['name' => 'states-edit','page'=>'States','title'=>'Edit'],
           ['name' => 'states-delete','page'=>'States','title'=>'Delete'],
           
           
           ['name' => 'cities-list','page'=>'Cities','title'=>'List'],
           ['name' => 'cities-create','page'=>'Cities','title'=>'Create'],
           ['name' => 'cities-edit','page'=>'Cities','title'=>'Edit'],
           ['name' => 'cities-delete','page'=>'Cities','title'=>'Delete'],
           
            
           ['name' => 'schools-list','page'=>'Schools','title'=>'List'],
           ['name' => 'schools-create','page'=>'Schools','title'=>'Create'],
           ['name' => 'schools-edit','page'=>'Schools','title'=>'Edit'],
           ['name' => 'schools-delete','page'=>'Schools','title'=>'Delete'],
        
           
            
           ['name' => 'school-classes-list','page'=>'School Classes','title'=>'List'],
           ['name' => 'school-classes-create','page'=>'School Classes','title'=>'Create'],
           ['name' => 'school-classes-edit','page'=>'School Classes','title'=>'Edit'],
           ['name' => 'school-classes-delete','page'=>'School Classes','title'=>'Delete'],
            
            ['name' => 'faq-list','page'=>'FAQ','title'=>'List'],
           ['name' => 'faq-create','page'=>'FAQ','title'=>'Create'],
           ['name' => 'faq-edit','page'=>'FAQ','title'=>'Edit'],
           ['name' => 'faq-delete','page'=>'FAQ','title'=>'Delete'],
            
            ['name' => 'admissions-list','page'=>'Admissions','title'=>'List'],
           ['name' => 'admissions-create','page'=>'Admissions','title'=>'Create'],
           ['name' => 'admissions-edit','page'=>'Admissions','title'=>'Edit'],
           ['name' => 'admissions-delete','page'=>'Admissions','title'=>'Delete'],
        
            ['name' => 'testimonials-list','page'=>'Testimonials','title'=>'List'],
           ['name' => 'testimonials-create','page'=>'Testimonials','title'=>'Create'],
           ['name' => 'testimonials-edit','page'=>'Testimonials','title'=>'Edit'],
           ['name' => 'testimonials-delete','page'=>'Testimonials','title'=>'Delete'],
        
            ['name' => 'demand-liberaries-list','page'=>'demand-liberaries','title'=>'List'],
           ['name' => 'demand-liberaries-create','page'=>'demand-liberaries','title'=>'Create'],
           ['name' => 'demand-liberaries-edit','page'=>'demand-liberaries','title'=>'Edit'],
           ['name' => 'demand-liberaries-delete','page'=>'demand-liberaries','title'=>'Delete'],
        
        ];
       
     
        foreach ($permissions as $permission) {
             Permission::create($permission);
        }
                DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}