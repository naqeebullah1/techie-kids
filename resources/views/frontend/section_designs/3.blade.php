
<section class="junior__welcome__area section-padding--lg poss-relative bg-image--21">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title--2">
                    <p><strong><?= $section->title ?></strong></p>
                    <h2 class="title__line" data-shadow="<?= $section->sub_title ?>"><?= $section->sub_title ?></h2>
                </div>
            </div>
        </div>
        <div class="row jn__welcome__wrapper--2 ">
            <div class="col-md-12 col-lg-7 col-sm-12">
                <div class="">
                    <!-- Start Single Content -->
                    <div class="single__welcome__content" style="font-size:18px;">
                        <p>
                        <?= $section->section ?>
                        </p>
                    </div>
                    <!-- End Single Content -->
                    @if($section->btn_title != "" && $section->btn_link != "")
                    <div class="wel__btn" style="margin-top:20px;">
                        <a class="dcare__btn" href="<?=$section->btn_link;?>"><?=$section->btn_title; ?> <i class="fa <?=$section->btn_icon?>"></i></a>
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-lg-5">
                <div class=" wow fadeInUp" data-wow-delay="0.5s" style="max-height:300px;overflow:hidden;">
                    <img src="<?=asset('images/thumbnails/'.$section->image)?>" style="max-width:500px;">
                </div>
            </div>
        </div>


    </div>
</section>