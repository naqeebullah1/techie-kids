@extends('master')
@section('title','Team Edit')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h4 class="semi-bold text-center light-heading">{{ __('Edit Team') }}</h4>

                <div class="card-body">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('admin.teams.update', $team->id) }}">
                        @method('patch')
                        @include('teams.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection