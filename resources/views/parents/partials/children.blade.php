<div id="second" class="z-index-0">
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger">
                    {{$error}}
                </div>
                @endforeach
    <!--<div class="row">-->
    <div class="form-container col-12"></div>
    <div class="col-12 hide-for-add">
        <div class="d-flex">
            <h2 class="mb-2" style="flex-grow: 1;">List of Children</h2>
            <a class="dcare__btn" href="<?= url('parent/children-create') ?>" onclick="event.preventDefault();loadForm(this)" style="line-height: 35px;height: 35px;">Add Child</a>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Picture</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <!--<th>Date of birth</th>-->
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $children = $user->students;
                ?>
                @forelse($children as $child)
                <tr>
                    <td width="150px"><img src="{{asset('images/uploads/'.$child->image)}}" onerror="this.src='{{asset('frontend/images')}}/no-image.png'"></td>
                    <td>{{ $child->first_name }}</td>
                    <td>{{ $child->last_name }}</td>
                    <!--<td>{{ formatDate($child->dob) }}</td>-->
                    <td>
                        <a class="btn btn-success btn-sm btn-small" href="<?= url('parent/children-create/'.$child->id) ?>" onclick="event.preventDefault();loadForm(this)"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger btn-sm btn-small" href="<?= url('parent/children-delete/'.$child->id) ?>" onclick="return confirm('Are you sure you want to delete this child?')"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="100%">No Record Found</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <!--</div>-->

    <!--    <div class="icon big">
        <i class="fa fa-users fa-4x"></i>
    </div>-->


    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at viverra est, eu finibus mauris. Quisque tempus vestibulum fringilla. Morbi tortor eros, sollicitudin eu arcu sit amet, aliquet sagittis dolor. </p>-->
</div>