<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStripeFieldsInAdmissionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admission_details', function (Blueprint $table) {
            $table->string('payment_method')->nullable();
            $table->integer('stripe_subscription_id')->nullable();
            $table->integer('stripe_customer_id')->nullable();
            $table->string('stripe_payment_intent_id')->nullable();
            $table->string('paid_amount')->nullable();
            $table->string('paid_amount_currency')->nullable();
            $table->string('plan_interval')->nullable();
            $table->string('plan_interval_count')->nullable();
            $table->string('customer_name')->nullable();
            $table->string('customer_email')->nullable();
            $table->string('plan_period_start')->nullable();
            $table->string('plan_period_end')->nullable();
            $table->string('plan_cancel_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admission_details', function (Blueprint $table) {
            
        });
    }
}
