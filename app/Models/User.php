<?php

namespace App\Models;

use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements Auditable {

    use \OwenIt\Auditing\Auditable,
        SoftDeletes;
    use HasFactory,
        Notifiable,
        HasRoles;

    protected $fillable = [
        'password',
        'reset_token',
        'reset_token_expiry',
        'first_name',
        'last_name',
        'email',
        'status',
        'phone1',
        'phone2',
        'address1',
        'address2',
        'city',
        'state',
        'zip',
        'created_by',
        'updated_by',
        'check_privacy',
        'check_terms',
        'check_privacy_date',
        'check_terms_date',
    ];

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = Hash::make($value);
    }

    public function ModelHasRoles() {
        return $this->hasMany('App\Models\ModelHasRole', 'model_id', 'id');
    }

    public function students() {
        return $this->hasMany(Student::class);
    }

    public function admissions() {
        return $this->hasMany(Admission::class, 'parent_id');
    }

    public function scopeGetList($query, $r) {
//        dd($a);
        return $query->selectRaw('CONCAT(last_name," - ",email) as value,id')
                        ->singleRole($r);


//        return $this->hasMany('App\Models\ModelHasRole', 'model_id', 'id');
    }

    public function scopeSingleRole($query, $r) {
        return $query->whereHas('ModelHasRoles', function($q) use($r) {
                    return $q->where('role_id', $r);
                });
    }

    public static function boot() {
        parent::boot();
        static::creating(function($model) {
            $user = auth()->user();
            if ($user) {
                $model->created_by = $user->id;
                $model->updated_by = $user->id;
            }
        });

        static::updating(function($model) {
            $user = auth()->user();
            if($user){
                
            $model->updated_by = $user->id;
            }
        });
    }

}
