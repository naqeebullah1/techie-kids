<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageDetailSection extends Model {

    use HasFactory;

    public $timestamps = false;
    protected $guarded = [];
    //protected $fillable = ['title','icon','description','btn_title','btn_icon','btn_link'];

}
