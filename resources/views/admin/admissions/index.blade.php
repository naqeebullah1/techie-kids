@extends('master')
@section('content')
<div class="mb-2">
    <h1 class="float-left">Admissions</h1>
    <div class="clearfix"></div>

@include('admin.admissions.stripe_search_form')
    <!--<div class="pull-left">
        <b>Legends: </b>
        <i style="padding: 1px 9px;" data-toggle="tooltip" data-placement="top" title="" class="rounded-circle bg-success ml-2 mr-2" data-original-title="Active"></i>
        <i style="padding: 1px 9px;" class="rounded-circle bg-danger mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive"></i>
    </div>-->
    <div class="clearfix"></div>
</div>


<div class="record-form"></div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <!--            <div class="card-header">
                            <div class="card-title pull-left">States</div>
                        </div>-->
            <div class="card-body">
                <div class="row table-responsive purchase-order-details"></div>
                <div class="row table-responsive purchase-orders" id="no-more-tables">
                    @include('admin.admissions.table')
                </div>

            </div>
        </div>
    </div>
</div>
@include('partials.loadmorejs')
@endsection
@section('style')
<style>
    .dcare__btn{
        align-self: center;
    }
</style>
@endsection

@section('script')
@if(Session::has('outcome'))
<script>
    $(function () {
        $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
    });
</script>

@endif
<script>
    function removePD() {
        $('.purchase-order-details').html('');
        $('.purchase-orders').removeClass('d-none');
    }
    function getDetails($this) {
        $.get($this, function (data) {
            $('.purchase-order-details').html(data.html);
            $('.purchase-orders').addClass('d-none');
            $('.dcare__btn').attr('style','');
            $('.dcare__btn').addClass('btn btn-primary');
        });
        return false;

    }

</script>
@endsection