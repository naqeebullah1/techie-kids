<table class="sortable table table-striped table-custom table-small">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Status</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @forelse($records as $user)
        @php 
        $active = '<i style="" class="rounded-circle bg-success" data-toggle="tooltip" data-placement="top" title=""></i>';
        $inactive = '<i style="" class="rounded-circle bg-danger" data-toggle="tooltip" data-placement="top" title=""></i>';
        @endphp
        <tr>
            <td><?=$user->first_name?></td>
            <td><?=$user->last_name?></td>
            <td><?=$user->email?></td>
            <td><?=$user->phone1?></td>
            <td>
               @if($user->status == 1)@php echo $active @endphp @else @php echo $inactive @endphp @endif
            </td>
            <td>
            @can('user-edit')
            <?php if(isset($show_edit) && $show_edit == 1) { ?>
                <a href="{{route('admin.users.edit',$user->id)}}" class="btn btn-primary mr-1 pt-1 pb-1 pl-2 pr-2" role="button">
                    <i class="fa fa-edit"></i>
                </a>   
                <?php } ?>
                @endcan
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

