@extends('layouts.front_theme')
@section('content')
<?php
$sections = $page->pageDetails;
?>
@foreach($sections as $section)
<?php
?>
@include('frontend.section_designs.'.$section->section_type)
@endforeach
@endsection
@section('script')
<script>
    function changeTab($this) {

        var $tab = $($this).attr('href');
        
        $('.custom-tabs .nav-link').removeClass('active');
        $('.tab-container').addClass('d-none');
        $($this).addClass('active');
        $($tab).removeClass('d-none');
        return false;
    }
</script>
@endsection
