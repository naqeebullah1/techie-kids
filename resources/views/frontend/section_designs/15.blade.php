<!--4 Sections design-->
<!-- Start Team Area -->
<?php
$sectionTabs = $section->sections;
//dump($section);
?>
<section class="dcare__team__area pt--40 pb--150 bg--white" style="padding-top:40px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section__title text-center">
                    <h2 class="title__line">{{$section->sub_title}}</h2>
                    <p>{{$section->title}}</p>
                </div>
            </div>
        </div>
        <div class="row mt--40">
            <!-- Start Single Team -->
            <?php $key = 1; $bgcolor = ""; ?>
            @foreach($sectionTabs as $sectionTab)
            @if($key == 1)
            <?php $bgcolor = "#FFC924"; ?>
            @elseif($key == 2)
            <?php $bgcolor = "#0072BC"; ?>
            @elseif($key == 3)
            <?php $bgcolor = "#F68A33"; ?>
            @else($key == 4)
            <?php $bgcolor = "#BFD731"; ?>
            @endif
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                <div class="team__style--3 team--4 hover-color-2">
                    <div class="team__thumb">
                        <img src="<?= asset('images/thumbnails/' . $sectionTab->icon) ?>" alt="team images">
                    </div>
                    <div class="team__hover__action" style="background:<?php echo $bgcolor; ?>">
                        <div class="team__details">
                            <div class="team__info">
                                <h6><a><?= $sectionTab->title ?></a></h6>
                                <span>&nbsp;</span>
                            </div>
                            <p>

                                <?= $sectionTab->description ?>
                            </p>
<!--                            <p>Lorem ipsum dolor sit amet, consecteturadmodi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                            <ul class="dacre__social__link--2 d-flex justify-content-center">
                                <li class="facebook"><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                <li class="twitter"><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                                <li class="vimeo"><a href="https://vimeo.com/"><i class="fa fa-vimeo"></i></a></li>
                                <li class="pinterest"><a href="https://www.pinterest.com/"><i class="fa fa-pinterest-p"></i></a></li>
                            </ul>-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Team -->
    <?php $key++; ?>
            @endforeach

        </div>
         @if($section->btn_title != "" && $section->btn_link != "")
                    <div class="wel__btn" style="margin-top:40px;display: flex;">
                        <a class="dcare__btn" href="<?=$section->btn_link;?>" style="margin:auto;"><?=$section->btn_title; ?> <i class="fa <?=$section->btn_icon?>"></i></a>
                    </div>
                    @endif
    </div>
</section>
<!-- End Team Area -->