<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\State;
use App\Models\City;
use Illuminate\Support\Facades\DB;

class CitiesController extends Controller {

    public function index(Request $request, $stateCode) {
        $state = State::where('state_code', $stateCode)->first();
        $cities = City::where('state_code', $state->state_code)->paginate(15);
        if ($request->ajax()) {
            $view = view('admin.cities.table', compact('cities'))
                    ->render();
            return response()->json(['html' => $view]);
        }
        return view('admin.cities.index', compact('cities', 'state'));
    }

    public function create($state_code, $id = null) {
        $city = new City();
        if ($id) {
            $city = City::find($id);
        }

        $view = view('admin.cities.form', compact('city'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function store(Request $request) {
//        dd($request->all());
        try {
            DB::beginTransaction();

            $city = $request->record;

            $id = $request->id;
            if ($id) {
                $city['updated_by'] = auth()->user()->id;
                $city = City::find($id)->update($city);
                $outcome = "City Updated Successfully";
            } else {
                $city = City::create($city);
                $outcome = "City Created Successfully";
            }

            DB::commit();
            return redirect()->back()->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function destroy($id) {
        $outcome = '';
        try {
            DB::beginTransaction();
            $record = City::find($id);
            if ($record->schools->count()) {
                return redirect()->back()->with('error', 'Schools are associated to this city. Please delete Schools first');
            }

            $record->delete();
            $outcome = "City deleted successfully.";

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

}
