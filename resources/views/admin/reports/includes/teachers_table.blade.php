<h4 class="font-weight-bold">Teachers</h4>
<div class="p-2">
    
@include('admin.reports.includes.users_table',['records'=> $teachers,'show_edit'=>1])
</div>

{{ $teachers->links()->with('type','2') }}
