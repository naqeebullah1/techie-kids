<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImageInPageDetails extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('page_details', function (Blueprint $table) {
            $table->string('section_type')->nullable();
            $table->integer('image')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('page_details', function (Blueprint $table) {
            $table->dropColumn('image');
            $table->dropColumn('section_type');
        });
    }

}
